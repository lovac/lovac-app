''' this is partially explained here: https://docs.pytest.org/en/latest/goodpractices.html#goodpractices '''
import pathlib
import pkg_resources
from setuptools import setup
from setuptools.command.test import test as test_command
try:
    from pip._internal.req import parse_requirements
except ModuleNotFoundError:
    from pip.req import parse_requirements
from pip import _internal as pip
import sys

with pathlib.Path('requirements.txt').open() as requirements_txt:
    REQS = [
        str(requirement)
        for requirement
        in pkg_resources.parse_requirements(requirements_txt)
    ]
    
class PyTestLint(test_command):
    """ Run the Pytest linter """
    def finalize_options(self):
        test_command.finalize_options(self)
        self.test_args = ["--pylint"]
        self.test_suite = True

    def run_tests(self):
        import pytest
        errcode = pytest.main(self.test_args)
        sys.exit(errcode)

setup(
    name='lovac',
    packages=['lovac'],
    include_package_data=True,
    install_requires=[
        REQS,
    ],
    setup_requires=[
        'pytest-runner',
        'pytest-pylint',
    ],
    tests_require=[
        'pytest',
        'pylint',
    ],
    python_requires='>=3',
    cmdclass={'lint': PyTestLint},
)