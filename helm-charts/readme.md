In this folder we're storing helm-charts. Currently only helm-charts which are copied from the internet to reduce dependencies are in here. This is only necessary if the public helm-chart does not allow to change the image and therefore also imagePullSecrets.

ToDo:
* nginx-ingress to use our cached imaged (it does support imagePullSecrets)
* Prometheus and Grafana to use our private images 

# cert-manager

cert-manager needed to be cloned as the chart does not support imagePullSecret.
Original stuff: https://github.com/k9ert/cert-manager/tree/v0.3.0
Changes in templates/deplyoment.yaml (adding imagePullSecret)