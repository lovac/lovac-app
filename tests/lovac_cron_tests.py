''' Tests for lovac.services and maybe even lovac.cron '''

import pytest
import mock
import json

from tests.fixtures_faketransactions import FakeResponse
from datetime import datetime

# let's squeeze 2 cron.py tests in here ...
def test_late_buyer(db_ready_app):
    ''' test the loop which should buy for all users '''
    #pylint: disable=unused-argument
    from lovac.cron import late_buyer
    fake_buy = json.loads('''{"id": "986683407", "datetime": "2018-02-18 23:22:50.709624", "price": "10452.14", "amount": "0.00057400", "type": "0"} 
    ''')
    bought_today_fakeresponse = FakeResponse(b'''[
        {"datetime" : "2012-10-10 10:12:31"}]''')
    with mock.patch('requests.post', return_value=bought_today_fakeresponse):
        with mock.patch('lovac.bitstampp.TradingClient.buy_btc', return_value=fake_buy):
            late_buyer()

def test_every_hour(db_tickerdata_ready_app):
    ''' test the every hour function '''
    #pylint: disable=unused-argument
    from lovac.cron import every_hour
    every_hour()

def test_notify_users(db_ready_app, client,fresh_user_with_nothing, fresh_user_with_bitstampconfig, fresh_user_with_adminrights):
    ''' tests whether mails get sendout for users short of fiat '''
    #pylint: disable=unused-argument
    from lovac.cron import notify_users
    with db_ready_app.app_context():
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 1, 16, 12, 30, 45)):
            with mock.patch('lovac.services.mail.send_fiat_shortage') as mock_send_fiat_shortage:
                with mock.patch('lovac.services.mail.send_confirmationmail') as mock_send_confirmationmail:
                    with mock.patch('lovac.services.user_is_short_of_fiat',side_effect=[True,False,False,False]) as mock_user_is_short_of_fiat:
                        notify_users()
                        assert mock_send_fiat_shortage.call_count == 1 # We have 4 Users but only one with bitstampconfig
                        assert mock_send_confirmationmail.call_count == 3 # for that user send_fiat_shortage is called


def test_cron_late_buyer(db_ready_app, client,fresh_user_with_nothing, fresh_user_with_bitstampconfig, fresh_user_with_adminrights):
    ''' test the late_buyer '''
    #pylint: disable=unused-argument
    from lovac.cron import late_buyer
    with db_ready_app.app_context():
        with mock.patch('lovac.services.purchase.automatic_buy') as mock_late_buyer:
            # We have 4 Users in the db, which means services.purchase.automatic_buy() should be called 4 times
            from lovac.models import db
            db.session.add(fresh_user_with_bitstampconfig)
            late_buyer()
            print(mock_late_buyer)
            assert mock_late_buyer.call_count == 1 # 3 users but only 1 has a bitstampconfig
            args, kwargs = mock_late_buyer.call_args_list[0]
            db.session.add(fresh_user_with_bitstampconfig) # no idea why we need to add the user to the session AGAIN
            assert args[0].email  == fresh_user_with_bitstampconfig.email
    

    

    


