''' tests for the btc module '''

def test_getbalance():
    ''' test getbalance '''
    from lovac.services.btc import get_balance
    # Hopefully this random address maybe from the master himself is more stable
    assert get_balance("1UZhhzWo85osGzNrs1BVjoE3FP8ea2umX") >= 5000001135
