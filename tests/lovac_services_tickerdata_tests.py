''' Tests for lovac.services and maybe even lovac.cron '''
from datetime import datetime, timedelta, date
import pytz

import pandas as pd

import pytest
import mock
import json

def test_we_have_tickerdata(client, db_tickerdata_ready_app):
    ''' Is the test-tickerdata in the DB?! '''
    #pylint: disable=unused-argument
    from lovac.tickerdata.models import TickerHourly
    from lovac.models import db
    app = db_tickerdata_ready_app
    with app.app_context():
        ticker_hourly = db.session.query(TickerHourly).all()
        print("ticker_hourly: " + str(ticker_hourly))
    assert len(ticker_hourly) == 1093 

def test_get_tickerdata1(client, db_tickerdata_ready_app):
    ''' the service is returning tickerdata '''
    #pylint: disable=unused-argument
    from lovac.services.tickerdata import get_tickerdata
    app = db_tickerdata_ready_app
    with app.app_context():
        tickerdata = get_tickerdata()       
        assert len(tickerdata.index) == 973

def test_get_current_tickerdata(client, db_tickerdata_ready_app):
    ''' tests the get_current_tickerdata '''
    #pylint: disable=unused-argument
    from lovac.services.tickerdata import get_current_tickerdata
    price, lower_band = get_current_tickerdata(datetime(2018, 3, 12,0,0,0))
    # This is a miracle!
    #assert price == 9595.033812538444
    #assert lower_band == 8221.619450327602

def test_find_index_for_date(client,db_tickerdata_ready_app):
    ''' test the find_index_for_date method '''
    #pylint: disable=unused-argument
    from lovac.services.tickerdata import get_tickerdata, _find_index_for_date
    from datetime import datetime, timedelta, date
    app = db_tickerdata_ready_app
    with app.app_context():
        td = get_tickerdata()
        # let's take a random timestamp
        mydate = td.iloc[953]["timestamp"]
        assert _find_index_for_date(td,mydate) == 953
        assert _find_index_for_date(td, date= mydate + timedelta(minutes=50)) == 953
        assert _find_index_for_date(td, mydate - timedelta(minutes=50)) == 952
        assert _find_index_for_date(td, datetime(2018, 3, 8,8,0,0,tzinfo=pytz.utc)) == 391
        assert _find_index_for_date(td, datetime(2018, 3, 8,8,23,0,tzinfo=pytz.utc)) == 391

def test_is_price_recovering_or_in_freefall(client,db_tickerdata_ready_app):
    ''' is the price in freefall meaning ... is it below th elower bollinger-band? '''
    #pylint: disable=unused-argument
    from lovac.services.tickerdata import get_tickerdata, is_price_in_freefall, is_price_recovering
    app = db_tickerdata_ready_app
    with app.app_context():
        not_in_freefall = datetime(2018, 3, 12,0,0,0,tzinfo=pytz.utc)
        assert not is_price_in_freefall(date=not_in_freefall)
        in_freefall = datetime(2018, 3, 7,18,12,0,tzinfo=pytz.utc)
        assert is_price_in_freefall(date=in_freefall)
        print("------------------------------------")
        td = get_tickerdata()
        print(td[389:395][["timestamp","weightedprice","lower_band"]])
        #     timestamp             weightedprice lower_band
        # 509 2018-03-08 05:00:00    9772.120698  9963.121723   --> freefall, not recovering
        # 510 2018-03-08 06:00:00    9783.333571  9927.805872   --> freefall, not recovering
        # 511 2018-03-08 07:00:00    9921.078177  9899.441827   --> not freefall, recovering
        # 512 2018-03-08 08:00:00    9918.843811  9871.687652   --> not freefall, not recovering
        # 513 2018-03-08 09:00:00   10070.266990  9850.374195   --> not freefall, not recovering
        # 514 2018-03-08 10:00:00   10010.558678  9827.409321   --> not freefall, not recovering
        app.logger.debug("WUUP:{}".format(get_tickerdata().iloc[391]["weightedprice"]))
        assert is_price_in_freefall(date=datetime(2018, 3, 8,5,0,0,tzinfo=pytz.utc))
        assert is_price_in_freefall(date=datetime(2018, 3, 8,5,23,0,tzinfo=pytz.utc))

        assert is_price_in_freefall(date=    datetime(2018, 3, 8,6,0,0,tzinfo=pytz.utc))
        assert not is_price_recovering(date= datetime(2018, 3, 8,6,0,0,tzinfo=pytz.utc))
        assert not is_price_in_freefall(date=datetime(2018, 3, 8,8,0,0,tzinfo=pytz.utc))
        assert is_price_recovering(date=     datetime(2018, 3, 8,8,0,0,tzinfo=pytz.utc))
        assert is_price_recovering(date=     datetime(2018, 3, 8,8,12,0,tzinfo=pytz.utc))

        assert not is_price_in_freefall(date=datetime(2018, 3, 8,9,0,0,tzinfo=pytz.utc))
        assert not is_price_recovering(date= datetime(2018, 3, 8,9,0,0,tzinfo=pytz.utc))

        assert not is_price_in_freefall(date=datetime(2018, 3, 8,10,0,0,tzinfo=pytz.utc))
        assert not is_price_recovering(date= datetime(2018, 3, 8,10,12,0,tzinfo=pytz.utc))


        assert not is_price_in_freefall(date=datetime(2018, 3, 8,11,0,0,tzinfo=pytz.utc))