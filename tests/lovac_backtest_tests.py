''' Tests for backtesting '''
from datetime import datetime, timedelta, date
import pytz

import pandas as pd
from pytz import timezone
import pytest
import mock
import json

def test_calc_investmentdates(client,db_tickerdata_ready_app    ):
    ''' Trying to get backtesting to work ... unsuccessfull for now. Should probably simply throw away this method '''
    #pylint: disable=unused-argument
    from lovac.services.tickerdata import get_tickerdata, _find_index_for_date
    from datetime import datetime, timedelta, date
    app = db_tickerdata_ready_app
    with app.app_context():
        td = get_tickerdata()
        investment_dates_all = pd.date_range(datetime(2018, 1, 1,tzinfo=timezone("UTC")),periods=30,freq='24H')
        # Remove those dates beyond our known data range
        investment_dates = investment_dates_all[investment_dates_all < td["timestamp"][-1]]
        # Get closest business dates with available data
        # This doesn't work and therefore backtesting is for now not to be continued
        #closest_investment_dates = td.index.searchsorted(investment_dates)
        print(td.index)