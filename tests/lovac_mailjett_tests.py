''' Tests for lovac.mailjett'''

import pytest
import mock
import json
import os

from tests.fixtures_faketransactions import FakeResponse
from datetime import datetime
from lovac.models import LovacError
from bitstamp.client import BitstampError



@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_sendmail(app):
    ''' A test for querying the ticker-data
    '''
    from lovac.mailjett import sendmail
    try:
        api_key = os.environ['MJ_APIKEY_PUBLIC']
        api_secret = os.environ['MJ_APIKEY_PRIVATE']
    except Exception as e:
        print(e)
        print("You need to have the api-credentials as environment-variables so that this will work!")
        assert False
    print(sendmail("kneunert@gmail.com","the subject: bla blub",mailtext="the text of the mail\n second line \n\n fourth line!"))
    assert False

@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_confirmationmail(app):
    ''' A test for querying the ticker-data
    '''
    from lovac.mailjett import sendmail
    try:
        
        api_key = os.environ['MJ_APIKEY_PUBLIC']
        api_secret = os.environ['MJ_APIKEY_PRIVATE']
    except Exception as e:
        print(e)
        print("You need to have the api-credentials as environment-variables so that this will work!")
        assert False
    with app.test_request_context():
        print(sendmail("kneunert@gmail.com","Lovac - Confirm your Email-Address",mailtemplate="emailconfirmation",token="sometoken"))
        assert False
