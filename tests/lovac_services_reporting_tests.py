''' Tests stuff in lovac.services.reporting '''

import os
import pytz
import datetime
from datetime import timedelta, datetime
import mock
import pytest

from lovac.models import User, Notification, db

def test_purchase_report(db_ready_app, fresh_user_with_purchases):
    from lovac.services.reporting import purchase_report
    with db_ready_app.test_request_context():
        with mock.patch('lovac.util.lovac_today', return_value=datetime(2018, 2, 19,0,0,0,tzinfo=pytz.utc)):
            purchase_report = purchase_report()
            print("purchase_report: {}".format(purchase_report))
            assert purchase_report['yesterday'] == 12.0
            assert purchase_report['last_7_days'] == 65.89
            assert purchase_report['last_30_days'] == 185.94
