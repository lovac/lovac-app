''' tests the util-functions of lovac '''
import pytest
import mock

from datetime import datetime

import logging
import traceback

def test_print_tracebacks():
    ''' some test '''
    # Let's raise an error
    try:
        some_function()
    except Exception as e:
        print("a MyError is an Exception")
        # An Exception has a __traceback__ and printing it, prints it to stderr
        traceback.print_tb(e.__traceback__)
        # logging can output an exception:
        logging.exception(e)
        # check for the message
        print("the context of an exception:")
        print(e.__context__)
        print("the class of an exception")
        print(e.__class__)
        print("the cause of an exception")
        print(e.__cause__)
        print("the message of an exception")
        print(str(e))
        #print(traceback.extract_tb(e.__traceback__))

    assert False


def some_function():
    ''' muuuh '''
    print("Let's call some other function")
    some_other_function()

def some_other_function():
    ''' muuh '''
    #NameError("the inner cause")
    raise MyError("Ups, something went wrong")


class MyError(Exception):
    ''' Some random custom Error '''
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super(MyError, self).__init__(message)