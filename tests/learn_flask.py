''' 
a Test which helps you to understand Flask in a non-trivial application.
'''
import os
os.environ['APP_SETTINGS']="lovac.config.TestConfig"
import sys
import lovac
import unittest
import tempfile
from flask import Flask

import requests
import mock

class FlaskTests(unittest.TestCase):
    ''' Tests to understand Flask '''
    def test_app(self):
        ''' calling the Flask-constructor '''
        app1 = Flask(__name__)
        # The Flask call obvioulsly should only be called once, it's a constructor!!
        app1.something="somevalue"
        print(str(app1) + " " + app1.something)
        app2 = Flask(__name__)
        # How stupid i have been when i expected this to work?
        with self.assertRaises(AttributeError):
            # AttributeError: 'Flask' object has no attribute 'something'
            print(str(app2.something))
        # We need that app but we don't want to pass it around, we want it magically to be injected
        # There seems to be some magic current_app to import:
        from flask import current_app
        # but this raises a RuntimError
        with self.assertRaises(RuntimeError):
            # RuntimeError: Working outside of application context.
            print(current_app.something)
        # Well, it doesn't if you're working within a flask-view or calling it from there
        # But e.g. in a cronjob, you doesn't have that luxury
        # Contexts might help
        # http://flask-sqlalchemy.pocoo.org/2.3/contexts/
        with app1.app_context():
            print(str(current_app) + " " + current_app.something)
        # Hmmm, that doesn't help us at all because we need the app in order to create the context
        # So maybe the solution is a "Appfactory" which sounds somehow cool
        # http://flask.pocoo.org/docs/0.12/patterns/appfactories/
        # For that we need to create functions within this testfuction.
        # In order to do this, we need to verify some extra-assumptions

    def test_functions_and_classes(self):
        ''' i can create functions within a method '''
        def some_function(value):
            ''' some_function '''
            print("the value is:" + value)
        some_function("abc")
        class SomeClass(object):
            '''  ... and i can create a class within a function of another class '''
            def some_other_function(self,some_value):
                print("somevalue is " + some_value)
        some_object=SomeClass()
        some_object.some_other_function("def")
    
    def test_funtions2(self):
        ''' Another test, another context '''
        with self.assertRaises(NameError):
            some_function("abc")
        with self.assertRaises(NameError):
            some_object=SomeClass()

    def test_appfactories(self):
        ''' We'll use two different classes for configuration '''
        class Config1(object):
            ''' Some Config-Class '''
            DATABASE='./tests/config1.sqlite'
            SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE
        class Config2(object):
            ''' Some other config-Class '''
            DATABASE='./tests/config2.sqlite'
            SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE
        
        # We have to trick a bit here because above classes are only valid in this context
        # but app.config.from_object needs a global visible object
        
        def create_app(config):
            ''' A function which is creating a Flask-APP'''
            lovac.app = Flask(__name__)
            lovac.app.config.from_object(config)

            from lovac.models import db
            db.init_app(lovac.app)

            #from yourapplication.views.admin import admin
            #from yourapplication.views.frontend import frontend
            from lovac.controller import controller
            lovac.app.register_blueprint(controller)
            return lovac.app
        print(__name__)
        app1 = create_app(Config1)
        # That's great but now the problem arises how to get hold on this reference
        # in other context. Which contexts are impacted?
        # 1. whoever "from lovac.models import db" needs to "db.init_app(app)"
        # 2. That's spreading to the controller who has current_app, though
        # 3. the filter.py which are doing something like
        #    from lovac import app
        #    @app.template_filter('bootstrap_notif_mapping')
        # 4. cli-commands like:
        #    @app.cli.command('initdb')
        #    def initdb_command():
        # 5. the login-manager and teardown-moethods in __init__.py
        # 6. services.py does:
        #    from lovac import connect_db
        # The question is: can we create create some sort of Singleton-pattern where lovac.app
        # gets injected from the outside? 
        # The appfactory is recommending another module-indirection:
        # # Here an example exampleapp.py file that creates such an application:
        # # from yourapplication import create_app
        # # app = create_app('/path/to/config.cfg')
        # But we don't want now to make parts of yourapplication now dependent on exampleapp.py
        # But maybe the exampleapp can do this:
        # lovac.app = create_app('/path/to/config.cfg')
        # I doubt it, but that might be due to the imports which need to avoid referring lovac.app1
        # from modules (at import time) but should refer to it at function-call-time.

        # So we're not able to test that approach entirely upfront. So let's create a branch and try it ...
        
        # Great, it works!!!!!!!
        # TBD: describe how, in short:
        # split the creation of the app in 2 parts:
        # 1. lovac.init_app() shich should be brief and immediately make lovac.app available (by the caller)
        # 2. lovac.enrich_app() does all the rest and also enrich the app with functions like app.connect_db()
        # ... and call that stuff from lovac.singleton (or the testcode)


if __name__ == '__main__':
    unittest.main()