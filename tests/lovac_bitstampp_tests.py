''' Tests for lovac.bitstampp '''

import pytest
import mock
import json

from tests.fixtures_faketransactions import FakeResponse
from datetime import datetime
from lovac.models import LovacError
from bitstamp.client import BitstampError



@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_get_ticker():
    ''' A test for querying the ticker-data
    '''
    from lovac.bitstampp import TradingClient, get_ticker
    ticker = get_ticker(quote="usd")
    print(ticker)
    ticker = get_ticker(quote="eur")
    print(ticker)
    from pytz import timezone
    print(datetime.fromtimestamp( int(ticker["timestamp"]),timezone('Europe/Berlin')))
    print(datetime.fromtimestamp( int(ticker["timestamp"]),timezone('UTC') ))
    assert False

@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_get_balance():
    ''' A test for querying the ticker-data
    '''
    from lovac.bitstampp import TradingClient
    import configparser
    config = configparser.ConfigParser()
    import os
    print(os.getcwd())
    config.read('instance/lovac_runtime.conf')
    tc = TradingClient(
        config["LOVACUNITTEST"]["BITSTAMP_USERNAME"],
        config["LOVACUNITTEST"]["BITSTAMP_KEY"],
        config["LOVACUNITTEST"]["BITSTAMP_SECRET"]
    )
    tc.trading_client.get_nonce()
    print(tc.get_balance("eur"))
    assert False

def test_get_transactions(bitstampp_tradingclient):
    ''' Not that useful but at least we have some test
    '''
    fake_transactions = FakeResponse(b'''[{"order_id": 789628842, "btc": "0.00051233", "btc_usd": 11711.1, 
        "eur": 0.0, "usd": "-6.00", "id": 47855738, "fee": "0.02", 
        "type": "2", "datetime": "2018-01-18 18:37:35"}]''')
    with mock.patch('requests.post', return_value=fake_transactions):
        transactions = bitstampp_tradingclient.get_transactions("usd")
        assert transactions[0]["order_id"] == 789628842

def test_buy_btc(bitstampp_tradingclient):
    ''' test buy_btc '''
    fake_purchase = FakeResponse(b'''{"datetime": "2018-01-17 21:19:59.660310", "id": "783497480", "type": "0", "price": "10726.48", "amount": "0.00055880"} 
    ''')
    with mock.patch('requests.post', return_value=fake_purchase):
        purchase = bitstampp_tradingclient.buy_btc(0.00055880, quote="UNTECOIN") # UnitTestCoins as it shouldn't matter!
        assert purchase["id"] == '783497480'

def test_buy_btc_RepackageBitstampError(bitstampp_tradingclient):
    ''' The tradingclient should not expose infra-specific Exceptions but repackage them '''
    with mock.patch('requests.post', side_effect=BitstampError("{'__all__': ['You can only buy 0.00001002 BTC. Check your account balance for details.']}")):
        try:
            purchase = bitstampp_tradingclient.buy_btc(0.00055880,quote="UNTECOIN")
            assert False
        except LovacError as error:
            print("LovacError-string:XXX"+str(error)+"XXX")
            assert str(error).__contains__("{'__all__': ['You can only buy 0.00001002 BTC. Check your account balance for details.']}")
        except BaseException as error:
            print(str(error))
            assert False

        