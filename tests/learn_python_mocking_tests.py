import mock
import pytest

def some_function_to_mock():
        ''' not so interesting, should return 2 '''
        return 5

@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_something():
    with mock.patch('json.dumps') as mock_dumps:
        # in doubt, import as late as poosible
        from json import dumps
        dumps({"wurst":"brot"}, ensure_ascii=True, wurst2="brot2")
        assert mock_dumps.called
        args, kwargs = mock_dumps.call_args
        assert args[0] == {"wurst":"brot"}
        #assert kwargs["wurst"]=="brot"
        print(args)
        print("-------------------------------")
        print(kwargs)
        assert False, "Let's fail to get the printed output!"


#@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_multiple_mock_returns():
    with mock.patch("tests.learn_python_mocking_tests.some_function_to_mock", return_value=2) as mock_some_function_to_mock:
        assert some_function_to_mock() == 2
        assert some_function_to_mock() == 2
        assert some_function_to_mock() == 2
    with mock.patch("tests.learn_python_mocking_tests.some_function_to_mock", side_effect=[2,9,10]) as mock_some_function_to_mock:
        assert some_function_to_mock() == 2
        assert some_function_to_mock() == 9
        assert some_function_to_mock() == 10
