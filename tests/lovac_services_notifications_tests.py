''' Tests stuff in lovac.services.mail '''

import os
import datetime
from datetime import timedelta, datetime
import mock
import pytest

from lovac.models import User, Notification, db

# We're doing our own fixtures here
@pytest.fixture(scope="function")
def fresh_user_with_no_notifs(app):
    ''' a very fresh user created at least password-resetted for each and every call '''
    with app.app_context():
        if app.load_user("fresh_user_with_no_notifs@user.com") == None:
            a_fresh_user = User("fresh_user_with_no_notifs@user.com","bla",False)
        else:
            a_fresh_user = app.load_user("fresh_user_with_no_notifs@user.com")
            a_fresh_user.set_password("bla")
        db.session.add(a_fresh_user)
        db.session.commit()
        if a_fresh_user.notifications != None:
            db.session.add(a_fresh_user)
            meta = db.metadata
            notification = meta.tables["notifications"]
            db.session.execute(
                notification.delete().where(
                    notification.c.user_id == a_fresh_user.email
                )
            )
            db.session.commit()
        return a_fresh_user

def add_notification(user, days_back, resolve=False, purpose="please_configure_bitstampcredentials"):
    ''' convenience method to add maillogs quickly '''
    from lovac.models import db
    #db.session.add()
    sent_at = datetime.now()-timedelta(days=days_back)
    notification = Notification(user, purpose,sent_at=sent_at)
    db.session.add(notification)
    db.session.commit()
    return notification

def test_reset_notifications(db_ready_app, fresh_user_with_no_notifs):
    ''' the reset sets the resolved to False '''
    add_notification(fresh_user_with_no_notifs,3)
    add_notification(fresh_user_with_no_notifs,2)
    add_notification(fresh_user_with_no_notifs,1)
    from lovac.services.notifications import reset_notification
    reset_notification(fresh_user_with_no_notifs, "please_configure_bitstampcredentials")
    for notif in fresh_user_with_no_notifs.notifications:
        assert notif.resolved


def test_consider_notifying_not_twice_a_day(db_ready_app, fresh_user_with_no_notifs):
    ''' tests the consider notifying method '''
    from lovac.services.notifications import consider_notifying
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
        assert mock_send_no_bitstamp_config.called
        # this will also create a Notification
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
        assert not mock_send_no_bitstamp_config.called

def test_consider_notifying_second_day(db_ready_app, fresh_user_with_no_notifs):
    ''' tests the consider notifying method '''
    from lovac.services.notifications import consider_notifying
    add_notification(fresh_user_with_no_notifs,1)
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
        assert mock_send_no_bitstamp_config.called
        # this will also create a Notification
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
        assert not mock_send_no_bitstamp_config.called

def test_consider_notifying_prove_fibonacci(db_ready_app, fresh_user_with_no_notifs):
    ''' tests the consider notifying method is sending mails after the 1,2,3,5,8,13,21 day '''
    from lovac.services.notifications import consider_notifying
    add_notification(fresh_user_with_no_notifs,2)
    add_notification(fresh_user_with_no_notifs,1)
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
        assert mock_send_no_bitstamp_config.called
        # this will also create a Notification
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
        assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=1)
    # will not send on the 4th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=2)
    # will send on the 5th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=3)
    # will not send on the 6th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=4)
    # will not send on the 7th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=5)
    # will send on the 8th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=6)
    # will NOT send on the 9th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=7)
    # will NOT not send on the 10th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=8)
    # will NOT send on the 11th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=9)
    # will NOT send on the 12th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=10)
    # will send on the 13th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=11)
    # will NOT send on the 14th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=12)
    # will NOT send on the 15th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
    today = datetime.now() + timedelta(days=13)
    # will NOT send on the 16th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert not mock_send_no_bitstamp_config.called
        today = datetime.now() + timedelta(days=18)
    # will send on the 21th day
    with mock.patch('lovac.services.mail.send_no_bitstamp_config') as mock_send_no_bitstamp_config:
        with mock.patch('lovac.util.lovac_today', return_value=today):
            consider_notifying(fresh_user_with_no_notifs,"please_configure_bitstampcredentials")
            assert mock_send_no_bitstamp_config.called