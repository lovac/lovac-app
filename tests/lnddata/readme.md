admin.macaroon and tls.cert are valid files but do not match to any lnd-server.
In testing, we don't have a lnd-server and so we rely on timeouts and all tests
should still not fail nevertheless.