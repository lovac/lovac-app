''' Tests stuff in lovac.services.registertoken '''
from datetime import datetime
import time

import mock

from tests.fixtures_faketransactions import FakeResponse

def test_confirmation_tokens(app):
    ''' tests the token funtionality to register your mailaddress '''
    with app.test_request_context():
        from lovac.services.tokenregistering import confirm_token, generate_confirmation_token
        token = generate_confirmation_token("wurst")
        assert confirm_token(token) == "wurst"
        time.sleep(2)
        assert confirm_token(token,expiration=2) == "wurst"
        assert confirm_token(token,expiration=1) == False
        time.sleep(1)
        assert confirm_token(token,expiration=2) == False

def test_confirm_forgotpassword_token(app):
    ''' tests the token functionality which should encapsulate the forgot_password thing '''
    with app.test_request_context():
        from lovac.services.tokenregistering import confirm_forgotpassword_token, generate_forgotpassword_token
        token = generate_forgotpassword_token("wurst")
        assert confirm_forgotpassword_token(token) == "wurst"
        time.sleep(2)
        assert confirm_forgotpassword_token(token,expiration=2) == "wurst"
        assert confirm_forgotpassword_token(token,expiration=1) == False
        time.sleep(1)
        assert confirm_forgotpassword_token(token,expiration=2) == False