''' Tests for lovac.models '''

import pytest
import mock
import json

import codecs

from tests.fixtures_faketransactions import FakeResponse
from datetime import datetime
from lovac.models import Payment, LovacError

def test_notification(client,app,fresh_user_with_bitstampconfig):
    #pylint: disable=unused-argument
    ''' the model test '''
    from lovac.models import db, Notification
    db.session.add(fresh_user_with_bitstampconfig)
    notification = Notification(fresh_user_with_bitstampconfig, "please_confirm_email")
    print(str(notification))
    assert str(notification).startswith("<notification fresh_user_with_bitstampconfig@user.com")
    assert str(notification).endswith("please_confirm_email>")


def test_payments(client,app,fresh_user_with_bitstampconfig):
    #pylint: disable=unused-argument
    ''' the service is not returning puchase-data for some user '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    payment = Payment(fresh_user_with_bitstampconfig,9999)
    assert 9999 == payment.satoshi_amount
    assert 'pending_invoice' == payment.status
    assert not payment.check_payment() # the payment is not yet paid
    class MockResponse(object):
        ''' A mock class '''
        def __init__(self):
            self.r_hash = codecs.decode('mHNnPfjG68J2kf/YYOkjcL9ZJYCwuOD33IYREQxdpNQ='.encode(), 'base64')
            self.payment_request = 'lnsb10u1pd5jp8lpp5npekw00ccm4uya53llvxp6frwzl4jfvqkzuwpa7uscg3zrza5n2qdqu24ek2u3qya6x2um5w4ek2u3nyusqcqzys6h74dkslsztmxe4kly64999ka9v8cqu99r754vfmjjk2ske7m39j43rduyr0mk30g0zru8xj4rm26uv34tsg7rn345cmzalnaccwq6gpv4nylq'
        def AddInvoice(self, something, metadata):
            return self
        def rhash():
            return "lnsb10u1pd5jp8lpp5npekw00ccm4uya53llvxp6frwzl4jfvqkzuwpa7uscg3zrza5n2qdqu24ek2u3qya6x2um5w4ek2u3nyusqcqzys6h74dkslsztmxe4kly64999ka9v8cqu99r754vfmjjk2ske7m39j43rduyr0mk30g0zru8xj4rm26uv34tsg7rn345cmzalnaccwq6gpv4nylq"
    my_mock = {}
    with mock.patch('lovac.lnd.Lnd.generate_invoice', return_value=MockResponse()):
        payment.generate_invoice()
    assert payment.r_hash == 'mHNnPfjG68J2kf/YYOkjcL9ZJYCwuOD33IYREQxdpNQ=\n'
    assert payment.payment_request == 'lnsb10u1pd5jp8lpp5npekw00ccm4uya53llvxp6frwzl4jfvqkzuwpa7uscg3zrza5n2qdqu24ek2u3qya6x2um5w4ek2u3nyusqcqzys6h74dkslsztmxe4kly64999ka9v8cqu99r754vfmjjk2ske7m39j43rduyr0mk30g0zru8xj4rm26uv34tsg7rn345cmzalnaccwq6gpv4nylq'
    assert 'pending_payment' == payment.status
    print(payment.status)
    print(payment.r_hash)
    print(payment.payment_request)
    class MockResponse(object):
        ''' A mock class '''
        def __init__(self, settled):
            self.settled = settled
        def LookupInvoice(self, something, metadata):
            return self
    with mock.patch('lovac.lnd.Lnd.stub', return_value=MockResponse(False)):
        assert not payment.check_payment()
        assert not payment.check_payment()
        assert 'pending_payment' == payment.status
    with mock.patch('lovac.lnd.Lnd.stub', return_value=MockResponse(True)):
        assert payment.check_payment()
        assert payment.status == 'complete'


def test_BtcAddress(client,db_ready_app,fresh_user_with_bitstampconfig):
    #pylint: disable=unused-argument
    from lovac.models import BtcAddress, db
    from lovac.util import lovac_now
    with db_ready_app.app_context():
        db.session.add(fresh_user_with_bitstampconfig)
        btcaddress = BtcAddress(fresh_user_with_bitstampconfig.bitstampconfig.id, lovac_now(),"wurst", 15)
        db.session.add(btcaddress)
        db.session.commit()


def test_InvitationCode(client,db_ready_app,fresh_user_with_bitstampconfig):
    #pylint: disable=unused-argument
    from lovac.models import InvitationCode, db
    from lovac.util import lovac_now
    with db_ready_app.app_context():
        db.session.add(fresh_user_with_bitstampconfig)
        invitation_code = InvitationCode("a-code" ,1, "a comment")
        db.session.add(invitation_code)
        db.session.commit()
        assert invitation_code.users == []
        assert len([]) == 0
        assert not invitation_code.used()
        assert not invitation_code.exhausted()
        fresh_user_with_bitstampconfig.invitationcode = invitation_code
        db.session.add(fresh_user_with_bitstampconfig)
        db.session.commit()
        assert invitation_code.used()
        assert invitation_code.exhausted()



    
