''' tests the util-functions of lovac '''
import pytest
import mock

from datetime import datetime

import lovac

def test_lovac_now():
    ''' test the mockable now() function '''
    with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 4, 1, 12, 30, 45)):
        assert lovac.util.lovac_now().date().year == 2018
        assert lovac.util.lovac_now().date().month == 4
        assert lovac.util.lovac_now().date().day == 1

def test_days_in_current_month():
    ''' test days in current moth '''
    assert lovac.util.days_in_current_month(datetime(2018, 3, 1, 12, 30, 45)) == 31   
    assert lovac.util.days_in_current_month(datetime(2018, 2, 1, 12, 30, 45)) == 28

def first_day_of_month():
    ''' test the first_day_of_month '''
    mydate = lovac.util.first_day_of_month(datetime(2018, 3, 3, 12, 30, 45)).date()
    assert mydate.day == 1
    assert mydate.year == 2018
    assert mydate.month == 3

def test_is_last_day_of_month():
    ''' tests that method '''
    assert not lovac.util.is_last_day_of_month(datetime(2018,2,3))
    assert lovac.util.is_last_day_of_month(datetime(2018,2,28))
    assert lovac.util.is_last_day_of_month(datetime(2018,9,30))
    assert not lovac.util.is_last_day_of_month(datetime(2018,7,30))

def test_is_middle_of_month(date=None):
    ''' tests that method '''
    assert not lovac.util.is_middle_of_month(datetime(2018,2,3))
    assert lovac.util.is_middle_of_month(datetime(2018,2,16))
    assert lovac.util.is_middle_of_month(datetime(2018,9,16))
    assert not lovac.util.is_middle_of_month(datetime(2018,7,30))

def test_handle_exception(db_ready_app, fresh_user_with_nothing):
    ''' tests handle Exception '''
    from lovac.util import handle_exception
    with db_ready_app.app_context():
        from lovac.models import db
        e = Exception()
        handle_exception(e)
        db.session.add(fresh_user_with_nothing)
        handle_exception(e,fresh_user_with_nothing)
