""" pytests for docker-compose
This testfile tests a docker-compose-setup
"""

import pytest
import requests
import os
import json

from requests.exceptions import (
    ConnectionError,
)

def is_responsive(url):
    """Check if something responds to ``url``."""
    try:
        print("trying ..." + url)

        response = requests.get(url)
        print("done")
        print(str(response))
        if response.status_code == 401 or response.status_code == 200:
            return True
    except ConnectionError as ce:
        print(ce)
        return False


@pytest.fixture(scope='session')
def lovac_app_service(docker_ip, docker_services):
    """Ensure that lovac_app_service is up and responsive."""
    url = 'http://{0}:{1}'.format (
        docker_ip,
        docker_services.port_for('web', 5000)
    )
    print("checking ... " + url)
    docker_services.wait_until_responsive(
        timeout=1.0, pause=0.1,
        check=lambda: is_responsive(url)
    )
    return url
    
@pytest.fixture(scope='session')
def docker_compose_file(pytestconfig):
    '''loads the docker-compose-file in the current-directory'''
    docker_compose_file = os.path.join(
        str(pytestconfig.rootdir),
        'tests/docker/lovac-app/'
        'docker-compose.yml'
    )
    print("Using " + docker_compose_file)
    return docker_compose_file


def test_lovac_app_image_smoke(lovac_app_service):
    """Checking whether the getinfo-call responds reasonable"""
    response = requests.get(lovac_app_service)
    print(response.text)
    response.raise_for_status()