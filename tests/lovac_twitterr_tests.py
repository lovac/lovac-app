''' Tests for lovac.mailjett'''

import pytest
import mock
import json
import os

from tests.fixtures_faketransactions import FakeResponse
from datetime import datetime
from lovac.models import LovacError
from bitstamp.client import BitstampError



@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_publish_tweet(app):
    ''' A test for querying the ticker-data
    '''
    from lovac.twitterr import send_tweet
    try:
        api_key = os.environ['TW_API_KEY']
        api_key_secret = os.environ['TW_API_KEY_SECRET']
        access_token = os.environ['TW_ACCESS_TOKEN']
        access_token_secret = os.environ['TW_ACCESS_TOKEN_SECRET']
    except Exception as e:
        print(e)
        print("You need to have the api-credentials as environment-variables so that this will work!")
        assert False
    send_tweet("Hello World! You should do Dollar-Cost-Averaging bitcoin and i can help with that!")
    assert False