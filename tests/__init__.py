import re

def login(client, username, password, nextparam=None, admin=False):
    ''' login helper-function '''
    if nextparam:
        url="/login?next={}".format(nextparam)
    else:
        url="/login"
    if admin:
        url="/admin"+url
    return client.post(url, data=dict(
        username=username,
        password=password
    ), follow_redirects=True)

def logout(client):
    ''' logout helper-method '''
    return client.get('/logout', follow_redirects=True)
    

def register(client,email,password,password2,invitation_code):
    ''' helper method to register easily '''
    return client.post('/register', data=dict(
        email=email,
        password=password,
        confirm=password2,
        invitation_code=invitation_code,
        tos = True
    ), follow_redirects=True)


def print_messages(result_data):
    ''' This extract flash-messages from lovac-html and prints them ''' 
    for message in re.findall("<!-- message -->(.*)", result_data.decode("utf-8") ):
        print("message:  {}".format(message))
