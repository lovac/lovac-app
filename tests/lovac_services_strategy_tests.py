''' Tests stuff in lovac.services.strategy '''
from datetime import datetime

import mock

from tests.fixtures_faketransactions import FakeResponse

def test_summary_purchases(db_ready_app, fresh_user_with_purchases):
    ''' test rhe summary of purchases '''
    from lovac.services.strategy import AccumulationStrategy
    with db_ready_app.app_context():
        from lovac.models import db
        db.session.add(fresh_user_with_purchases)
        strategy = AccumulationStrategy.from_user(fresh_user_with_purchases)
        summary = strategy._summary_purchases(datetime(2018, 4, 1, 12, 30, 45).date(),datetime(2018, 4, 8, 12, 30, 45).date())
        assert summary["fiat_sum"] == 6
        summary = strategy._summary_purchases(datetime(2018, 4, 1, 12, 30, 45).date(),datetime(2018, 4, 17, 12, 30, 45).date())
        assert summary["fiat_sum"] == 42

def test_calculate_purchase_amount(db_ready_app, fresh_user_with_bitstampconfig, fresh_user_with_purchases):
    ''' test the calculate purchase amount '''
    from lovac.services.strategy import AccumulationStrategy
    with db_ready_app.app_context():
        from lovac.models import db
        db.session.add(fresh_user_with_bitstampconfig)
        db.session.add(fresh_user_with_purchases)
        strategy = AccumulationStrategy.from_user(fresh_user_with_bitstampconfig)
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 1, 13, 12, 30, 45)):
            assert strategy.calculate_purchase_amount() == 25 # Would be first time ever
        strategy = AccumulationStrategy.from_user(fresh_user_with_purchases)
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 1, 14, 12, 30, 45)):
            assert strategy.calculate_purchase_amount() == 0 # have bought already
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 1, 15, 12, 30, 45)):
            print("check calculate_purchase_amount for " + str(datetime(2018, 1, 15, 12, 30, 45).date()))
            assert strategy.calculate_purchase_amount() == 6
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 1, 25, 12, 30, 45)):
            print("check calculate_purchase_amount for " + str(datetime(2018, 1, 25, 12, 30, 45).date()))
            assert strategy.calculate_purchase_amount() == 0
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 2, 10, 12, 30, 45)):
            print("check calculate_purchase_amount for " + str(datetime(2018, 2, 10, 12, 30, 45).date()))
            assert strategy.calculate_purchase_amount() == 24
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 4, 1, 12, 30, 45)):
            assert strategy.calculate_purchase_amount() == 6
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 4, 8, 12, 30, 45)):
            assert strategy.calculate_purchase_amount() == 42
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 4, 29, 12, 30, 45)):
            assert strategy.calculate_purchase_amount() == 114