''' test for the lovac module '''
import os
os.environ['APP_SETTINGS']="lovac.config.TestConfig"
import sys
import lovac
import unittest
import tempfile

import json

import requests
import mock
from flask_testing import TestCase

def login(client, username, password):
    ''' login for admins '''
    return client.post('/admin/login', data=dict(
        username=username,
        password=password
    ), follow_redirects=True)

def logout(client):
    ''' logout for admins '''
    return client.get('/admin/logout', follow_redirects=True)

def test_not_accessable(client, user_jwt_token_response):
    ''' not accessable, neither for anonymous nor logged-in-non-admin-users '''
    response = client.get('/admin/dashboard', follow_redirects=True)
    print(str(response))
    assert response.status == '200 OK'
    print(user_jwt_token_response.get_data(as_text=True))
    access_token = json.loads(user_jwt_token_response.get_data(as_text=True))["access_token"]
    response = client.get('/api/v1alpha/hello', headers={'Authorization': 'Bearer '+access_token},
        follow_redirects=True
    )
    assert response.status == '401 UNAUTHORIZED'

def test_login_logout(client):
    ''' login, access secured resource, logout, login with wrong credentials '''
    result = login(client,'admin', 'derida99')
    assert b'Admin Dashboard' in result.data
    result = logout(client)
    assert b'You were logged out' in result.data
    result = login(client,'adminx', 'derida99')
    assert b'Invalid username or password' in result.data
    result = login(client,'admin', 'defaultx')
    assert b'Invalid username or password' in result.data


def test_create_new_user(client, fresh_user_with_adminrights):
    ''' you can create a non-admin-user but that user won't be able to admin-login'''
    lovac.models.db.session.add(fresh_user_with_adminrights)
    response = login(client,fresh_user_with_adminrights.email, 'bla')
    response = client.post('/admin/newuser', data=dict(
        email="testuser21",
        password="somepassword",
        admin=False
    ), follow_redirects=True)
    assert response.status == "200 OK"
    logout(client)
    response = login(client,'testuser21', 'somepasssword')
    assert response.status == "401 UNAUTHORIZED"
    assert not b'Admin Dashboard' in response.data
    assert b'Invalid username or password' in response.data


def test_delete_user(db_ready_app, client, fresh_user_with_adminrights):
    ''' you can delete a user '''
    from lovac.services import create_new_user, load_user
    with db_ready_app.app_context():
        create_new_user("testuser22","testuser11")
        assert load_user("testuser22") != None
    lovac.models.db.session.add(fresh_user_with_adminrights)
    response = login(client,fresh_user_with_adminrights.email, 'bla')
    response = client.post('/admin/deluser', data=dict(
        email="testuser22"
    ), follow_redirects=True)
    assert response.status == "200 OK"
    assert load_user("testuser22") == None

def test_endmarker():
    ''' no one knows why this test is needed '''
    pass
