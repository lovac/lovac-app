'''
testing tickerdata
'''
from pandas import DataFrame

from datetime import datetime, timezone
from pytz import timezone


def test_csv_2_df(app):
    ''' test the c2v to fataframe function, verifiing how the csv looks like '''
    import lovac.tickerdata
    with app.app_context():
        df = lovac.tickerdata.csv_2_df("td-test.csv")
        print(df.shape)
        assert df.shape[0] == 316
        # let's check the headers:
        assert df.columns[1] == "timestamp"
        # it start on 2017-11-21
        assert datetime.fromtimestamp(df.iloc[0]["timestamp"]).strftime('%Y-%m-%d').startswith("2017-11-21")
        # ends 2017-12-04
        assert datetime.fromtimestamp(df.iloc[-1]["timestamp"]).strftime('%Y-%m-%d').startswith("2017-12-04")

def test_csv_2_df_otherdataset(app):
    ''' test the c2v to fataframe function, verifiing how the csv looks like'''
    import lovac.tickerdata
    with app.app_context():
        td = lovac.tickerdata.csv_2_df("td-test2.csv")
        print(td.shape)
        assert td.shape[0] == 777
        # let's check the headers:
        assert td.columns[1] == "timestamp"
        # it start on 2018-02-28
        #print(datetime.fromtimestamp(df.iloc[0]["timestamp"]).strftime('%Y-%m-%d'))
        assert datetime.fromtimestamp(td.iloc[0]["timestamp"]).strftime('%Y-%m-%d').startswith("2018-02-28")
        # ends 2018-04-02
        print(td.iloc[-1]["timestamp"]) # Unixtimestamps are probably UTC
        print(datetime.fromtimestamp(td.iloc[-1]["timestamp"]).strftime('%Y-%m-%d')) # This might be local time
        assert datetime.fromtimestamp(td.iloc[-1]["timestamp"]).strftime('%Y-%m-%d').startswith("2018-04-01")
        print(td[0:3]["timestamp"])
        assert td.iloc[0]["timestamp"] == 1519790400 #02/28/2018 @ 4:00am (UTC)
        assert td.iloc[99]["timestamp"]==1520146800 # <-- the timestamp in row 101 !!
        assert td.iloc[195]["timestamp"] == 1520492400 # 03/08/2018 @ 7:00am (UTC)
        assert td.iloc[195]["weightedprice"] == 9921.07817699265 

def test_csv_2_db(db_ready_app):
    ''' tests that the data from csv will end up in the db in a correct way '''
    import lovac.tickerdata
    with db_ready_app.app_context():
        df = lovac.tickerdata.csv_2_df("td-test2.csv")
        from lovac.tickerdata import csv_2_db
        csv_2_db("td-test2.csv")
#1445,1520485200,9781.33,9840.0,9684.0,9796.28,715.88949077,6995758.510283493,9772.120698068862,btc,bitstamp,9796.28
# ==> 03/08/2018 @ 5:00am (UTC) 
#1446,1520488800,9809.57,9869.0,9728.0,9761.97,515.8508401,5046740.841517369,9783.333570880757,btc,bitstamp,9761.97
# ==> 03/08/2018 @ 6:00am (UTC)
#1447,1520492400,9752.93,10002.18,9701.28,9971.14,1283.87532904,12737427.508917993,9921.07817699265,btc,bitstamp,9971.14
# ==> 03/08/2018 @ 7:00am (UTC) which is March 8, 2018 8:00:00 AM GMT+01:00
#1448,1520496000,9970.59,9999.96,9853.45,9904.99,581.01589535,5763005.917889092,9918.843811351322,btc,bitstamp,9904.99
#1449,1520499600,9913.99,10150.0,9900.0,10073.11,1305.46287028,13146359.649614628,10070.266990278284,btc,bitstamp,10073.11
# As a result
        from lovac.models import db
        from lovac.tickerdata.models import TickerHourly
        ticker_hourly = db.session.query(TickerHourly).all()
        assert len(ticker_hourly) == 777
        from lovac.services.tickerdata import get_tickerdata
        td = get_tickerdata()
        assert td.shape[0] == 657 # 777 -120 (which are substracted for the ma)
        
        one_row = db.session.query(TickerHourly).filter(TickerHourly.timestamp == datetime(2018,3,8,8,0,0,tzinfo=timezone('UTC'))).all()
        assert one_row[0].timestamp == datetime(2018,3,8,8,0,0)
        assert one_row[0].weightedprice == 9921.07817699265 
        # As a result, the wrong value comes from querying and get_tickerdata
        
        assert td.iloc[75]["timestamp"] == datetime(2018,3,8,8,0,0,tzinfo=timezone("UTC")) 
        assert td.iloc[75]["weightedprice"] == 9921.07817699265 
    
def test_fromtimestamp():
    ''' i hope that's it! '''    
    # this won't work without the tz/tzinfo
    mydatetime = datetime.fromtimestamp(1520492400, tz=timezone("UTC"))  # 03/08/2018 @ 7:00am (UTC)
    assert mydatetime == datetime(2018,3,8,7,0,0, tzinfo=timezone("UTC"))



    
    