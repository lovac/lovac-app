''' Tests for lovac.services '''
import pytz
import pytest
import mock
import json

from tests.fixtures_faketransactions import FakeResponse
from datetime import datetime
from lovac.models import LovacError


def test_bought_today(client, app):
    ''' bought today '''
    #pylint: disable=unused-argument
    from lovac.services.purchase import bought_today
    with app.app_context():
        result = bought_today()
        print(result)
        assert result == False


def test_buy_the_dip_job(client, db_tickerdata_ready_app, fresh_user_with_bitstampconfig, fresh_user_with_purchases):
    ''' tests the buy_the_dip_job '''
    from lovac.services.purchase import buy_the_dip_job
    from lovac.models import db, Purchase
    db.session.add(fresh_user_with_purchases)
    db.session.add(fresh_user_with_bitstampconfig)
    fresh_user_with_bitstampconfig.bitstampconfig.strategy=u'buy_the_dip'
    fresh_user_with_purchases.bitstampconfig.strategy=u'buy_the_dip'
    db.session.add(fresh_user_with_purchases)
    db.session.add(fresh_user_with_bitstampconfig)
    db.session.commit()
    
    with db_tickerdata_ready_app.app_context():
        with mock.patch('lovac.services.purchase.automatic_buy', return_value=Purchase(fresh_user_with_purchases.bitstampconfig, datetime(2018, 3, 7,18,12,0,tzinfo=pytz.utc), 12,13,14,15)) as mock_automatic_buy:
            with mock.patch('lovac.util.lovac_now', return_value=datetime(2018, 3, 8,8,0,0,tzinfo=pytz.utc)):
                btc_sum, fiat_sum = buy_the_dip_job()
                assert btc_sum == 26
                assert fiat_sum["eur"]==0
                assert fiat_sum["usd"]==24


def test_automatic_buy(client, app):
    ''' test the services.automatic_buy 
        mocking: the buy-call and second time also the bought_today method
    '''
    #pylint: disable=unused-argument
    from lovac.services.purchase import automatic_buy
    from lovac.models import User, Bitstampconfig
    user = User("wurst1","doof","usd")
    try:
        automatic_buy(user)
        raise AssertionError("Should raise an exception")
    except LovacError:
        pass

    with mock.patch('lovac.services.purchase.buy') as mock_buy:
        user = User("wurst","doof","usd")
        user.bitstampconfig = Bitstampconfig("user0","bla","blub","blob", "usd")
        user.bitstampconfig.dailybuy = 9
        with app.app_context(): 
            automatic_buy(user)
    with mock.patch('lovac.services.purchase.buy') as mock_buy:
        user = User("wurst","doof",False)
        user.bitstampconfig = Bitstampconfig("user0","bla","blub","blob","usd", False)
        user.bitstampconfig.dailybuy = 9
        with app.app_context(): 
            try:
                automatic_buy(user)
                raise AssertionError("If automatic_buy_flag is false, automatic_buy is not supposed to be called!")
            except LovacError:
                pass
    with mock.patch('lovac.services.purchase.buy') as mock_buy:
        with mock.patch('lovac.services.purchase.bought_today', return_value=True) as mock_bought_today:
            user = User("wurst","doof",False)
            user.bitstampconfig = Bitstampconfig("user0","bla","blub","blob", "usd")
            automatic_buy(user)
            assert mock_bought_today.call_count == 1
            assert not mock_buy.called # if you have already bought today, you can't do it again
    with mock.patch('lovac.services.purchase.buy') as mock_buy:
        with mock.patch('lovac.services.purchase.decrease_defer_counter') as mock_decrease_defer_counter:
            with mock.patch('lovac.services.purchase.bought_today', return_value=False) as mock_bought_today:
                user = User("wurst","doof",False)
                user.bitstampconfig = Bitstampconfig("user0","bla","blub","blob", "usd")
                user.bitstampconfig.delay_buy_counter = 1
                automatic_buy(user)
                assert mock_bought_today.call_count == 1
                assert not mock_buy.called # If you have deferred the buy, you won't buy
                assert mock_decrease_defer_counter.called # and your defer-counter got decreased

def test_buy(client,app, user0):
    ''' test the services.buy TWICE (second time is a regression test) 
        As the local_db_test is not done in the buy-method, this will work
        with faking the response from bitstamp.
    '''
    #pylint: disable=unused-argument
    from lovac.models import User, Purchase
    from lovac.services.purchase import buy
    # This is actually a workaround. We're using json.loads here to mock the lovac.bitstampp.TradingClient.buy_btc
    # Would have been better to make this an integration-test and mock requests.post, however we already have 
    # "reserved" that moch for "bought_today_fakeresponse"
    # ToDo: maybe it would have been better to do it the other way around or add another test
    # which is doing it the other way around: mock bought_today and requests.post to test the integration with 
    # buy_btc
    fake_buy = json.loads('''{"id": "986683407", "datetime": "2018-02-18 23:22:50.709624", "price": "10452.14", "amount": "0.00057400", "type": "0"} 
    ''')
    fake_buy["datetime"] = datetime.strptime(fake_buy['datetime'],"%Y-%m-%d %H:%M:%S.%f")
    fake_buy2 = json.loads('''{"id": "986683408", "datetime": "2018-02-18 23:23:50.709624", "price": "10453.14", "amount": "0.00047400", "type": "0"} 
    ''')
    fake_buy2["datetime"] = datetime.strptime(fake_buy2['datetime'],"%Y-%m-%d %H:%M:%S.%f")
    bought_today_fakeresponse = FakeResponse(b'''[
        {"datetime" : "2012-10-10 10:12:31"}]''')
    with app.app_context():
        user = User.query.filter_by(email="user0")[0]
        assert str(user.email) == "user0"
        with mock.patch('requests.post', return_value=bought_today_fakeresponse):
            with mock.patch('lovac.bitstampp.TradingClient.buy_btc', return_value=fake_buy):
                result = buy(user=user)
        assert isinstance(result,Purchase) , "should be of type Purchase"
        assert str(result.datetime) == "2018-02-18 23:22:50.709624"
        assert user.bitstampconfig.purchases[0].btc == 0.000574
        with mock.patch('requests.post', return_value=bought_today_fakeresponse):
            with mock.patch('lovac.bitstampp.TradingClient.buy_btc', return_value=fake_buy2):
                result = buy(user=user)
        assert isinstance(result,Purchase) , "should be of type Purchase"
        assert str(result.datetime) == "2018-02-18 23:23:50.709624"
        assert user.bitstampconfig.purchases[1].btc == 0.00047400
        assert user.bitstampconfig.purchases[0].btc == 0.000574
        assert user.autostatus == 10

def test_increase_defer_counter(db_ready_app,user0):
    ''' tests the increase '''
    from lovac.models import User
    from lovac.services.purchase import increase_defer_counter, decrease_defer_counter
    with db_ready_app.app_context():
        user = User.query.filter_by(email="user0")[0]
        assert user.email == "user0"
        with mock.patch('lovac.util.lovac_now', return_value=datetime(2003, 8, 29, 12, 30, 45)):
            increase_defer_counter(user)
            print("defer_counter =" + str(user.bitstampconfig.delay_buy_counter))
            assert user.bitstampconfig.delay_buy_counter == 1
            increase_defer_counter(user)
            print("defer_counter =" + str(user.bitstampconfig.delay_buy_counter))
            assert user.bitstampconfig.delay_buy_counter == 2
            increase_defer_counter(user)
            print("defer_counter =" + str(user.bitstampconfig.delay_buy_counter))
            assert user.bitstampconfig.delay_buy_counter == 2
            increase_defer_counter(user)
            increase_defer_counter(user)
            increase_defer_counter(user)
            assert user.bitstampconfig.delay_buy_counter == 2
            decrease_defer_counter(user)
            assert user.bitstampconfig.delay_buy_counter == 1
            decrease_defer_counter(user)
            assert user.bitstampconfig.delay_buy_counter == 0
            decrease_defer_counter(user)
            assert user.bitstampconfig.delay_buy_counter == 0
            
def test_update_autostatus(db_ready_app, fresh_user_with_purchases):
    ''' test the update autostatus '''
    from lovac.services.purchase import update_autostatus
    with db_ready_app.app_context():
        from lovac.models import db
        db.session.add(fresh_user_with_purchases)
        update_autostatus(fresh_user_with_purchases,12)
        assert fresh_user_with_purchases.autostatus==12