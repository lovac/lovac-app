""" pytests for Flask
Fixtures are way more fficient then the typical setUp/tearDown stuff
https://docs.pytest.org/en/latest/fixture.html 
"""

import pytest

import os
import sys
import lovac
import unittest
import tempfile

import json

import requests
import mock
from flask_testing import TestCase
from lovac.config import TestConfig

def test_api_root_endpoint(client):
    ''' do we have the API-endpoint-Documentation page up '''
    result = client.get('/api', follow_redirects=True)
    assert b'API' in result.data
    assert b'Message' in result.data

def test_api_smoketest(client):
    ''' the smoketest-endpoint is up '''
    result = client.get('/api/smoketest', follow_redirects=True)
    assert b'i am alive' in result.data

def test_api_pricedata(client, db_tickerdata_ready_app):
    ''' the pricedata-endpoint is up and returns json'''
    #pylint: disable=unused-argument
    result = client.get('/api/v1alpha/prices/btcusd', follow_redirects=True)
    assert json.loads(result.get_data(as_text=True))
    myJson = json.loads(result.get_data(as_text=True))



def test_api_pricedata_year(client, db_tickerdata_ready_app):
    ''' the pricedata-endpoint is up and returns json'''
    #pylint: disable=unused-argument
    result = client.get('/api/v1alpha/prices/btcusd/2017', follow_redirects=True)
    print(result.get_data(as_text=True))
    my_json = json.loads(result.get_data(as_text=True))
    print(str(my_json[0]))
    assert "2017" in my_json[0]["timestamp"]

def test_api_pricedata_month(client, db_tickerdata_ready_app):
    ''' the pricedata-endpoint is up and returns json'''
    #pylint: disable=unused-argument
    result = client.get('/api/v1alpha/prices/btcusd/2017/12', follow_redirects=True)
    my_json = json.loads(result.get_data(as_text=True))
    assert "2017-12" in my_json[0]["timestamp"]
    assert "2017-12" in my_json[-1]["timestamp"]

def test_api_pricedata_day(client, db_tickerdata_ready_app):
    ''' the pricedata-endpoint is up and returns json'''
    #pylint: disable=unused-argument
    result = client.get('/api/v1alpha/prices/btcusd/2017/11/30', follow_redirects=True)
    print(result.get_data(as_text=True))
    #print(type(result))
    #print(dir(result))
    
    # Prio1, get the build back green
    my_json = json.loads(result.get_data(as_text=True))
    assert "2017-11-30" in my_json[0]["timestamp"]
    assert "2017-11-30" in my_json[-1]["timestamp"]
