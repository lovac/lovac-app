""" pytests for Flask
Fixtures are way more fficient then the typical setUp/tearDown stuff
https://docs.pytest.org/en/latest/fixture.html 
"""

import pytest

import os
import sys
import lovac
import unittest
import tempfile

import json

import requests
import mock
from flask_testing import TestCase
from lovac.config import TestConfig

# def test_dummy(app):
#     print("---------------------")
#     print(app.extensions)
#     assert False


def test_register(client):
    ''' testing the registration '''
    result = client.post('/api/v1alpha/register', data=dict(
        username="someusername",
        password="somepassword"
    ), follow_redirects=True)
    assert b'someusername' in result.data

def test_api_login_bad(client):
    ''' testing the login '''
    result = client.post('/api/v1alpha/auth', data=dict(
        username="admin",
        password="badtry"
    ), follow_redirects=True)
    assert b'Wrong credentials' in result.data

def test_api_login_good_admin(admin_jwt_token_response):
    ''' testing the login for an admin'''
    assert b'Logged in as admin' in admin_jwt_token_response.data
    assert b'access_token' in admin_jwt_token_response.data
    assert b'refresh_token' in admin_jwt_token_response.data

def test_api_login_good_user(user_jwt_token_response):
    ''' testing the login for a user'''
    assert b'Logged in as user0' in user_jwt_token_response.data
    assert b'access_token' in user_jwt_token_response.data
    assert b'refresh_token' in user_jwt_token_response.data

def test_api_jwt_shielded_no_access(client):
    ''' do not login, get no access to a shielded resource '''
    result = client.get('/api/v1alpha/hello',
        follow_redirects=True
    )
    assert result.status == '401 UNAUTHORIZED'

def test_api_jwt_shielded(admin_jwt_token_response, client):
    ''' login, get token and access a shielded resource '''
    access_token = json.loads(admin_jwt_token_response.get_data(as_text=True))["access_token"]
    result = client.get('/api/v1alpha/hello', headers={'Authorization': 'Bearer '+access_token},
        follow_redirects=True
    )
    assert b'admin' in result.data

def test_api_jwt_refresh_token(admin_jwt_token_response, client):
    ''' login, get token and refresh_token '''
    refresh_token = json.loads(admin_jwt_token_response.get_data(as_text=True))["refresh_token"]
    result = client.post('/api/v1alpha/refresh', headers={'Authorization': 'Bearer '+refresh_token},
        follow_redirects=True
    )
    print(result.data)
    assert b'refresh_token' in admin_jwt_token_response.data