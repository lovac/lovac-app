""" pytests for Flask
Fixtures are way more fficient then the typical setUp/tearDown stuff
https://docs.pytest.org/en/latest/fixture.html 
"""

import pytest

import os
import sys
import lovac
import unittest
import tempfile

import json

import requests
import mock
from flask_testing import TestCase
from lovac.config import TestConfig
from lovac.bitstampp import TradingClient
from lovac.models import User, InvitationCode, Notification
from tests.fixtures_faketransactions import fake_transactions

#@pytest.fixture(scope="module",params=["lovac.config.TestConfig","lovac.config.CockroachBasedConfig"])
@pytest.fixture(scope="module",params=["lovac.config.TestConfig"])
def app(request):
    ''' the Flask-App, but uninitialized '''
    app = lovac.create_app(request.param)
    #app = lovac.create_app("lovac.config.CockroachDevelopmentConfig")
    app.config["TESTING"]=True
    app.testing = True
    app.app_context().push()
    lovac.init_app(app)
    return app

@pytest.fixture(scope="module")
def db_ready_app(app):
    ''' the Flask-App initialized '''
    lovac.init_db(app, purge_db=True)
    return app

@pytest.fixture(scope="module")
def db_tickerdata_ready_app(db_ready_app):
    ''' the Flask-App initialized with 200 days of tickerdata '''

    # Let's load some ticker-test-data. This set has data for:
    # 120 days of the very beginning which will get cut down in order to get a 20day-MA
    # 2017.11.30 - one day in dec
    # 2017.12.21
    lovac.tickerdata.csv_2_db("td-test.csv")
    # This dataset is March 2018 and it has very many buy-the-dip-points
    lovac.tickerdata.csv_2_db("td-test2.csv")
    return db_ready_app

@pytest.fixture(scope="function")
def fresh_user_with_nothing(app):
    ''' a very fresh user created at least password-resetted for each and every call '''
    with app.app_context():
        if app.load_user("fresh_user_with_nothing@user.com") == None:
            a_fresh_user = User("fresh_user_with_nothing@user.com","bla",False)
        else:
            a_fresh_user = app.load_user("fresh_user_with_nothing@user.com")
            a_fresh_user.set_password("bla")
        lovac.models.db.session.add(a_fresh_user)
        lovac.models.db.session.commit()
        return a_fresh_user

@pytest.fixture(scope="function")
def fresh_user_with_bitstampconfig(app):
    ''' a very fresh user created at least password-resetted for each and every call '''
    with app.app_context():
        if app.load_user("fresh_user_with_bitstampconfig@user.com") == None:
            a_fresh_user = User("fresh_user_with_bitstampconfig@user.com","bla",False)
        else:
            a_fresh_user = app.load_user("fresh_user_with_bitstampconfig@user.com")
            a_fresh_user.set_password("bla")
        lovac.models.db.session.add(a_fresh_user)
        lovac.models.db.session.commit()
        if a_fresh_user.bitstampconfig == None:
            a_fresh_user.bitstampconfig = lovac.models.Bitstampconfig( "fresh_user_with_bitstampconfig@user.com","bla3","freshblub2","blob2","usd")
            lovac.models.db.session.add(a_fresh_user.bitstampconfig)
            lovac.models.db.session.commit()
        return a_fresh_user

@pytest.fixture(scope="function")
def fresh_user_with_adminrights(app):
    ''' a very fresh adminuser at least password-resetted for each and every call '''
    with app.app_context():
        if app.load_user("fresh_user_with_adminrights@user.com") == None:
            a_fresh_user = User("fresh_user_with_adminrights@user.com","bla",admin=True)
            lovac.models.db.session.add(a_fresh_user)
            lovac.models.db.session.commit()
            return a_fresh_user
        else:
            a_fresh_user = app.load_user("fresh_user_with_adminrights@user.com")
            a_fresh_user.set_password("bla")
            lovac.models.db.session.add(a_fresh_user)
            lovac.models.db.session.commit()
            return a_fresh_user

@pytest.fixture(scope="function")
def client(db_ready_app):
    ''' a test_client from an initialized Flask-App '''
    return db_ready_app.test_client()

@pytest.fixture(scope="function")
def admin_jwt_token_response(client):
    ''' a authenticated admin-user from an initialized Flask-App '''
    response = client.post('/api/v1alpha/auth', data=dict(
        username="admin",
        password="derida99"
    ), follow_redirects=True)
    return response

@pytest.fixture(scope="function")
def user_jwt_token_response(client):
    ''' a authenticated non-admin-user from an initialized Flask-App '''
    response = client.post('/api/v1alpha/auth', data=dict(
        username="user0",
        password="0re5u"
    ), follow_redirects=True)
    return response

@pytest.fixture(scope="function")
def user0(db_ready_app):
    ''' Give you a user0 which has a nonvalid bitstampconfig '''
    with db_ready_app.app_context():
        if lovac.models.Bitstampconfig.query.filter_by(user_id="user0").count()==0:
            user0 = lovac.models.User.query.filter_by(email="user0")[0]
            bitstamp_config = lovac.models.Bitstampconfig("user0","bla","blub","blob","usd")
            lovac.models.db.session.add(bitstamp_config)
            lovac.models.db.session.add(user0)
            lovac.models.db.session.commit()
        user0 = User.query.filter_by(email="user0")[0]
        print(user0.bitstampconfig)
        lovac.models.db.session.add(user0)
        return user0

@pytest.fixture(scope="module")
def invcode(db_ready_app):
    ''' Gives you an invcode called "invcode" '''
    with db_ready_app.app_context():
        if lovac.models.InvitationCode.query.filter_by(code="invcode").count()==0:
            invcode = InvitationCode("invcode",1,"incode test")
            lovac.models.db.session.add(invcode)
            lovac.models.db.session.commit()
        return lovac.models.InvitationCode.query.filter_by(code="invcode")[0]

@pytest.fixture(scope="module")
def invcode2(db_ready_app):
    ''' Gives you an invcode called "invcode2" '''
    with db_ready_app.app_context():
        if lovac.models.InvitationCode.query.filter_by(code="invcode2").count()==0:
            invcode = InvitationCode("invcode2",1,"incode2 test")
            lovac.models.db.session.add(invcode)
            lovac.models.db.session.commit()
        return lovac.models.InvitationCode.query.filter_by(code="invcode2")[0]




@pytest.fixture(scope="function")
def fresh_user_with_purchases(db_ready_app, fake_transactions):
    ''' a very fresh user created at least password-resetted for each and every call '''
    with db_ready_app.app_context():
        if db_ready_app.load_user("fresh_user_with_purchases@user.com") == None:
            a_fresh_user = User("fresh_user_with_purchases@user.com","bla",False)
        else:
            a_fresh_user = db_ready_app.load_user("fresh_user_with_purchases@user.com")
            a_fresh_user.set_password("bla")
        lovac.models.db.session.add(a_fresh_user)
        lovac.models.db.session.commit()
        if a_fresh_user.bitstampconfig == None:
            a_fresh_user.bitstampconfig = lovac.models.Bitstampconfig( "fresh_user_with_purchases@user.com","bla2","freshblub3","theSecret","usd")
        lovac.models.db.session.add(a_fresh_user.bitstampconfig)
        lovac.models.db.session.commit()
        from lovac.services.purchase import sync_bitstamp_transactions
        with mock.patch('requests.post', return_value=fake_transactions):
            print("syncing .... syncing .... syncing")
            db_ready_app.logger.debug("Syncing fake-transactions ....")
            sync_bitstamp_transactions(a_fresh_user, purge=True)
        return a_fresh_user

@pytest.fixture(scope="module")
def adminuser(db_ready_app, fake_transactions):
    ''' Give you the admin-user to do tests '''
    with db_ready_app.app_context():
        user = User.query.filter_by(email="admin")[0]
        lovac.models.db.session.add(user)
        return user

@pytest.fixture(scope="function")
def bitstampp_tradingclient(db_ready_app,user0):
    ''' 
    Gives you a bitstamp Tradingclient initialized with a nonvalid bitstampconfig from user0
    This will only work if you mock the requests
    '''
    with db_ready_app.app_context():
        return TradingClient.from_user(user0)