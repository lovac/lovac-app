''' Tests stuff in lovac.services.mail '''

import os

import mock
import pytest

def ensure_mailconfig():
    ''' throw an exception if we can't sen mails '''
    try:
        api_key = os.environ['MJ_APIKEY_PUBLIC']
        api_secret = os.environ['MJ_APIKEY_PRIVATE']
    except Exception as e:
        print(e)
        print("You need to have the api-credentials as environment-variables so that this will work!")
        assert False

def change_mailaddress_for_user(user,newmailaddress):
    ''' convenience to short the tests '''
    from lovac.models import User, Bitstampconfig, db
    # WTF is so difficult here?!
    db.session.add(user)
    if user.bitstampconfig:
        db.session.add(user.bitstampconfig)
        bsc = user.bitstampconfig
        assert user.bitstampconfig.currency != None
    user.email=newmailaddress
    db.session.commit()
    db.session.add(user)
    if user.bitstampconfig:
        user.bitstampconfig = bsc
        db.session.add(bsc)
        assert user.bitstampconfig.currency != None


def test_send_confirmationmail(app):
    ''' tests send_confirmationmail '''
    app.config["SEND_MAILS"] = True # later we'll assert that mock_mailjet.called
    from lovac.services.mail import send_confirmationmail
    from lovac.models import User
    with app.test_request_context():
        with mock.patch('lovac.sendgridd.sendmail') as mock_sendgridd:
            send_confirmationmail("sometestuser@gmail.com")
            assert mock_sendgridd.called
            args, kwargs = mock_sendgridd.call_args
            print(kwargs)
            assert args[0] == 'sometestuser@gmail.com'
            assert args[1] == 'Confirm your Email-Address'

def test_send_forgotpasswordmail(app):
    ''' tests send_forgotpasswordmail '''
    app.config["SEND_MAILS"] = True # later we'll assert that mock_mailjet.called
    from lovac.services.mail import send_forgotpasswordmail
    from lovac.models import User
    with app.test_request_context():
        with mock.patch('lovac.sendgridd.sendmail') as mock_sendgridd:
            send_forgotpasswordmail("sometestuser@gmail.com")
            assert mock_sendgridd.called
            args, kwargs = mock_sendgridd.call_args
            print(kwargs)
            assert args[0] == 'sometestuser@gmail.com'
            assert args[1] == 'Forgot your password?'


@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_send_fiat_shortage_testmail(db_ready_app,fresh_user_with_bitstampconfig):
    ''' intended to really send out a mail! '''
    from lovac.sendgridd import sendmail
    ensure_mailconfig()
    with db_ready_app.test_request_context():
        from lovac.services.mail import send_fiat_shortage
        #db_ready_app.config["SEND_MAILS"] = True
        change_mailaddress_for_user(fresh_user_with_bitstampconfig,"kneunert@gmail.com")
        send_fiat_shortage(fresh_user_with_bitstampconfig)
        assert False

def test_send_fiat_shortage(db_ready_app,fresh_user_with_bitstampconfig):
    ''' tests send_confirmationmail ''' 
    db_ready_app.config["SEND_MAILS"] = True # later we'll assert that mock_mailjet.called
    from lovac.services.mail import send_fiat_shortage
    with db_ready_app.app_context(): # do not use test_request_context() as we're firing that from a "cronjob"
        with mock.patch('lovac.sendgridd.sendmail') as mock_sendgridd:
            from lovac.models import db
            db.session.add(fresh_user_with_bitstampconfig)
            send_fiat_shortage(fresh_user_with_bitstampconfig)
            assert mock_sendgridd.called
            args, kwargs = mock_sendgridd.call_args
            print(kwargs)
            assert args[0] == fresh_user_with_bitstampconfig.email
            assert args[1] == "Fiat shortage in your Exchange account"
            #assert  "transfer USD from your main-account " in args[2]

@pytest.mark.skip(reason="This is just experimental for interactive use")
def test_send_no_bitstamp_config_testmail(db_ready_app,fresh_user_with_nothing):
    ''' intended to really send out a mail! '''
    from lovac.sendgridd import sendmail
    ensure_mailconfig()
    with db_ready_app.test_request_context():
        from lovac.services.mail import send_no_bitstamp_config
        db_ready_app.config["SEND_MAILS"] = True
        change_mailaddress_for_user(fresh_user_with_nothing,"kneunert@gmail.com")
        send_no_bitstamp_config(fresh_user_with_nothing)
        assert False
