''' test for the lovac module '''
import os
os.environ['APP_SETTINGS']="lovac.config.TestConfig"
import sys
import lovac
import unittest
import tempfile

import requests
import pytest
import mock
from flask_testing import TestCase
from flask import current_app

from tests.fixtures_faketransactions import FakeResponse

import lovac

@pytest.fixture(scope="function")
def trading_client(user0):
    ''' a fake-trading-client but use it only with Fake-Requests!! '''
    return lovac.bitstampp.TradingClient.from_user(user0)

def test_has_filter(db_ready_app):
    ''' check whether the filters are there '''
    assert len(db_ready_app.template_context_processors["controller"]) == 6
    assert db_ready_app.jinja_env.filters["bootstrap_notif_mapping"] != None

def test_load_user(db_ready_app):
    ''' whether we can load_user from the database '''
    admin = db_ready_app.load_user("admin")

def test_TradingClient(trading_client):
    ''' test the trading_client '''
    response = response = FakeResponse(b'''{
        "usd_balance": "100.00",
        "btc_balance": "2.001",
        "usd_reserved": "40",
        "btc_reserved": "1",
        "fee": "0.24",
        "usd_available": "60.00",
        "btc_available": "1.001"}''')
    with mock.patch('requests.post', return_value=response):
        result = trading_client.get_balance("usd")
        print(result)
    assert isinstance(result, dict)
    assert result["fiat_balance"] == 100
    assert result["btc_balance"] == 2.001
    assert result["fiat_reserved"] == 40
    assert result["btc_reserved"] == 1
    assert result["fiat_available"] == 60


def test_models(db_ready_app):
    ''' whether we can load the user_models '''
    from lovac import models
    with db_ready_app.app_context():
        models.get_users()