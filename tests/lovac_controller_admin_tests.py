''' test the lovadmin controller module '''
import os
import json

import mock

from tests import login, logout, print_messages

def test_login_logout(client, fresh_user_with_nothing, fresh_user_with_adminrights):
    ''' whether we can login or logout '''
    from lovac.models import db
    db.session.add(fresh_user_with_nothing)
    db.session.add(fresh_user_with_adminrights)
    result = login(client,fresh_user_with_adminrights.email, 'bla', admin=True)
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    result = logout(client)
    assert b'You were logged out' in result.data
    print_messages(result.data)
    result = login(client,fresh_user_with_adminrights.email, 'wrong_password', admin=True)
    assert b'Invalid username or password' in result.data
    print_messages(result.data)
    result = login(client,'non_existing_account', 'wrong_password', admin=True)
    assert b'Invalid username or password' in result.data
    print_messages(result.data)
    result = login(client,fresh_user_with_nothing.email, 'bla', admin=True) # correct password but not admin
    assert b'Invalid username or password' in result.data
    print_messages(result.data)

def test_admin_usermanagement(app,client,fresh_user_with_nothing, fresh_user_with_adminrights, fresh_user_with_purchases, invcode):
    ''' Can we see the dashboard? '''
    from lovac.models import db
    db.session.add(fresh_user_with_nothing)
    db.session.add(fresh_user_with_adminrights)
    db.session.add(fresh_user_with_purchases)
    result = login(client,fresh_user_with_adminrights.email, 'bla', admin=True)
    result = client.get('/admin/usermanagement')
    print(result.data)
    assert b'5 Users in total' in result.data
    assert b'user0' in result.data
    assert b'fresh_user_with_adminrights@user.com' in result.data
    assert b'fresh_user_with_nothing@user.com' in result.data
    assert b'fresh_user_with_purchases@user.com' in result.data
    
def test_invitationcodes(app,client, fresh_user_with_adminrights, invcode):
    ''' the inv-codes page '''
    from lovac.models import db
    db.session.add(fresh_user_with_adminrights)
    result = login(client,fresh_user_with_adminrights.email, 'bla', admin=True)
    result = client.get('/admin/invitationcodes')
    assert b'Number of invitationCodes in the System: 1' in result.data

def test_admin_userdetails(app,client, fresh_user_with_adminrights, fresh_user_with_nothing, fresh_user_with_purchases, invcode):
    ''' User details?! '''
    from lovac.models import db
    db.session.add(fresh_user_with_nothing)
    db.session.add(fresh_user_with_adminrights)
    db.session.add(fresh_user_with_purchases)
    result = login(client,fresh_user_with_adminrights.email, 'bla', admin=True)
    result = client.get('/admin/user-details/fresh_user_with_nothing@user.com')
    result = client.get('/admin/user-details/fresh_user_with_purchases@user.com')


def test_admin_sendmail(app,client,fresh_user_with_adminrights):
    ''' Can we send mail to individual clients? '''
    from lovac.models import db
    db.session.add(fresh_user_with_adminrights)
    app.config["SEND_MAILS"] = True # later we'll assert that mock_mailjet.called
    result = login(client,fresh_user_with_adminrights.email, 'bla', admin=True)
    fake_balance = {'btc_available': 0.09591537, 'btc_balance': 0.09591537, 'btc_reserved': 0.0, 'fee': 0.25, 'fiat_available': 0.08, 'usd_balance': 0.08, 'usd_reserved': 0.0}
    with mock.patch('lovac.services.get_balance', return_value=fake_balance):
        result = client.get('/admin/sendmail')
        assert b'Sending a mail to the user (or all all users)' in result.data
        print_messages(result.data)
    with mock.patch('lovac.sendgridd.sendmail') as mock_sendgridd:
        with mock.patch('lovac.services.get_balance', return_value=fake_balance):
            result = client.post('/admin/user-details/fresh_user_with_adminrights@user.com/sendmail', follow_redirects=True, data=dict(
                mailsubject="blubblaBlub",
                mailtext="bla"
            ))
            print(result.data)
            assert mock_sendgridd.called
            assert b'Mail Successfully sent!' in result.data
            print_messages(result.data)


def test_admin_sendmail_all(app,client, fresh_user_with_adminrights, fresh_user_with_nothing):
    # pylint: disable=unused-import
    ''' Can we send mail to individual clients? '''
    from lovac.models import db
    db.session.add(fresh_user_with_adminrights)
    app.config["SEND_MAILS"] = True # later we'll assert that mock_mailjet.called
    result = login(client,fresh_user_with_adminrights.email, 'bla', admin=True)
    result = client.get('/admin/sendmail')
    assert b'Sending a mail to the user (or all all users)' in result.data
    print_messages(result.data)
    with mock.patch('lovac.sendgridd.sendmail'):
        result = client.post('/admin/sendmail', follow_redirects=True, data=dict(
                mailsubject="blubblaBlub",
                mailtext="bla"
        ))
        print_messages(result.data)
        #assert b'Issues sending mail to these folks:  admin user0' in result.data
        # Damn we should mock sendgrid here rather then sendgridd :-( 
        #assert b'Mail Successfully sent to 3 recipients!' in result.data
        print_messages(result.data)
        #assert False