''' test the lovac user controller module '''
import os
import json
import mock

from tests import login, logout, print_messages
    
os.environ['APP_SETTINGS']="lovac.config.TestConfig"

def test_dashboard(client, fresh_user_with_purchases):
    #pylint: disable=unused-argument
    ''' tests the dashboard '''
    from lovac.models import db
    db.session.add(fresh_user_with_purchases)
    result = login(client, fresh_user_with_purchases.email, 'bla')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    balance=json.loads('''        
        {"btc_balance": 0.00514260, "fiat_available": 2.13, 
        "fee": 0.24, "btc_reserved": 0.00000000, 
        "fiat_balance": 2.13, "btc_available": 0.00514260, 
        "fiat_reserved": 0.00}
    ''')
    with mock.patch('lovac.services.get_balance', return_value=balance):
        result = client.get('/dashboard')
    print(result.data)
    assert b'Current Accumulation Setup: USD 6 daily' in result.data
    assert b'<td>2018-01-20 11:55:53</td>\n        <td>0.00048379 </td>' in result.data
    assert b'There is a shortage of USD in your account.' in result.data

def test_tickerdata(client,fresh_user_with_bitstampconfig):
    #pylint: disable=unused-argument
    ''' tests the tickerdata '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    result = client.get('/ticker/monthly/2018/3')
    result = client.get('/ticker')


def test_buy_get(client, fresh_user_with_bitstampconfig):
    ''' test the buy page '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    fake_balance = json.loads('''        
        {"btc_balance": 0.00514260, "fiat_available": 7.13, 
        "fee": 0.24, "btc_reserved": 0.00000000, 
        "fiat_balance": 7.13, "btc_available": 0.00514260, 
        "fiat_reserved": 0.00}
    ''')
    with mock.patch('lovac.services.bought_today_bitstamp', return_value=False):
        with mock.patch('lovac.services.get_balance', return_value=fake_balance):
            result = client.get('/buy')
            #print(result.data)
            assert b'Here, you can buy now if you like' in result.data
    with mock.patch('lovac.services.bought_today_bitstamp', return_value=True):
        with mock.patch('lovac.services.get_balance', return_value=fake_balance):
            result = client.get('/buy')
            #print(result.data)
            assert b"You can't buy today." in result.data
    with mock.patch('lovac.services.bought_today_bitstamp', return_value=False):
        fake_balance["fiat_available"]=2.13
        with mock.patch('lovac.services.get_balance', return_value=fake_balance):
            result = client.get('/buy')
            print(result.data)
            assert b"You can't buy today." in result.data

def test_buy_post(client,fresh_user_with_bitstampconfig):
    ''' test the buy post '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    print("Logged in successfully!!!!!!")
    with mock.patch('lovac.services.bought_today_bitstamp', return_value=True):
        result = client.post('/buy', follow_redirects=True)
        print(result.data)
        assert b'Sorry, you already bought today!' in result.data

def test_buy_post_fail(client,fresh_user_with_bitstampconfig):
    ''' test the buy post if some error occors. This is just an example and the errorpage should
        basically come up for any kind of error '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    from psycopg2 import InternalError
    # Not really approriate that mock, but who cares what funtion is throwing that error?!
    # and iw as not able to mock lovac.models.db.session.commit see #12
    with mock.patch('lovac.services.bought_today_bitstamp', side_effect=InternalError):
        result = client.post('/buy', follow_redirects=True)
        print(result.data)
        assert b'An error appeared. We\'re so sorry!' in result.data

def test_buy_post_whenNoMoneyOnBitstamp(client,fresh_user_with_bitstampconfig):
    ''' test the buy post '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    with mock.patch('lovac.services.bought_today_bitstamp', return_value=True):
        result = client.post('/buy', follow_redirects=True)
        print(result.data)
        assert b'Sorry, you already bought today!' in result.data
        print_messages(result.data)

def test_accum_plan(client, db_ready_app, user0):
    ''' whether we can change the accum-plan '''
    result = login(client,'user0', '0re5u')
    print(result)
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    result = client.get('/settings/accum-plan')
    assert b'This results in a monthly investment (30 days) of USD 180.' in result.data
    result = client.post('/settings/accum-plan', data=dict(
        daily_investment="27", # we're ommitting the strategy and expect that it doesn't change
    ), follow_redirects=True)
    print(result.data)
    assert b'This results in a monthly investment (30 days) of USD 810.' in result.data
    assert db_ready_app.load_user("user0").bitstampconfig.dailybuy == 27
    assert db_ready_app.load_user("user0").bitstampconfig.strategy == u'dollar_cost_averaging'
    result = client.post('/settings/accum-plan', data=dict(
        daily_investment="27",
        strategy="buy_the_dip"
    ), follow_redirects=True)
    assert db_ready_app.load_user("user0").bitstampconfig.strategy == u'buy_the_dip'

