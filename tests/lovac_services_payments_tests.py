''' tests for the payments-module '''
import datetime
import pytz
import mock
import codecs
from lovac.models import Payment
import lovac # mock lovac_now



def test_search_or_create_payment(db_ready_app, fresh_user_with_bitstampconfig):
    ''' tests search_or_create_payment '''
    #pylint: disable=unused-argument
    from lovac.services.payments import search_or_create_payment
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    with db_ready_app.app_context():
        class MockResponse(object):
            ''' A mock class '''
            def __init__(self):
                self.r_hash = codecs.decode('mHNnPfjG68J2kf/YYOkjcL9ZJYCwuOD33IYREQxdpNQ='.encode(), 'base64')
                self.payment_request = 'lnsb10u1pd5jp8lpp5npekw00ccm4uya53llvxp6frwzl4jfvqkzuwpa7uscg3zrza5n2qdqu24ek2u3qya6x2um5w4ek2u3nyusqcqzys6h74dkslsztmxe4kly64999ka9v8cqu99r754vfmjjk2ske7m39j43rduyr0mk30g0zru8xj4rm26uv34tsg7rn345cmzalnaccwq6gpv4nylq'
            def AddInvoice(self, something, metadata):
                return self
            def rhash():
                return "lnsb10u1pd5jp8lpp5npekw00ccm4uya53llvxp6frwzl4jfvqkzuwpa7uscg3zrza5n2qdqu24ek2u3qya6x2um5w4ek2u3nyusqcqzys6h74dkslsztmxe4kly64999ka9v8cqu99r754vfmjjk2ske7m39j43rduyr0mk30g0zru8xj4rm26uv34tsg7rn345cmzalnaccwq6gpv4nylq"
        
        class MockResponse2(object):
            ''' A mock class '''
            def __init__(self):
                self.r_hash = codecs.decode('mHNnPfjG68J2kf/YYOkjcL9ZJYCwuOD33IYREQxdpNQ='.encode(), 'base64')
                self.payment_request = 'lnsb10u1pd5jp8lpp5npekw00ccm4uya53llvxp6frwzl4jfvqkzuwpa7uscg3zrza5n2qdqu24ek2u3qya6x2um5w4ek2u3nyusqcqzys6h74dkslsztmxe4kly64999ka9v8cqu99r754vfmjjk2ske7m39j43rduyr0mk30g0zru8xj4rm26uv34tsg7rn345cmzalnaccwq6gpv4nylr'
            def AddInvoice(self, something, metadata):
                return self
            def rhash():
                return "lnsb10u1pd5jp8lpp5npekw00ccm4uya53llvxp6frwzl4jfvqkzuwpa7uscg3zrza5n2qdqu24ek2u3qya6x2um5w4ek2u3nyusqcqzys6h74dkslsztmxe4kly64999ka9v8cqu99r754vfmjjk2ske7m39j43rduyr0mk30g0zru8xj4rm26uv34tsg7rn345cmzalnaccwq6gpv4nylq"
      
        with mock.patch('lovac.lnd.Lnd.generate_invoice', return_value=MockResponse()):
            with mock.patch('lovac.util.lovac_now', return_value=datetime.datetime(2018, 1, 25, 12, 30, 45,tzinfo=pytz.utc)): # much longer then an hour
                payment = search_or_create_payment(fresh_user_with_bitstampconfig)
                assert payment.payment_request == 'lnsb10u1pd5jp8lpp5npekw00ccm4uya53llvxp6frwzl4jfvqkzuwpa7uscg3zrza5n2qdqu24ek2u3qya6x2um5w4ek2u3nyusqcqzys6h74dkslsztmxe4kly64999ka9v8cqu99r754vfmjjk2ske7m39j43rduyr0mk30g0zru8xj4rm26uv34tsg7rn345cmzalnaccwq6gpv4nylq'
                print(payment.created_at)
                assert payment.has_expired()
        with mock.patch('lovac.lnd.Lnd.generate_invoice', return_value=MockResponse2()):
            payment = search_or_create_payment(fresh_user_with_bitstampconfig)
            assert payment.payment_request == MockResponse2().payment_request
            print(payment.created_at)
            assert not payment.has_expired()



def test_calculate_lfee_balance(db_ready_app, fresh_user_with_purchases):
    ''' test_calculate_lfee_balance ''' 
    #pylint: disable=unused-argument
    from lovac.services.payments import calculate_lfees_payments_balance
    from lovac.models import db
    db.session.add(fresh_user_with_purchases)
    calculate_lfee_balance = calculate_lfees_payments_balance(fresh_user_with_purchases)
    print(str(calculate_lfee_balance))
    assert calculate_lfee_balance == -13628


def test_recommended_payment(db_ready_app, fresh_user_with_purchases):
    ''' test_recommended_payment '''
    #pylint: disable=unused-argument
    from lovac.services.payments import recommended_payment
    from lovac.models import db
    db.session.add(fresh_user_with_purchases)
    recommended_payment = recommended_payment(fresh_user_with_purchases)
    print(str(recommended_payment))
    assert recommended_payment == 14628
    # ToDo we should test the recommended payment for a positive balance!

