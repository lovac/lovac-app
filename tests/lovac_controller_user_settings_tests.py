''' test the lovac user controller module '''
import os
import time

from tests import login, logout, print_messages
    
def test_wizard(client,app,fresh_user_with_nothing):
    ''' test the register endpoint '''
    from lovac.models import db
    db.session.add(fresh_user_with_nothing)
    result = login(client, fresh_user_with_nothing.email, "bla")
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    result = client.get("/wizard")
    assert b'The wizard should help you to setup the connection to Bitstamp' in result.data
    result = client.get("/wizard/step/1")
    assert b'Bitstamp allows you to create subaccounts' in result.data
    result = client.get("/wizard/step/2")
    assert b'Now, we need to fund the subaccount.' in result.data
    result = client.get("/wizard/step/3")
    assert b'In this step we create the API-key' in result.data
    result = client.get("/wizard/step/4")
    assert b'As the last step of connecting to Bitstamp, we need now ' in result.data
    assert b'to copy over the API-key-values to configure them here' in result.data
    result = client.post("/wizard", data=dict(
        currency="usd",
        key="Wurst",
        username="Wurs",
        secret="WillFail"
    ))
    assert b'Field must be between 5 and 10 characters long.' in result.data # username
    assert b'Field must be between 6 and 40 characters long.' in result.data # key
    assert b'bitstamp_secret is usually about 32 characters long.' in result.data # key
    result = client.post("/wizard", data=dict(
        currency="usd",
        key="WurstWurst",
        username="WurstWurst",
        secret="123456789012345678901234567890_12"
    ))
    #assert b'Bitstamp-Credentials seem to not be valid! ' in result.data # key
    assert b'Double-Check your configuration and make sure your API-key is activated!' in result.data # key
    result = client.post("/wizard", data=dict(
        currency="usd",
        key="Jui6Mdr2BxwgevI5JrTZA9d06BHV0lcL",
        username="148378",
        secret="j0FkBinTo4zY1nh5eRIWGPLHfRxGqHrt"
    ))
    assert b'Successfully stored!' in result.data
    assert b'Congratulations, you\'re almost done. In the next step,' in result.data
    assert b'you\'re setting up your accumulation plan.' in result.data
    print_messages(result.data)
    result = client.get("/wizard/step/5")
    assert b'The last step is deciding how much you want to invest on a daily basis.' in result.data
    