''' test the lovac user controller module '''
import os
import time

import mock

from tests import login, logout, register, print_messages

    
def test_login_logout(client, fresh_user_with_bitstampconfig):
    ''' whether we can login or logout '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    result = logout(client)
    assert b'You were logged out' in result.data
    print_messages(result.data)
    result = login(client,'non_existing_user', 'bla')
    assert b'Invalid username or password' in result.data
    print_messages(result.data)
    result = login(client,fresh_user_with_bitstampconfig.email, 'wrong_password')
    assert b'Invalid username or password' in result.data
    print_messages(result.data)
    # test the next param
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla',nextparam="http://localhost:5000/settings/change-password")
    assert b'Logged in successfully.' in result.data
    assert b'Change Password' in result.data
    print_messages(result.data)
    logout(client)
    result = client.get('/settings/change-password', follow_redirects=True)
    assert b'Please Sign In' in result.data
    print_messages(result.data)


def test_register(client,app,invcode):
    ''' test the register endpoint'''
    result = client.get("/register")
    assert b'Please Register' in result.data
    print_messages(result.data)
    result = register(client,"wurst","eins","zwei","wurst")
    assert b'Invalid email address.' in result.data #email
    assert b'Field must be between 6 and 25 characters long.' in result.data # password
    assert b'Passwords must match.' in result.data # confirm
    print_messages(result.data)
    result = register(client,"test@user.com","einszweidrei4","einszweidrei4",invcode.code)
    # asks for confirmation mail
    assert b'Please check your Email and click the confirmation-link' in result.data
    print_messages(result.data)
    result = login(client,'test@user.com', 'einszweidrei4')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    logout(client)
    user = app.load_user("test@user.com")
    result = register(client,"test2@user.com","einszweidrei4","einszweidrei4",invcode.code)
    assert b'Sorry, this invitation-code is already exhausted!' in result.data
    print_messages(result.data)


def test_confirm_token(client,app,invcode2,fresh_user_with_bitstampconfig):
    ''' A user needs to confirm his mail before ... eventually (not) yet) he can use the application '''
    app.config["SEND_MAILS"] = True # later we'll assert that mock_mailjet.called
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    email="test3@user.com"
    password = "einszweidrei4"
    with mock.patch('lovac.sendgridd.sendmail') as mock_sendgridd:
        result = register(client,email,password,password,invcode2.code)
        assert mock_sendgridd.called
    assert b'Please check your Email and click the confirmation-link' in result.data
    # That's the question here: Will a redirect work? Do we need to make that call unauth?
    #result = login(client,email, password)
    invalid_token='InRlc3QzQHVzZXIuY29tIg.Dwlq8w.zJCzgevkGpkecHLIX-N3YWle9KY' # outdated but otherwise valid (test3@user.com)
    result = client.get("/confirm/{}".format(invalid_token),follow_redirects=False)
    # Without a login, you'll get redirected 
    assert result.status_code == 307
    domain="localhost.localdomain:5000"
    assert "http://"+domain+"/login?next=http://"+domain+"/confirm/InRlc3QzQHVzZXIuY29tIg.Dwlq8w.zJCzgevkGpkecHLIX-N3YWle9KY" in result.location
    result = login(client,email, password)
    result = client.get("/confirm/{}".format(invalid_token),follow_redirects=True)
    assert b'The confirmation link is invalid or has expired.' in result.data
    print_messages(result.data)
    # let's secretly calculate an valid token for another user ...
    from lovac.services.tokenregistering import generate_confirmation_token
    invalid_token = generate_confirmation_token(fresh_user_with_bitstampconfig.email)
    result = client.get("/confirm/{}".format(invalid_token),follow_redirects=True)
    assert b'The confirmation link is invalid or has expired.' in result.data
    print_messages(result.data)   
    # let's secretly calculate a valid token for test3@user.com ...
    token = generate_confirmation_token(email)
    result = client.get("/confirm/{}".format(token),follow_redirects=True)
    # Without a login, you won't be able to confirm
    assert b'Dashboard' in result.data
    assert b'Your Account is now confirmed!' in result.data
    print_messages(result.data)
    assert app.load_user("test3@user.com").confirmed


def test_forgotpassword(client,app,fresh_user_with_bitstampconfig):
    ''' tests the forgotpassword functionality '''
    result = login(client, fresh_user_with_bitstampconfig.email, "bla")
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    result = client.get("/forgotpassword")
    assert b'Please enter your confirmed Mailaddress and you' in result.data
    print_messages(result.data)
    result = client.post("/forgotpassword", data=dict (
        email="nicht_existierendeMail@user.de"
    ))
    assert b'If that Email exists, then please check ' in result.data
    assert b'your Email and click the link to reset your Password!' in result.data
    print_messages(result.data)
    result = client.post("/forgotpassword", data=dict (
        email="fresh@user.de"
    ))
    assert b'If that Email exists, then please check ' in result.data
    print_messages(result.data)

def test_reset_password(client,app,fresh_user_with_bitstampconfig):
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    from lovac.services.tokenregistering import generate_forgotpassword_token
    # tiny wrong token in endpoint
    token = generate_forgotpassword_token("fresh_user_with_bitstampconfig@user.com")
    result = client.post("/reset-password/{}a".format(token), data=dict (
        token=token,
        new_password1 = "fresh-user",
        new_password2 = "fresh-user"
    ))
    result = login(client, fresh_user_with_bitstampconfig.email, "fresh-user")
    assert b'Invalid username or password' in result.data
    print_messages(result.data)
    # This time should work
    result = client.post("/reset-password/{}".format(token), data=dict (
        token=token,
        new_password1 = "fresh-user",
        new_password2 = "fresh-user"
    ))
    result = login(client, fresh_user_with_bitstampconfig.email, "fresh-user")
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)

def test_forgotpassword(client, db_ready_app , fresh_user_with_bitstampconfig):
    ''' whether we can reset the pasword '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    from lovac.services.tokenregistering import generate_confirmation_token
    token = generate_confirmation_token(fresh_user_with_bitstampconfig.email+"_forgotpassword")
    result = client.get("/reset-password/{}".format(token),follow_redirects=True)
    assert bytearray(token,'utf-8') in result.data, "should contain token in hidden field"
    assert bytearray("/reset-password/{}".format(token),'utf-8') in result.data, "should contain endpoint"
    result = client.post('/reset-password/{}'.format(token), data=dict(
        token=token,
        new_password1="bla1bla5",
        new_password2="bla1bla5"
    ), follow_redirects=True)
    print(result.data)
    assert b'Please Sign In' in result.data
    print_messages(result.data)
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla1bla5')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    result = logout(client)
    assert b'You were logged out' in result.data
    print_messages(result.data)
    # toDo: Test Lots of failures

def test_changepassword(client,app,fresh_user_with_bitstampconfig):
    ''' whether we can change the pasword '''
    from lovac.models import db
    db.session.add(fresh_user_with_bitstampconfig)
    result = login(client,fresh_user_with_bitstampconfig.email, 'bla')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    result = client.post('/settings/change-password', data=dict(
        current_password="blub_false",
        new_password1="einszweidrei",
        new_password2="einszweidrei"
    ), follow_redirects=True)
    assert b'Current password did not match!' in result.data
    print_messages(result.data)
    result = client.post('/settings/change-password', data=dict(
        current_password="bla"
    ), follow_redirects=True)
    assert b'This field is required.' in result.data
    print_messages(result.data)
    result = client.post('/settings/change-password', data=dict(
        current_password="bla",
        new_password1="eins",
        new_password2="zwei"
    ), follow_redirects=True)
    assert b'New passwords must match.' in result.data
    assert b'New password must be between 6 and 25 characters long.' in result.data
    print_messages(result.data)
    result = client.post('/settings/change-password', data=dict(
        current_password="bla",
        new_password1="bla1bla1",
        new_password2="bla1bla1"
    ), follow_redirects=True)
    print(result.data)
    assert b'Success!!' in result.data
    print_messages(result.data)
    result = logout(client)
    assert b'You were logged out' in result.data
    print_messages(result.data)
    result = login(client, fresh_user_with_bitstampconfig.email, 'bla1bla1')
    assert b'Logged in successfully.' in result.data
    print_messages(result.data)
    logout(client)