This is a standard flask-application. It runs a bot which is "low volume aquiring bitcoin" via Bitstamp.
User python 3.6. It's known to not work in python 3.8.

development:

    virtualenv --python=python3 default
    source default/bin/activate
    pip install -r requirements.txt
    pip install --editable .
    export FLASK_APP=lovac.singleton
    export FLASK_DEBUG=true
    # run the tests
    coverage run --source=lovac setup.py test
    coverage report # maybe get an idea of how the coverage is
    flask initdb # initialize the database
    flask bcc2csv --days 20 # decide how many days of tickerdata you want to download from Bitcoincharts and store as csv ...
    flask csv2db            # ... and load them in the DB
    flask run
    # goto http://localhost:5000/login (admin/derida99)
    # click on settings and enter your details (follow the Link to bitstamp to create the API-Key)


### Run Tests:

```
pytest
pytest tests/lovac_backtest_tests.py         # Run tests in a specific file
pytest tests/lovac_backtest_tests.py -k back # Run tests in a specific file matching "back"
```

### Run Linter:
```
python setup.py lint
# Search for unused imports:
pylint --enable=unused-imports --confidence=UNDEFINED  lovac.controller.user_selfadmin | grep Unused
```

# Deploy to Minikube

The following process assumes that the lovac namespace exists.

## Create ImagePullSecret

The Lovac image is available in the private Gitlab Docker registry. To enable
Kubernetes to pull the image one needs to create a secret in Kubernetes
containing the credentials required to authenticate against the Docker registry.
The process is described
[in the k8s documentation](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).
Instead of using the password it is recommended [to create a token](https://gitlab.com/profile/personal_access_tokens)
in gitlab and use this token to authenticate.

```
kubectl create -n lovac secret docker-registry gitlab-registry-creds --docker-server=registry.gitlab.com --docker-<your-name> --docker-password=<your-token> --docker-email=<your-email>
```

## Deploy lovac

After creating the Docker registry secret in the lovac namespace the application
can be deployed using the definition in *deploy.yaml*.

```
kubectl apply -f deploy.yaml
```

## Database
Lovac currently supports two types of Database flavors:
* sqlite: For local testing purposes
* MySQL: For production deployment

In order to use the sqlite database (default) you don't need to adapt anything.

If you want to use the MySQL instead you need to switch the configuration to the production one. Also you need to provide some details about the MySQL deployment in environment variables.

```
export config=lovac.config.ProdConfig
export DB_HOST=mysql
export DB_PORT=3306
export DATABASE=lovac
export DB_USER=lovac-user
export DB_PASSWORD=password
flask run
```

## ToDo
* Run database in Kubernetes

# run via docker-compose

The current docker-compose file includes a nginx-proxy which might get automatically configured to use letencrypt via the other two containers. A simple docker based environment with self-signed certificates (already created in data/certs/dev.*) is as simple as

```
docker login registry.gitlab.com
docker-compose up
```

The solution is powered by https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion and very similiar to "Evert Ramos's Examples".

# alpha.lovac.xyz


