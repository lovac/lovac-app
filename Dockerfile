FROM registry.gitlab.com/lovac/lovac-app/pythonbase:20210114095534

WORKDIR /usr/src/app
ENV FLASK_APP lovac.singleton
ENV PYTHONUNBUFFERED 1
EXPOSE 5000

ENV CONFIG lovac.config.ProductionConfig

COPY . .

RUN pip install -r requirements.txt && pip install -e .

CMD [ "flask", "run" , "--host=0.0.0.0" ]
