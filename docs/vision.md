LOVAC is the low volume accumulator of bitcoin

It wants to help people organize their retirement-funds ... well the bitcoin part of that (only). As such lovac:
* let the user decides about his "Sparplan", the default is 5$ a day
* Ensures as best as it can that the Sparplan gets executed
* Enables audit of the Sparplan spanning decades, this involves:
  * how much money did i spent into the Sparplan, when?
  * How is my retirement-fund doing fiat-wise?
  * on which bitcoin-addresses are my funds lying around, auditing their movement

Lovac is heading towards a MVP. We're almost feature-complete but a lot of NFRs are not here yet. At least we have a domain (lovac.xyz) :
* Multi-user concept and execution
* Almost everything around operational concepts
* decision about the primary database ?
