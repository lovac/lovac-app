## 01.Feb 2019 - Kim
We're much further steps now with mail but it's not done yet. Currently i'm struggling with LND again. You should support powerusers whenever you find them and takinbo complained about not being able to pay his invoice as it's expired. I fixed it but now i'm in trouble with timezones (again).
In order to be able to mock as much as possible, i'm using lovac_now() to return datetime(tz=pytz.utc). I'm not sure why i haven't that problem elsewhere but if i want to store such a date in cockroachdb, i get:
sqlalchemy.exc.InternalError: (psycopg2.InternalError) value type timestamptz doesn't match type TIMESTAMP of column "created_at"
So, https://www.cockroachlabs.com/docs/stable/timestamp.html tells you that:
"The TIMESTAMP data type stores a date and time pair in UTC, whereas TIMESTAMPTZ stores a date and time pair with a time zone offset from UTC."
So i guess the best thing to do is switching over to TIMESTAMPTZ but i guess this should be done with almost all times used. So if i would do that, i might break a lot of code/tests but as the tests are running on HSQL but are fine at the same time, they might not even get catched in the tests. We have to start testing directly on cockroach.

## 20th Jan 2019 - Kim
Backups is now done at every night 4am. The next thing is about mails. Mails are so important to keep people engaged. So let's plan for something a bit more sophisticated. Let's try to collect requirements here and then look at the implementation:
* We need to initially get someone on board: Signup, confirm-mail, configure API-credentials (wizard)
* We need to motivate people further to fillup there Exchange-Account and or remove btc from their exachange account
* If we don't want to be too exposed security-issues, people need to do that manually and we need to be as helpfull as possible with that
So here is the scenario for an average user:
* Bob signed up but didn't confirmed his mail-address
* Bob gets reminding mails after 1,2,3,5,8 days, he would get thrown out after X days
* After confirming his mail eventually, he now also don't configure via Wizard, same applies here.
* If the User bought the dip, we want to send him a mail
* Additionally the user should get regular mails informing him about hodling and other funfacts about bitcoin

So let's think about how we can achieve all these requirements.
* We will create an identifier for the reason sending a mail to the user. 
* We'll add a new table which is storing mails which has been sent to users.
* So here are lists of identifiers. All identifiers starting with "please_" are asking the user to do something:
  * please_confirm_email
  * please_configure_bitstampcredentials
  * please_configure_accumplan
  * please_send_fiat
  * please_withdraw_bitcoin
  * learn_general

Via the date and the identifier, the algorithm can determine whether to send yet another please_mail or a learn_mail. Please_mails are resent via fibonacci (1,2,3,5,8 days) and learn_mails in regular intervals (monthly?!).
In order to fix reaccuring issues (please_send_fiat), we need to cancel out specific mails. In order to not loose that data, let's have an additional column "resolved".


## 10th Jan 2019 - Kim
Wow, 2,5 month without any Blog-entry. But so much happened during that time. We got a Logo. We have T-Shirts. We did a lot about the User-Experience: We have Mail. And with mail we have now Email-Confirmationmal, Forgotpasswordmail and You're-short-on-fiat-at-bitstampmail. We can also mail individual Customers or even all of them.
The customer can temporily deactivate his plan to stop accumulating for whatever reason. AND we implemented EUR as a second currency.
But what's next? Several things in my mind:
* Backup is still not existing in a reliable way, i would say that the next thing to have on a daily basis and maybe even having a HA-node somewhere else.
* A lot more mails and a lot more clever. I'd like to have Notificationschemes and also notifying the user in increasing time-windows. E.g. when the user is short of Fiat, let's inform him directly, after a day, after 2, 4, 8, 16 days or something like that.
* Twitter-integration: We need to advertise more effectively. Announcing "buy-the-dip" via twitter with Volume might be a very interesting feature to get more customers. That feature is also cool to get controlled growth: Automatically Publishing invitationcodes with each dip.
* A medium-article about the theory of DCA. I did a lot with jupyter-notebooks and i just have to make a reasonable article out of that.
* There is a ex-hybris-conference coming. Maybe make a presentation there?
* Think about an advertisement film but that's costing money. Maybe raise some at the above conference?
* revisit the refill idea: Get a watchonly address and refill that wallet.
Let's see where this is heading.

## 21th Oct 2018 - Kim
Again even more then a month without an update. The enthusiasm calmed down a bit indeed. However and nevertheless there is a lot happening. The Logo has been created but is now stuck on the computer of the designer. We might create T-Shirts next and integrate the Logo into the page.
Buy-the-dip is finished and it's even working in production. It took a good while and a bug which finally was a missing sorting-stance in a SQL-query.
Also i tried out fiverr and ordered a fix and extension for the price-graph. Now, the y-axis is automatically scaling with the values it should show and the bubbles have mouseover on how much bitcoin they represent in btc. Although the guy who got $29 for the job fixed a bug.
So what are the next steps?
Logo! Let's do it!

## 14th august 2018 - Kim
Wow, almost a month now. My goal was to write something every two weeks. Hmmm, have to get better at it. But it does not mean that nothing happens. On the contrary. Here is a list:
* I might get a Logo. However, i might need to poke that guy now every week to get it in half a year and i don't know yet how to poke hime most effectively
* As a prerequisite to more precise data-investigation i implemented a way to see exact price and datetime-data in the ticker-charts. You can see there now the lower bollinger-band-price and the actual-price side by side for each datapoint if you hover with the mouse
* I wanted to have the buy-the-dip-feature. If the price drops below the bollinger-band and then enters it again, that's a buy-signal. I described the feature first at 4th February. It's finished by tests but it doesn't work yet in production. I have issues with time-zones. The tickerdata from Bitstamp seems to be based on timestamps on Europe/Berlin but my current Server is in the US (see below). If it finally works, i'll need an extra-article to explain a bit better anyway.
* Unfortunately after some deployment my "dogan" snowflake-server had an issue with the opening of the 443 port. I poked around for an hour or two and then decided to rather work on a fully automated setup via Digital-Ocean rather then fixing a snowflake issue. I have scripts now, half-baked, which are not even in a spike but not checked-in. The replacement for https://alpha.lovac.xyz is now https://beta.lovac.xyz. However the setup needs to have a resonable public IP-Address which is reassignable but keeps stable at the DNS-level. This will also need another dedicated blog-entry. 
Enough for today.

## 18th july 1018 - Kim
Apart from the WOW-effect, integration is done. And on the POC, we have the QR-code and the WOW-effect. So we're doing huge steps forward. In the POC, i wrote a whole bunch of details about how all that stuff is working and WOW-effect is, because it might be that i haven't it mentioned here, yet.
I won't copy and paste and so i just reference it (here)[https://gitlab.com/lovac/lovac-spikes/blob/spikes/issue-11/readme.md]. Next up is someconsolidation. I haven't lost code-coverage yet, still at 79% but i just had to figure out that essential parts are not covered. Also now the WOW-effect needs to be implemented next.


## 5th july 2018 - Kim
The web-ui now is in a stage of a minimal POC for integration with lightning. A webrequest shows all the existing invoices and its state in the DB and a new freshly generated. There is code in place which checks the state of the invoice and some errorhandling if the invoice is not found. However, two major issues with the solution so far:
* No QR-code. We want cool scanning with you Android phone
* No instand feedback if someone pays the invoice
Both is necessary to get a reasonable WOW-effect. However, i'm also making progress on another front: The lightning-production-setup. I researched a bit and found out that the new hot kid in the place (satoshis-place) is using a ODroid setup and there is also a nice (tutorial)[https://github.com/Stadicus/guides/blob/master/thundroid/README.md] existing which explains the ODroid lightning/BitcoinCore-setup so i ordered one and 500GB SSD-harddrive and got a testnet node quickly up and running, including the lightning node.
At the end, the question was how to make it accessible to the outside. A lightning-node needs a public-IP-address. My ISP at home only gives me IPv6. That's fine but i wanted a IPv4. So the solution was a ssh-reverse-tunnel to alpha.lovac.xyz which is forwarding the port 9735 (the lightning peer-2-peer port from ODroid to alpha. 
ssh -f -N -T -R 0.0.0.0:9735:localhost:9735 lovac@alpha.lovac.xyz
We will need something similiar with the grpc-port later as well. In order to establish and keep up the tunnel, it has been created with the help of autossh.

```
[Unit]
Description=AutoSSH tunnel service everythingcli MySQL on local port 5000
After=network.target

[Service]
Environment="AUTOSSH_GATETIME=0"
ExecStart=/usr/bin/autossh -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -NR 0.0.0.0:9735:localhost:9735 tunnel@alpha.lovac.xyz
User=bitcoin
Group=bitcoin

[Install]
WantedBy=multi-user.target
```

## 18th june 2018 - Kim
The last week was mainly dominated by the spike and getting reasonable mitigation for the cockroach-issues in place. 
I've created [#12](https://gitlab.com/lovac/lovac-app/issues/12) and https://gitlab.com/lovac/lovac-app/issues/13 which are both bugs coming from cockoachdb handling the app completely useless. I've tried to dig a bit into what the issues are about but that was quite difficult. I read some fundamentals about cockroachdb which also made me wonder whether this is really the best choice.
The practical approach was, for now, to put reasonable error-handling-code in place which is rolling back the transaction. Let's see whether the current state is at least sustainable with not bringing the whole application down.
The spike is going fine. There we have a script ( [lnd-bootstrap.sh](https://gitlab.com/lovac/lovac-spikes/blob/spikes/issue-11/utils/lnd-bootstrap.sh) ) which should prove and show how lnd and a lightning-transaction is working end-2-end which is potentially embeddable in an integration-test. Both images (btcd and lnd) only needed minor adjustments to have them locally. Both are doing a git-checkout from the public repo. btcd is doing a 2-stage-build which is something we also should look into for our own purposes. I'll now continue with the Stage 2 of the tutorial: The web-ui for lnd.

## 11th june 2018 - Kim
Time is fleeting so fast. I've merged the K8S efforts. and fixed already some imcompatibilities there. We can now spinup hulk on demand. Also i've switched to cockroachDB on alpha, but not yet on hulk yet. That's a bigger effort as one have to solve the certificate-issue.
Also i've added a "setupWizard" which guides the user much better on how to setup stuff with bitstamp. A lot of nice screenshots in there but also the text should be revised.
Also now we have a register-functionality and invitation-codes. The coolest feature though are now red circles which are representing the users buy in the ticker.
Also i've started with "lovac-fees" or lfee for short. I've introduced a column in the Purchase-table and while storing a Purchase, the lfee is calculated (0,25% of btc-amount purchased) and based on that i can show the user his "fee-balance" which is currently for me -23929 satoshis. It's time to make that a plus! So i've just created #11  https://gitlab.com/lovac/lovac-app/issues/11 : Work through the lightning tutorial. That's the next thing i'll do as a spike.

## 26th may 2018 - Kim
Cool, i just migrated the user-controller to JWT (Json Web Token) completely. And i'm looking in cockroachDB. But i really should probably work in a bit more strucured way but anyway ... as longs as i'm getting stuff done, who cares!
Ok, so let's look at the last spike which i haven't integrated yet: The hulk experiment should scale like the incredible hulk. It does not yet, but it's a good start using gcloud.
https://gitlab.com/lovac/lovac-spikes/tree/spikes/issue-4

I have a manual-directory which explains all the steps in much more detail. Apart from that directory, everything is completely automated. The control of Kubernetes and gcloud is done via a dockercontainer which contains the gcloud-binary, helm and kubectl on top of an alpine-image. 
I created a number of new stages:
* the old deploy is now called deploy_alpha
* deploy_hulk contains a manual triggered job which is provisioning the gcloud-k8s-cluster and deploys lovac on it. The scripts are located in the utils-directory next to the build.sh script. We'll get ready-to-go-letsencrypt certificates out of the box thanks to cert-manager which is installed via helm. Also the dns is completely automated. Later the app will be ready at hulk.gce.lovac.xyz
* day2ops currently adds a node to the cluster and deploys Prometheus and grafana into the cluster. Grafana is also automatically configured with a number of standard-dashboards. It's not exposed via nginx, though. It can be excessed via kubectl's port-forwarding
* The teardown stage removes all of it again. This is also manual.
* All deployment-yaml-files are located in a k8s-subdirectory with subdirectories per each usecase. Due to the easyness of helm, the below list is not complete:
  * the cert-manager issuer custom-resource. cert-manager itself is installed via helm

That's an amazing outcome. And together with cockroachdb, i have some great ideas how we can make lovac scale seamlessly.
However, let's look at the weaknesses of this:
* The helm installation is incredible convenient but creates external (yet) unpinned dependencies. The helm-dependency is not the only dependency. We also implicitely rely on the underlying image. Not good. Currently, we have the following dependencies/docker-images:
    * stable/nginx-ingress quay.io/jetstack/cert-manager-controller:v0.3.0
    * stable/cert-manager
    * stable/prometheus
    * stable/grafana
* In order to mitigate that, we should create local versions of all the used charts. That is possible due to https://docs.helm.sh/using_helm/#more-installation-methods which make it possible to install from a local unpacked chart directory

So this does not look good enough yet in order to integrate it into lovac-app. Several changes should be done, though:
* a better for the control-image is probably "cloudcontrol".
* We need to create local versions of all images
* We need to create local versions of all used helm-charts and use them instead
* We should investigate security-implications of helm: https://docs.helm.sh/using_helm/#securing-your-helm-installation

Puh, ok enough to do still.



## 18nd may 2018 - Kim
Way too many things happened and i didn't wrote as much here as i should. Whenever i write here, my work gets more structured so even if this won't get read ever, this is helpfull to get more efficient/effective. Ok what happened?

Sub $1 investment plan
We have now fully implemented the "sub 1$" investment plan. You can change your plan any time you want and the automatic purchase will calculate correctly on a monthly basis. You can defer your buy or can buy directly. All fine with that.
Part of that is also the sync from Bitstamp and having a cache of all transactions in the DB. However we're not yet showing DB-entries on the Dashboard. Guess we should fix that.

Kubernetes POC
I made a huge kubernetes Spike/POC. I even refactored it so that it's ready to go into the app but i haven't done so. I'll write about the reasoning ... maybe tomorrow?!

Cold-Storage
For some time already i worries about pushing coins into cold-storage. How could you track something like that? How would you calculate your yields? How would you know how big your stash is? And even if you only sum-up transactions, that's imho not enough. You should not only know how much you have, but also where that money is! For sure you should also bve able to spend that money but that's for security reasons out of scope ;-).
Because of that i added BtcAddresses connected to a BitstampConfig. There you can add your address and it will automatically check the balance (via blockchain.info). This is using Peter Todd's python-bitcoinlib. Currently the calculation which should be corrected based on this is broken. So we need to refactor the calculation code a bit better.

ToDos:
* Write about the GCloud Kubernetes POC
* Integrate the GCloud Kubernetes POC
* Refactor the calculation-code
* Do a lot more refactoring and think about the "service-layer" in lovac/services
* Maybe make another attempt to cleanup the backlog and work with tickets?

Also i have to think about getting people onboard. Maybe making slides to be able to pitch. However i would for now consider such activities out of scope for this blog. But there are things happening on that area.


## 24nd april 2018 - Kim
... aaaand done! Now we have a sync-mechanism which syncs all the transactions from bitstamp to the local database. Whenever we buy, the new purchase is stored in the db keeping local DB and bitstamp in sync. As long as we're making backups, we don't need the sync-mechanism. However we're happy to have it if we want to purge the DB which we want to do quite often currently.
We should feel uncomfortable after 1000 days, so in about 3 years because then the syncing won't work anymore as expected. Anyway, let's worry about that later.
Let's create a new cool feature: I want to accumulate less than $6 per day. Let's say $1. Because bitstamp does not allow less then $5 ($6?!) for a single purchase, we need to accumulate for 6 days until we can purchase. However, that's already half implemented, isn't it?!
So what do we have to adjust? Let's make a list:
* The Purchase need to check whether it's at least $6 per Purchase. Otherwise we need to skip the purchase. However, we don't decrease the defer_counter in that case as this one is reserved only to defer if a real purchase would be possible.
* We need to adjust the prediction-algorithm calculating the next purchase. We have to take into account not only "tomorrow + defer_counter" but be a bit more cautious here.

Let's think about the second requirement. Let's assume this setup:
* We want to purchase $1 per day
* We have bought already $6 on the 6th, then the user deferred 10 times
* Now is the 13th, so what the calculation so when will be the next purchase and how much will it be?

The current algorithm only calculates the amount like this:
should_purchase_this_month = user.bitstampconfig.dailybuy * on_date.day
As on_date would be today+10 = 23
should_purchase_this_month: $23 
We already purchased $6 that makes $17

The complexity increases when we do the purchase on the 23 and the user again deferred 2 times and on the 24th the system should tell when and how much will probably be purchased.

We have to take care about the semantics of on_date. This is 

However, the User defeered another 10 times which would result in $17


## 22nd april 2018 - Kim
We have buyOrDefer implemented. Unfortunately the defering does not increase the amount which will get bought next time. Because what you want if you defer the buy is that your amount stashes up so that you can buy even more because you expect the price to fall. Here is an example:
* You want to buy $6 per day
* The month just got started and you buy $6 on the 1st and 2nd.
* On the 3rd you decide to defer the buy
* so on the 4th, you expect that the automatic_buy (and also the manual buy) would purchase $12
* if instead you would defer another 3 days that would mean that you would purchase $30
If we want to implement that, we'll run in a problem: We have to somehow decide how to calculate what has been purchased the past month. We're already running a DB with purchases but we if i setup lovac anew, the DB is not filled with the purchases from bitstamp. On the other hand: Bitstamp only allows you to grab the oldest 1000 entries from your user-transactions. Which means that if we want to run for years, we have to store these transactions ourself. Because of that, the above feature needs a robust syncing mechanism. which preserves older transactions whereas also allows to recreate accounts if they are not older then 3 years (1000 entries is roughly 3 years of daily purchases).
The simplest solution is:
* With initial bitstampconfig you do a full-sync with 1000 entries
* After that, we rely on our own DB
* Aditionally there is a "resync-button" which removes all purchase-entries and make a full-resync
* You can only push that button if you have less then 1000 purchase entries
* We worry about the rest later
Let's go ...


## 22nd april 2018 - Kim
Wow, almost a month without a blogpost but lots of coding. I have some new features. Here is a list:
* The bitstamp-secret is no longer exposed via the UI. Very important for Security-reasons
* also because of security, you can update a bitstamp-config. You have to delete it and create anew
* You can now also change your password
* And admins can create new users via the Admin-dashboard
* You can specify your daily-investment in the "Accumulation setup"
* Also we have numerous bugfixes and we're live on the internets: https://alpha.lovac.xyz
* The above has a nginx in fron which is automatically managing a letsencrypt-certificate
* We have a full CD-Setup. Whenever the tests are green, it gets deployed on alpha
* Also you can now manually buy or defer. More about that in the next blogpost
However, unfortunately we're still not running on a mysql (although we've a mysql-container in our docker-compose-file).

## 27nd march 2018 - Kim
Hmmmm, doesn't look that cheap. We are running now about close to a month on Google's K8S and we've burnt about 1 100 Euro! WOW! I'm not sure but that's by far too expensive to me. Sure, we don't have to pay it as we're getting 300$ from Google, but still, too much of a burnrate.
We have to reduce these costs.
i closed it down for now entirely.
![](k8s-kosten.png)

## 18nd march 2018 - Kim
We will use docker ... all over! And we need to have an outstanding experience when we're using docker in development, in CI/CD and also production. Docker should support us in all these areas:
* Want a lightning-node on your dev-system?
* Want to run the tests but want FAST feedback so that this relies on a docker-image which already has the requirements.txt installed? 
* Want to mock a situation where alice just paid 0.0001 BTC to the lovac-lighnting-node and you want to use fixtures to magically appear that in terms of a docker-containers?
* Want to add some magic to the bitcoin-image we're using or want to patch it with a new version and this process whould be supported until prod?!
* Want to reuse baseimages in order to make things secure and images slim?!

Docker, Docker, Docker!!!
So have to decide how we treat the definition of docker-containers but using them in terms of references at the same time. We want to define EVERYTHING which potentially affects the prod-system to be defined in a repo. We don't want to have multiple-repos (we won't have multi-team-setups, soon) and monorepos are much easier to handle.
That means that we will put all the definitions of a docker-container inside the repo but at the same time we can't rebuild all the images all the time! That's simply not efficient. That means that we need to use tags in order to reuse images which we've created before and that means that images:tag will be refenrenced in the repo alongside with the definition of a image and both will hopefully allways fit together (over all branches/tags/commitIDs).
So how do we solve that on gitlab? That was the question for spikes/issue-3.
First, i played around with lolaus.sh (google it) for some time until i realized that this will create lots of headaches using branches. We would need to switch to a master-commit-approach which is limiting and Manu is not a fan of that. I'm happy that i digged deeper and realized that a simple timestamp-based approach is all what we need. So here is an example


Requirements:
* We have a requirements.txt and want a buildcontainer which contains all the pip-modules in the versions specified in requirements.txt
* Whenever a version changes, the build-container needs a rebuild and needs to be used immediately from that point in time (in that branch)
Solution:
* we create stages which are checking and rebuilding images whenever they are needed in the process.
* In this case a stage "build_reqimage" which starts a script which is comparing the timestamp of the (used) image with the timestamp of requirements.txt
* As images don't have timestamps, we'll use timestamps as tags for these images
* An implementation is here: https://gitlab.com/lovac/lovac-spikes/blob/spikes/issue-3/docker/build.sh#L21-28
* If the image is newer, all fine, nothing to do, over to the next stage
* If the image is older, then rebuild and push it AND change the reference where it is used AND skip the all subsequent stages
* The commit which changed the reference will cause another run
* Features which are needed in order to implement this:
    * https://docs.gitlab.com/ce/ssh/README.html#deploy-keys
    * https://docs.gitlab.com/ce/ci/variables/README.html#secret-variables
    * https://docs.gitlab.com/ce/ci/ssh_keys/README.html#ssh-keys-when-using-the-docker-executor

That way, this is simple to use, follows best practices in terms of references and works seamlessly over branches.
However there are still questions remaining:
* How does a developer inject his changes as seamlessly as possible BEFORE committing? (different run-modes for build.sh-scripts?!)
* ... ?!

## 17nd march 2018 - Kim
Finally we found a more reasonable way to setup the whole Application with a proper understanding of how the Application Context and an Application Factory works. While fiddling, one thing also became clear: You need to seperate the instantiation and the initialisation of the Application:
(from singleton.py)
```python
app = lovac.create_app()
app.app_context().push()
# (...)
lovac.init_app(app)
```
If you would put everything in the create-call, you can't import code which is dependent on an initialized ApplicationContext, you can't do "from flask import current_app". So you have to push the app_context but on the other side, you don't want to do that from within the create_app-function because this would be a quite shitty side-effect which srews up your whole dependency injection.

## 10nd march 2018 - Kim
We need to get testing right! CD ftw! Manu is currently adapting to mysql and that made some changes necessary which introduced complexity. And we're not yet testing anything with mysql. Especially manual adaptions to sql-queries is horrible. Because of that, we should do everything with SQL-Alquemy. I guess i'll create a task for that.
Maybe it's also possible to double the tests and test everything with Mysql. We should probably also separate the unittests from the integrationtests but we did tradeoff here which i'd always do like that:
What's better: unittests in the beginning or integration-tests?
Answer: integration-tests
You won't get any POC going if you do TDD from the very beginning.

## 6nd march 2018 - Kim - BTC-Price USD 10.618,-
In the meantime we have a tiny Admin-UI and can create Users. The cronjob though is not multi-user-capable. We are using JWT for the Admin-UI and we have a small rest-interface which we can enlarge. Just the Vue.js thingy is not progressing. On the upside we have a test-coverage constantly raising and now being 74%.
So let's try to progress on the Business-side again a liitle bit. The obvious one is to get the cronjob going for multi-User but i would also like to make the buying a bit more fancy. So here are some stories in order to derive a Datamodel from it:
* Each user can configure more then one token.
* For each token you're setting up an accumulation plan
* An accumulation plan gives a monthly amount and assumes "daily purchases". Minimum is "180$ daily"
* Each day the user can decide about his daily purchase:
    * Defer it today (and even defer it tomorrow)
    * Buy an amount whereas (monthly/30) < amount < dayOfMonth*(monthly/30)-alreadySpentThisMonth
    * Effectively that way a user can accumulate more purchasing-power by defering a buying-decision (in a bear-market)
* On the last day of the month, the user can't decide/defer anymore, monthly-alreadySpentThisMonth will get purchased
Because of that we will have:
* BitstampConfig will get two new attributes: monthlyAmount and DeferCounter
    * the DeferCounter is zero by default. If the cronjob wants to buy but the DeferCounter > 0, then the purchase gets deferred today and the counter get decreased by one


## 25nd Feb 2018 - Kim
In short: Vue.js looks much more flexible and less bloaty than this Angular > V.1 stuff. I really liked Angular but look into issue#1 for details.
As a resulted i looked into Vue.js and that looks pretty neat. Much easier, better to smoothly plug it in and very lightweight. I also found a nice integration-template and as a result as a first step i'm creating a REST-Interface with JWT-Auth into lovac. 


## 21nd Feb 2018 - Kim
Again i want to catchup with my purchases. For heaven sake the automation is now working and yesterday night for the first time i made an automated purchase.
Also this now contains multimonth data and i want to have a better overview over my purchases in within a month. So maybe i want to improve the the UI somehow quickly. Something which was also important when i wanted to have a datepicker but couldn't have one for the tickerdata.
I guess somehow i should get a better overview over frontend-tech. If i want nice dashboards which gives me the overview over a month, or a year and a purchase-list and such kind of stuff i guess we need some more clever UI-frontend-tech. I'm particulary thinking about more clever javascript clients, maybe doing Ajax-Calls and such.
For that reason let's do some research. What there, more or less ready to integrate with Flask. How is Flask doing such kind of stuff anyhow? So let's collect some nuggets on the internet and do some review here, in the blog.

## 20nd Feb 2018 - Kim - BTC-Price $11568
Yesterday, i hopefully made the automatic purchase feature work, at least for the admin-user for now. I changed my feelings again. The last blog-entry is from 4th of Feb (BTC $8900). At that time, bitcoin was in decline since almost 2 month. So now BTC is rising again since 6nd Feb and if i look on my dashboard i feel bad to not have purchased relentlessly. Once i calculated how much i've not spent and manually purchased on Bitstamp directly to "catchup". So if the automatic-buying feature works (for every user) maybe that's enough to go live from a feature-perspective? We should go live with a smaller than minimum set anyway, shouldn't we?

## 04nd Feb 2018 - Kim
After using my own system for almost a month, i'm unhappy. I have the feeling that i could have done much better if i'm not buying randomly on a daily basis. I want to invest a bit more clever and i want that automated. And i don't think that a simple cronjob will do it even initially. This should be a reasonable sustainable service.
Ok, let's put some thought into it. There are certain variables here:
* How much to invest (regulary) in which time-window
* how flexible you are within that timewindow in order to get the best price

The default-setup is 6$ a day which really comes down to 180$ a month.
So now we can get more flexible with that setup. 
stage1: Try to get a good daily price. So let's assume we would run a cronjob every hour: We would have 24 occasions to buy and we have to decide for one of those. Numerous strategies might come to our minds. But i also think about bysing the dip which somehow needs stage2 to be effective.
stage2: We know that we're in a very volatile scenario. If prices really get low for  shorter time (and we always assume that on almost every time-window) we would profit much bigger if we would invest more then $5 but on the other hand do not invest for a lot of other days.
stage2+: Would ideally only invest exactly once a month but on very good day.
So one way of figuring out whether we're currently dipping is "Bollinger bands".
https://en.wikipedia.org/wiki/Bollinger_Bands
The Article reveals the parameters which are important here:
* the period (not mentioned but implicitely in there)
* the n-period moving average
* The upper-band (irrelevant for us, because we're only buying)
* the lower-band at k times an N-Period standard deviation

The period is clear: we've always thought about hourly charts. This is because the minimum strategy (stage1) gives us choice to invest in 24 occasions a day. We're not doing HFT and we want to be responsible with calculations (with millions of potential users) so let's more or less decide to talk about hourly prices.
The N-Period: That's more tricky. Even on stage2+ we HAVE to buy once a month. So what we need is a combination of N-Period and k which is giving us a buy-signal in a median of 1 per month. 
Puh, difficult. I'm not willing to calculate that but here is a way to do that:
https://www.youtube.com/watch?v=Q1GXhDtg_90
https://www.youtube.com/watch?v=eyJtb4YVPZ8
(whole course: https://www.youtube.com/playlist?list=PLAwxTw4SYaPnIRwl6rad_mYwEk4Gmj7Mx)
So let's do that rather later for homework and simply start with some thumb-values:
* The 50day moving average is a very popular one around TA-people. So because we're using hourly-charts, let's take the 50*25 which is the 1250MA. Might be a performance desaster to calculate, but let's worry about that later.
* Standard values for N and k are 20 and 2. As we chose basically 50 for N, so let's take and arbitrary lower k: 1.
Next toDos: Let's implement the lower bollinger-band visually and then see how many buys-decisions we get depending on variating N and k. As a side-effect, let's improve visualizing the ticker-data:
* choose a month
* show bollinger-band 

## 30nd Jan 2018 - Kim
The vision gets clearer but fuzier as well. Let's somehow organize the sound (bitcoin) part of the world's retirements funds in a trustless manner ... or as trustless as possible.
Some more ideas what we might do and how it relates to the vision:
* use https://en.bitcoin.it/wiki/Timelock for locking money until you're retiring. Might sound cool but i guess it's difficult to get people using it. And also it would be difficult for us to create such addresses with the security and truslessness needed
* The refill idea: There are thousands of people who would like to spend bitcoin but refill them whenever they spend some. People could register Watching-only wallets and we would rebuy bitcoins automatically as people are spending them. Might be a distraction from the original idea but maybe even more worth doing? At least the implication is more difficult.


## 26nd Jan 2018 - Kim
This is an internal communication medium in order to make design-decisions transparent for later purposes 
