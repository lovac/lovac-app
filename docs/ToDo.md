Eine kleine ToDo-Liste um besser zu priorisieren (Backlog anyone ;-) )

* Tickerdata: Implementierung von Monaten bei der Anzeige, Bollingerbands, refactor on the way

* Tests: Die Bitstamp-API muss gemocked werden damit man ordentliche und bessere Tests schreiben kann. 
http://flask.pocoo.org/docs/0.12/testing/ (z.T. erledigt, siehe Tests)

* Endlich automatisierten buy implementieren: Wer um 11pm noch nicht gekauft hat --> autobuy 



* https://github.com/lightninglabs/lightning-coindesk zum Laufen bringen und verstehen, danach auf Lovac portieren


erledigt
========

* Umstellen auf eine UserDB: SQLAlchemy ans laufen bringen, ungefähr so:
http://docs.sqlalchemy.org/en/latest/orm/tutorial.html
Die Datenbankinitialisierung sollte danach halt schonmal einen User anlegen
(erledigt)

* Speichern der API-Token in der DB (encrypted mit key der nur im Memory vorhanden ist und salted mit dem Username) (erledigt)