docker pull cockroachdb/cockroach:v2.0.2
docker tag cockroachdb/cockroach:v2.0.2 registry.gitlab.com/lovac/lovac-app/cockroach:v2.0.2
docker push registry.gitlab.com/lovac/lovac-app/cockroach:v2.0.2