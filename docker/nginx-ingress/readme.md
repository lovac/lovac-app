The nginx-ingress manages all ingresses.

    docker pull quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.14.0
    docker tag quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.14.0 registry.gitlab.com/lovac/lovac-app/nginx-ingress-controller:0.14.0 
    docker push registry.gitlab.com/lovac/lovac-app/nginx-ingress-controller:0.14.0 