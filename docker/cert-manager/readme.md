The cert-manager manages the generation of ssl-certificates via letsencrypt.
We're using v0.3.0 and copied the dockerimage from the official place:

    docker pull quay.io/jetstack/cert-manager-controller:v0.3.0
    docker tag quay.io/jetstack/cert-manager-controller:v0.3.0 registry.gitlab.com/lovac/lovac-app/cert-manager-controller:v0.3.0
    docker push registry.gitlab.com/lovac/lovac-app/cert-manager-controller:v0.3.0

