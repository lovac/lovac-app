#!/bin/bash
set -e

export CLUSTERNAME=my-test-cluster3

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -c|--clustername)
    CLUSTERNAME="$2"
    shift # past argument
    shift # past value
    ;;
    -m|--minikube)
    MINIKUBE="true"
    shift # past argument
    echo "    --> Deploying on minikube"
    ;;
    *)    # unknown option
    POSITIONAL="$1" # save it in an array for later
    shift # past argument
    ;;
esac
done

start=`date +%s`

if [ -f "/credentials/gce-service-account.json" ]; then
    export CLIENT_EMAIL=$(jq -r '.client_email' /credentials/gce-service-account.json)
    echo "    --> Read credentials for gcould"
fi

if [ ! -z $CLIENT_EMAIL ]; then
    echo "    --> Get the Auth-stuff done for GCE"
    gcloud auth activate-service-account $CLIENT_EMAIL --key-file=/credentials/gce-service-account.json
    gcloud config set project lovac-spike-05-2018
    
    echo "    --> get credentials"
    export CLUSTERNAME=my-test-cluster3
    gcloud container clusters get-credentials ${CLUSTERNAME} --zone us-west1-c

    echo "    --> Check whether cluster is running and get access"
    gcloud container clusters list | grep ${CLUSTERNAME} || exit 2
    gcloud container clusters get-credentials ${CLUSTERNAME} --zone us-west1-c
fi



echo "    --> Check for proper gitlab-registry-creds"
kubectl get secret gitlab-registry-creds | grep gitlab-registry-creds || exit 2

echo "    --> Let's deploy the lovac-app"
# ToDo: 
# - check whether lovac is deployed kubectl get pods | grep lovac or something like that
# - if not apply like below (:latest)
kubectl apply -f k8s/lovac/app.yaml --record
# - if yes, https://tachingchen.com/blog/kubernetes-rolling-update-with-deployment/ 
#    kubectl set image deployment lovac-deployment lovac=registry.gitlab.com/lovac/lovac-app:${CI_PIPELINE_ID} --record

echo "    --> Prepare to be able to use Helm"
kubectl get serviceaccount --all-namespaces | grep tiller || kubectl create serviceaccount --namespace kube-system tiller
kubectl get clusterrolebinding | grep tiller-cluster-rule || kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller --upgrade
kubectl rollout status deployment/tiller-deploy -w -n kube-system

echo "    --> Install the nginx-ingress-controller with Helm"
helm ls --all nginx-ingress | grep DEPLOYED || helm install --name nginx-ingress stable/nginx-ingress --set rbac.create=true
kubectl rollout status deployment/nginx-ingress-controller -w
echo "    --> Sleep for 60 seconds to ensure IP-Address"
sleep 60

echo "    --> grep the IP of the Loadbalancer and get it a DNS name"
export LB_IP=$(kubectl get svc nginx-ingress-controller -o json | jq -r '.status.loadBalancer.ingress[].ip')
CURRENT_IP=$(gcloud dns record-sets list --zone=gce-lovac-xyz | grep hulk.gce.lovac.xyz | sed -e 's/.*A     60     //g')
echo "    --> CURRENT_IP=$CURRENT_IP , the correct IP is $LB_IP"
if [[ "$LB_IP" != "$CURRENT_IP" ]]; then
    gcloud dns --project=lovac-spike-05-2018 record-sets transaction start --zone=gce-lovac-xyz
    if [[ ! -z "$CURRENT_IP" ]]; then
        echo "    --> removing current IP-Entry for hulk.gce.lovac.xyz"
        gcloud dns --project=lovac-spike-05-2018 record-sets transaction remove --zone=gce-lovac-xyz \
            --name hulk.gce.lovac.xyz --ttl 60 \
            --type A "$CURRENT_IP"
        gcloud dns --project=lovac-spike-05-2018 record-sets transaction remove --zone=gce-lovac-xyz \
            --name prom.gce.lovac.xyz --ttl 60 \
            --type A "$CURRENT_IP"
        gcloud dns --project=lovac-spike-05-2018 record-sets transaction remove --zone=gce-lovac-xyz \
            --name mon.gce.lovac.xyz --ttl 60 \
            --type A "$CURRENT_IP"
    fi
    echo "    --> Adding the IP $LB_IP to DNS-name"

    gcloud dns --project=lovac-spike-05-2018 record-sets transaction add ${LB_IP} --name=hulk.gce.lovac.xyz. --ttl=60 --type=A --zone=gce-lovac-xyz
    # We can potentially expose it later so let's reserve already 2 more dns-names
    gcloud dns --project=lovac-spike-05-2018 record-sets transaction add ${LB_IP} --name=prom.gce.lovac.xyz. --ttl=60 --type=A --zone=gce-lovac-xyz
    gcloud dns --project=lovac-spike-05-2018 record-sets transaction add ${LB_IP} --name=mon.gce.lovac.xyz. --ttl=60 --type=A --zone=gce-lovac-xyz
    gcloud dns --project=lovac-spike-05-2018 record-sets transaction execute --zone=gce-lovac-xyz
    echo "    --> Sleep for 60 seconds to ensure DNS-Update"
    sleep 60
fi



echo "    --> Install the cert-manager with Helm"
helm ls --all cert-manager | grep DEPLOYED || \
helm install \
    --name cert-manager \
    --version 0.3.0 \
    --namespace kube-system \
    --set image.repository=registry.gitlab.com/lovac/lovac-app/cert-manager-controller \
    --set image.pullSecret=gitlab-registry-creds \
    ./helm-charts/cert-manager

echo "    --> Configure Let's encrypt"
kubectl apply -f k8s/cert-manager/certmanager-issuer-staging.yaml
kubectl apply -f k8s/cert-manager/certmanager-issuer.yaml
while [[ "$(kubectl get issuer letsencrypt --output=jsonpath={.status.conditions\[0\].type})" !=  "Ready" ]]; do echo "waiting ..."; sleep 5; done

echo "    --> Get us a cert for hulk.gcs.lovac.xyz"
kubectl apply -f k8s/lovac/cert3.yaml
while [[ "$(kubectl get certificates hulk-gce-lovac-xyz -o json | jq -r '.status.conditions[0].type')" !=  "Ready" ]]; do echo "waiting ..."; sleep 5; done

echo "    --> Create the ingress using the cert above"
kubectl apply -f  k8s/lovac/ingress443.yaml

end=`date +%s`

runtime=$((end-start))

echo "    --> SUMMARY: This took $runtime"