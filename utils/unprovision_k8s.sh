#!/bin/bash
set -e

export CLUSTERNAME=my-test-cluster3

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -c|--clustername)
    CLUSTERNAME="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL="$1" # save it in an array for later
    shift # past argument
    ;;
esac
done

start=`date +%s`

source utils/common.sh

gcloud config set project lovac-spike-05-2018

echo "    --> remove the cluster"
kubectl delete --ignore-not-found --all ingress
sleep 30
helm ls --all nginx-ingress | grep DEPLOYED && helm delete nginx-ingress # This should also remove the L4-GCE-Loadbalancer
sleep 30
gcloud container clusters delete my-test-cluster3 --zone us-west1-c --quiet

end=`date +%s`

runtime=$((end-start))

echo "    --> SUMMARY: This took $runtime"