#!/bin/bash
set -e

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -m|--minikube)
    MINIKUBE="true"
    shift # past argument
    echo "    --> Deploying on minikube"
    ;;
    *)    # unknown option
    POSITIONAL="$1" # save it in an array for later
    shift # past argument
    ;;
esac
done

source utils/common.sh

echo "    --> Make helm ready"
helm init --upgrade
helm repo update

echo "    --> Create monitoring namespace"
kubectl get namespace monitoring | grep -q monitoring || kubectl create namespace monitoring

echo "    --> Install prometheus with Helm"
helm ls --all | grep "^prom" | grep -q DEPLOYED || helm install --namespace monitoring --name prom --wait stable/prometheus
kubectl rollout status deployment prom-prometheus-server --namespace monitoring

echo "    --> Grafana"
helm ls --all graf | grep "^graf" | grep -q DEPLOYED || helm install --name graf --namespace monitoring --wait -f k8s/grafana/datasources.yaml stable/grafana
kubectl rollout status deployment graf-grafana --namespace monitoring

echo "    --> Run this to get access to Grafana: "
echo 'POD_NAME=$(kubectl get pods --namespace monitoring -l "app=grafana" -o jsonpath="{.items[0].metadata.name}")'
echo 'kubectl --namespace monitoring port-forward $POD_NAME 3000'


