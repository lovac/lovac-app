#!/bin/bash
set -e

export CLUSTERNAME=my-test-cluster3

echo "Provisioning a Kubernetes Cluster"
echo "================================="

echo "    --> Evaluating parameters"
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -c|--clustername)
    CLUSTERNAME="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL="$1" # save it in an array for later
    shift # past argument
    ;;
esac
done


start=`date +%s`

echo "    --> Start executing at $start"

source utils/common.sh

echo "    --> set project lovac-spike-05-2018"
gcloud config set project lovac-spike-05-2018

echo "    --> Create the cluster"
gcloud container clusters list | grep ${CLUSTERNAME} || \
gcloud container clusters create ${CLUSTERNAME} \
    --num-nodes 2 \
    --zone us-west1-c \
    --machine-type g1-small \
    --cluster-version 1.10.7-gke.1 \
    --node-version 1.10.7-gke.1 
gcloud container clusters get-credentials ${CLUSTERNAME} --zone us-west1-c

echo "    --> Create the gitlab-registry-creds to download images from the gitlab-registry (default namespace)"
kubectl get secret gitlab-registry-creds | grep gitlab-registry-creds || \
kubectl create secret docker-registry gitlab-registry-creds \
    --docker-server=registry.gitlab.com \
    --docker-username ${GL_NAME} \
    --docker-password ${GL_TOKEN} \
    --docker-email ${GL_MAIL}

# Is that the right way to go? I'd rather dig into RBAC to enable serviceAccounts get the credentials ... but later
echo "    --> Create the gitlab-registry-creds to download images from the gitlab-registry (kube-system namespace)"
kubectl get secret gitlab-registry-creds -n kube-system | grep gitlab-registry-creds || \
kubectl create secret docker-registry gitlab-registry-creds \
    --docker-server=registry.gitlab.com \
    --docker-username ${GL_NAME} \
    --docker-password ${GL_TOKEN} \
    --docker-email ${GL_MAIL} \
    -n kube-system

end=`date +%s`

runtime=$((end-start))

echo "    --> SUMMARY: This took $runtime"