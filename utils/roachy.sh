#!/bin/bash


help () {
cat <<HEREDOC
$(basename "$0") [command] [flags -- conveniently do things with cockroachDB

where:
  
  command:
    sql     - opens a sql-console
    bash    - enters bash in the container
    backupsql - will create a backup of the SQL-DB and stores it on the server timestamped
  
  flags:
    -h    show this help text
    -s    opens sql-console
    -b    opens bash and maybe executes all what's coming after the -b 
    --tail-bc           tail -f the bitcoin-log
    --tail-bc-testnet3  tail -f the bitcoin-log of testnet3
    --tail-lnd-testnet3 tail -f the lnd-log of testnet3

some hints for the SQL-console:

SHOW TABLES;
SHOW CREATE TABLE btcaddress;
ALTER TABLE user ADD COLUMN z DECIMAL DEFAULT (DECIMAL '1.4');
ALTER TABLE purchase ADD COLUMN names STRING;
ALTER TABLE purchase RENAME COLUMN btc_usd TO btc_fiat;
ALTER TABLE purchase RENAME COLUMN usd TO fiat;
ALTER TABLE purchase ADD COLUMN currency STRING(10);

HEREDOC
}

id=$(docker inspect --format="{{.Id}}" lovac-app_roach_1)
backup_path="backups/"

if [[ $1 == -* ]]; then
    echo "Command missing!"
else
    command=$1
    shift
fi

set -e

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --backupsql)
    mode="backupsql"
    shift # past argument
    ;;
    -s|--sql)
    mode="sql"
    shift # past argument
    echo "    --> entering sql"
    echo "Some hints for the SQL console:"
    ;;
    -b|--bash)
    mode="bash"
    shift # past argument
    echo "    --> entering bash"
    ;;
    -w|--web)
    mode="web"
    shift # past argument
    ;;
    --tail-bc)
    mode="tail-bc"
    shift # past argument
    ;;
    --tail-bc-testnet3)
    mode="tail-bc-testnet3"
    shift # past argument
    ;;
    --tail-lnd-testnet3)
    mode="tail-lnd-testnet3"
    shift # past argument
    ;;
    --all)
    all="true"
    shift # past argument
    ;;
    --backup-path)
    backup_path="$2"
    shift # past argument
    shift
    ;;
    --debug)
    set -x
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL="$POSITIONAL $1" # save it in an array for later
    shift # past argument
    ;;
esac
done

if [[ "$command" = "backupsql" ]]; then
    if [ "$hostname" == "dogan" ]; then
	    docker exec -it ${id} /cockroach/cockroach.sh dump lovac --insecure > ~/backupsql/
    else
        id=$(ssh lovac@beta.lovac.xyz "docker inspect --format="{{.Id}}" lovac-app_roach_1")
        ssh lovac@beta.lovac.xyz "docker exec -i ${id} /cockroach/cockroach.sh dump lovac --insecure" > ${backup_path}/lovac-app-`date +%Y-%m-%d-%H-%M-%S`.sql
    fi
elif [[ "$command" = "sql" ]]; then
    cat <<HEREDOC
Some hints for SQL ...

    SHOW TABLES;
    SHOW CREATE TABLE btcaddress;
    ALTER TABLE user        ADD     COLUMN  some_number DECIMAL DEFAULT (DECIMAL '1.4');
                purchase                    names STRING;
                user                        lfee INTEGER;
                payment     RENAME  COLUMN  created_at TO created_at2;
                payment     DROP    COLUMN  created_at2;
    UPDATE      payment SET chainid = 'testnet3' WHERE chainid is NULL;

HEREDOC
    if [ "$hostname" == "dogan" ]; then
	    docker exec -it ${id} /cockroach/cockroach.sh sql --insecure -d lovac
    else
        id=$(ssh lovac@beta.lovac.xyz "docker inspect --format="{{.Id}}" lovac-app_roach_1")
        ssh lovac@beta.lovac.xyz "docker exec -i ${id} /cockroach/cockroach.sh sql --insecure -d lovac"
    fi
elif [[ "$command" = "bash" ]]; then
	if [[ $POSITIONAL = "" ]]; then
		docker exec -it ${id} bash $*
	else
		docker exec -it ${id} $POSITIONAL
	fi
elif [[ "$command" = "web" ]]; then
	ssh -f lovac@beta.lovac.xyz -L 8080:localhost:8080 -N
	echo "    --> Creating tunnel to beta"
	ssh -f lovac@beta.lovac.xyz -L 8080:localhost:8080 -N
	echo "    --> Check http://localhost:8080"
elif [[ "$command" = "tail-bc" ]]; then
    ssh -t admin@192.168.178.92 'sudo tail -f /home/bitcoin/.bitcoin/debug.log'
elif [[ "$command" = "tail-bc-testnet3" ]]; then
    ssh -t admin@192.168.178.92 'sudo tail -f /home/bitcoin/.bitcoin/testnet3/debug.log'
elif [[ "$command" = "tail-lnd-testnet3" ]]; then
    ssh -t admin@192.168.178.92 'sudo tail -f /home/bitcoin/.lnd/logs/bitcoin/testnet/lnd.log'
elif [[ "$command" == "logs" ]]; then
    if [ "$hostname" == "dogan" ]; then
	    docker exec -it ${id} /cockroach/cockroach.sh sql --insecure -d lovac
    else
        id=$(ssh lovac@beta.lovac.xyz "docker inspect --format="{{.Id}}" lovac-app_web_1")
        if [[ "$all" = "true" ]]; then
            ssh lovac@beta.lovac.xyz "{ cat logs/* | cut -d'|' -f2 && docker logs ${id} ; }" 
        else
            ssh lovac@beta.lovac.xyz "docker logs ${id} "
        fi
    fi
else
	help
fi
