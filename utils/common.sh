#!/bin/bash
set -e

echo "    --> Managing GCE credentials"
if [ -f "/credentials/gce-service-account.json" ]; then
    echo "    --> Read credentials for gcould"
    export CLIENT_EMAIL=$(jq -r '.client_email' /credentials/gce-service-account.json)

    echo "    --> Get the Auth-stuff done for GCE"
    gcloud auth activate-service-account $CLIENT_EMAIL --key-file=/credentials/gce-service-account.json
    gcloud config set project lovac-spike-05-2018
    
    echo "    --> get credentials"
    export CLUSTERNAME=my-test-cluster3
    gcloud container clusters get-credentials ${CLUSTERNAME} --zone us-west1-c || echo "could not get credentials. Probably not running"

    # Do we really need that? Let's skip it for now and hope that it works out.
    #echo "    --> Resize the cluster to 3"
    #gcloud container clusters resize ${CLUSTERNAME} --node-pool default-pool --zone us-west1-c --quiet \
    #    --size 3
fi

echo "    --> Managing Gitlab credentials"
if [ -f "/credentials/gitlab-service-account.json" ]; then
    echo "    --> found gitlab credentials in /credentials/gitlab-service-account.json"
    config_file=/credentials/gitlab-service-account.json
elif [ -f ~/.gitlab/gitlab-service-account.json ]; then
    echo "    --> found gitlab credentials in ~/.gitlab/gitlab-service-account.json"
    config_file=~/.gitlab/gitlab-service-account.json
else
    echo "    --> No gitlab-credentials found" && exit 2
fi

export GL_TOKEN=$(jq -r '.docker_password' $config_file)
export GL_MAIL=$(jq -r '.docker_email' $config_file)
export GL_NAME=$(jq -r '.docker_username' $config_file)