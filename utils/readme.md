A couple of utility-scripts mainly having a glue-code-character.

# build.sh
should probably names build-image.sh and is building images and updating references after it has build and pushed the image. It's the core of the docker-dependency approach of lovac.

# common.sh
is sourced by many "cloud-managing scripts" (see below) which need access-rights to gcloud or k8s.
It assumes that the developer already has the access-rights OR it will read them from the two json-files found on the gitlab-runner-node which itself is mounting the credentials:

'''
[[runners]]
  name = "voldemort-spikes"
  url = "https://gitlab.com/"
  token = "e5af8e51e178e6ee24da284141e816"
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "docker:latest"
    privileged = true
    disable_cache = false
    volumes = ["/cache","/etc/gitlab-runner/mount:/credentials:rw"]
    shm_size = 0
  [runners.cache]
'''

and two file in there:

the gcloud-serviceaccount is explained here:
https://gitlab.com/lovac/lovac-spikes/tree/spikes/issue-4#gce-service-account-and-gitlab-runner-integration

'''
root@voldemort:/etc/gitlab-runner/mount# cat gce-service-account.json 
{
  "type": "service_account",
  "project_id": "lovac-spike-05-2018",
  "private_key_id": "213f5263f4e50d4b7520ea5222a541eb92a7c793",
  "private_key": "-----BEGIN PRIVATE KEY-----\n ..... a long key here ...l8bk\n-----END PRIVATE KEY-----\n",
  "client_email": "voldemor-gl-runner@lovac-spike-05-2018.iam.gserviceaccount.com",
  "client_id": "104476762572402369875",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://accounts.google.com/o/oauth2/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/voldemor-gl-runner%40lovac-spike-05-2018.iam.gserviceaccount.com"
}
'''

This is a self-invented format. The credentials here need to be created on personal-access-tokens:
https://gitlab.com/profile/personal_access_tokens
The docker_email is therefore the mail-address of the person which owns the access-token:

'''
{
  "docker_password": "shhhh...a.secret",
  "docker_email": "kneunert@gmail.com",
  "docker_username": "the username"
}
root@voldemort:/etc/gitlab-runner/mount#
'''
