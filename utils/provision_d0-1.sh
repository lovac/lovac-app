#!/bin/bash

# for debugging
# set -x

echo "    --> Loading token"
export TOKEN=$(cat instance/do.key)
NAME="test01"

#curl -X GET "https://api.digitalocean.com/v2/actions" \
#		-H "Authorization: Bearer $TOKEN"

#curl -X GET "https://api.digitalocean.com/v2/droplets" \
#		-H "Authorization: Bearer $TOKEN"

echo "    --> Check for droplet existing"

RESPONSE=$(curl -s -X GET "https://api.digitalocean.com/v2/droplets" -H "Authorization: Bearer $TOKEN" | jq --arg name "test01" '.droplets[] | select(.name==$name) | contains({"name":$name})')
if [ $RESPONSE != "true" ];

REQUEST=jq -n --arg name $NAME  '{"name":$name,"region":"fra1","size":"s-1vcpu-1gb","image":"ubuntu-16-04-x64"}'

curl -X POST "https://api.digitalocean.com/v2/droplets" \
	-d "$REQUEST" \
	-H "Authorization: Bearer $TOKEN" \
	-H "Content-Type: application/json"
fi

echo "Let's wait on the machine being available"
for (( ; ; ))
do 
   echo -n "."
   echo "Pres CTRL+C to stop..."
   sleep 1
   RESPONSE=$(curl -s -X GET "https://api.digitalocean.com/v2/droplets" -H "Authorization: Bearer $TOKEN" | jq -r --arg name "$NAME" '.droplets[] | select(.name==$name) | .status')
   echo "$RESPONSE"
   if [[ $RESPONSE = "active" ]]
   then
	break       	   #Abandon the loop.
   fi
done

fi



