#!/bin/bash
echo Wait for servers to be up
sleep 10

HOSTPARAMS="--host roach --insecure"
SQL="/cockroach/cockroach.sh sql $HOSTPARAMS"

$SQL -e "CREATE DATABASE lovac;"
/cockroach/cockroach.sh user set lovac $HOSTPARAMS
$SQL -d lovac -e "GRANT ALL ON DATABASE lovac TO lovac"