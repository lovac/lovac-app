#!/bin/bash

export IP_ADDRESS=138.68.15.197
ssh root@${IP_ADDRESS} "sudo apt-get install fail2ban git"

echo "    --> Install Docker ..."
ssh root@${IP_ADDRESS} "sudo apt-get remove docker docker-engine docker.io"
ssh root@${IP_ADDRESS} "sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common"
ssh root@${IP_ADDRESS} "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -"
ssh root@${IP_ADDRESS} "sudo apt-key fingerprint 0EBFCD88"
ssh root@${IP_ADDRESS} 'sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"'
ssh root@${IP_ADDRESS} 'sudo apt-get update'
ssh root@${IP_ADDRESS} 'sudo apt-get install docker-ce'
ssh root@${IP_ADDRESS} 'sudo docker run hello-world'

echo "    --> Install Docker-compose ..."
ssh root@${IP_ADDRESS} "sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose"
ssh root@${IP_ADDRESS} "sudo chmod +x /usr/local/bin/docker-compose"
ssh root@${IP_ADDRESS} "docker-compose --version"

echo "    --> Crteating user and make ready for checkout"

ssh root@${IP_ADDRESS} "adduser --disabled-password --gecos '' lovac"
ssh root@${IP_ADDRESS} "sudo usermod -a -G docker lovac"

ssh root@${IP_ADDRESS} "mkdir -p /home/lovac/.ssh"
ssh root@${IP_ADDRESS} "chmod 700 /home/lovac/.ssh"

ssh root@${IP_ADDRESS} 'ssh-keygen -t rsa -f /home/lovac/.ssh/id_rsa -q -P ""'
ssh root@${IP_ADDRESS} 'cat /home/lovac/.ssh/id_rsa.pub'
ssh root@${IP_ADDRESS} "ssh-keyscan -H 52.167.219.168 >> /home/lovac/.ssh/known_hosts"

ssh root@${IP_ADDRESS} 'echo "|1|pMIFti+tv0eb3gkrmDip1WZrs1E=|nhBkGy3BIibKl/3Xj8AirUbplPU= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> /home/lovac/.ssh/known_hosts'
ssh root@${IP_ADDRESS} "echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDCxh9AuYHgt41i3xhdvj1Aa6shN20gy0WrIAw+67WQcqtpKg+n6SSwRFs8DLpBaWtnggqGLKhN+5/cTRRLq3sC5nJcKdwRp+7zBAMEJtV60+9vWGuQL6QKLltWPaExdC2uWYKKwcGKhsAKmBOcuZeW0tes+vAw12MPFw2/pjL3mJBjKRq5tFimnkKfiE2qsLML2G7WM0bBvhyN8hJCi+uzfRJm1BkF7+LfwzhfwPkdIbWQz7ygwVFkPLuIgxIEN4HGqlzAa0Axz3SmXHEaB4uY+Becd8Dg80/MwpzuJfd1GDJV/AzPK/+Ayj1D48jySffBdIl1lIaHq+RQjSWyABkp lovacci@secretvariables\n
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjwLDQqe1UoHcDWo8dfIsXRjspCoOuw52TuUTcOM91FVSoMR3Prl8tMetsHKNqfk/Xl1LnsEbfhRzADhhElH64nujRAs7hCvMDOm9gMyFhGSf0wk4R1EdExhoooj83RlzZ9qHP9eXz4RsJz5SHms7qmVV3BKL+KU5nt2q32o6ktk8KUPWQjMyncpZYvulZZ64C6PFzSvNoxA4haPUH//i4CvkyFaIGstcMVOyQpEmmsV0okYCftHRuaQW0C2Mr+sjbIGhDO97YmhDnNH69pY4ElbFIMDFlkC92ZrNElLQv0ApRmnHaiRiFwos0YYdG1NKsgBq62u1CcbKdDyOa7ZeJ kim@nucy\n
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCapBvacf/PN9/lh/hTG8MES2T+ZQ0FacCu0tR/HXvu9n9OCnXZClQCPkfpYjZqBqGpQTAQkA55UqPp7s2z3T8OePvvyjeKaRg2imwa48eltY/mY9d0HUdo9eoSb16qe05roOlT0gKwgEoNliLOecWoQNHnuAVZGTdxA722o7wzUS8bcQXBjoF5uA+suk83KSGiWBce+YjybaZaE+hhHObroCZrMlbZli7h8QmxnnyQ3io0OBR9KNAwTpi3xQWLHUICi1UklraBTJ5sO2J6X6deAbHwOfDecIDor1zfE9DtlSyY1au5Y3zwJPYfg/l7YcyURZJwJNjBck/WoolHh/nD kim@lucky\n
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCyqpQPKFWXIjruoBFHn5sUGwrne8T32nE2aXb9gm8eDJDMf/QhF6It+fMM676v1OfIp50XqnX+NM8AwkETiljRghrf24qDSza2s6UD8YQNqt7AG01SDeeE4gNLfU9aYPAyG/awfYWlo1s3Gy8v5E9MK51wF47nZG2UHbrILBh3IR540WgiotyLQsJBCf3NFbK/4TjfVgABIftQfCfdaE0g8A5fvoTzcLJXE+yum65SNPho5KjRRrvnN4m8wJvHwZ4A9YotIazlpW8XxGBUkkjw/PQ9AlTqDQw068uYNO7dDSQY8cz3E9dteBSqGAozVFvVjlaQ1Ap5apcZuxIgA05h bitcoin@thundroid' > /home/lovac/.ssh/authorized_keys"
ssh root@${IP_ADDRESS} "chmod 600 /home/lovac/.ssh/authorized_keys"
ssh root@${IP_ADDRESS} "chown -R lovac:lovac /home/lovac/.ssh"

ssh root@${IP_ADDRESS} "sudo -H -u lovac bash -c 'cd /home/lovac && git clone git@gitlab.com:lovac/lovac-app.git'"

ssh root@${IP_ADDRESS} "echo 'VIRTUAL_HOST=beta.lovac.xyz\
LETSENCRYPT_HOST=beta.lovac.xyz\
LETSENCRYPT_EMAIL=admin@lovac.xyz' > /home/lovac/lovac-app/.env"

# ToDo: Copying over configuration for LND

# ToDo: 
# Recover data  
# tar -czf data.tar.gz data/
scp root@alpha.lovac.xyz:/home/lovac/lovac-app/data.tar.gz .
scp data.tar.gz root@beta.lovac.xyz:/home/lovac/lovac-app




