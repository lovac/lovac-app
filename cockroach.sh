#!/bin/bash

echo "cockroach setup ..."
RUNNING=$(docker inspect --format="{{.State.Running}}" roach1 2> /dev/null)

if [ "$RUNNING" == "false" ]; then
    echo "    -> container not running, starting now ..."
    docker start roach1
fi

if [ $? -eq 1 ]; then
echo "    -> container not existing, creating now ..."
docker run -d \
    --name=roach1 \
    --hostname=roach1 \
    -p 26257:26257 -p 8080:8080  \
    -v "${PWD}/cockroach-data/roach1:/cockroach/cockroach-data"  \
    cockroachdb/cockroach:v2.0.2 start --insecure
fi




# https://www.cockroachlabs.com/docs/stable/build-a-python-app-with-cockroachdb.html


docker exec -it roach1 ./cockroach sql --insecure -e 'drop database lovac'

echo "    -> creating the database"
docker exec -it roach1 ./cockroach sql --insecure -e 'create database lovac'
echo "    -> creating the user"
docker exec -it roach1 ./cockroach user set lovac --insecure
echo "    -> creating the grant"
docker exec -it roach1 ./cockroach sql --insecure -e 'GRANT ALL ON DATABASE lovac TO lovac'
