''' cron-job-like definitions in lovac '''
import schedule
import traceback
from threading import Thread
import time
from datetime import datetime
import lovac
from flask import current_app as app
import lovac # patch user_is_short_of_fiat
from lovac.services.settings import update_cold_storage
from lovac.services.tickerdata import is_price_in_freefall, is_price_recovering, get_current_tickerdata
from lovac.services.purchase import buy_the_dip_job, automatic_buy, update_autostatus
from lovac.services.mail import send_fiat_shortage
from lovac.services.notifications import consider_notifying, reset_notification, Purposes
from lovac.util import handle_exception, is_last_day_of_month, is_middle_of_month

# All jobs which are defined here and which are executed by the schedule-framework
# are not allowed to throw any exception because otherwise they are running every second
# throwing exceptions by the second.

def on_startup():
    ''' Stuff that should be done soon after startup '''
    try:
        lovac.services.save_tickers_hourly()
    except Exception as exception:
        handle_exception(exception)

def every_3minutes():
    ''' just for testing purposes '''
    try:
         with app.app_context():
            pass
            #lovac.services.mail.send_confirmationmail("kneunert@gmail.com")
    except Exception as exception:
        handle_exception(exception)


def every_hour():
    ''' stuff which should happen every hour '''
    try:
        with app.app_context():
            lovac.services.save_tickers_hourly()
            if is_price_in_freefall():
                price, lowerband = get_current_tickerdata()
                app.logger.info("PRICE is in freefall, currently at {} whereas lower band at {}".format(price,lowerband))
            else:
                if is_price_recovering():
                    buy_the_dip_job()
    except Exception as exception:
        handle_exception(exception)

def notify_users():
    ''' check whether we need to notify users '''
    try:
        with app.app_context():
            users = lovac.services.get_users()
            app.logger.info("iterating over the {} users ...".format(str(len(users))))
            for user in users:
                try:
                    if user.email == "admin" or user.email == "user0":
                        continue
                    app.logger.debug("iterating user {}".format(user.email))
                    if not user.confirmed:
                        app.logger.debug("  user {} is not confirmed!".format(user.email))
                        consider_notifying(user,Purposes.please_confirm_email)
                    if user.bitstampconfig: # user needs a bitstampconfig
                        if lovac.services.user_is_short_of_fiat(user):
                            app.logger.debug("user {} is short of fiat!".format(user.email))
                            consider_notifying(user,Purposes.please_send_fiat) 
                        else:
                            reset_notification(user,Purposes.please_send_fiat)
                except Exception as exception:
                    app.logger.error("Unknown Error for {}".format(user.email))
                    handle_exception(exception,user)
    except Exception as exception:
        app.logger.error("Unknown Error in check_for_monthly_things")
        handle_exception(exception)


def late_buyer():
    ''' It's 23:30h so let's check that we have bought and do it if not yet done '''
    if app.config["ENABLE_LATE_BUYER_JOB"]:
        app.logger.debug("cron: late_buyer started")
    else:
        app.logger.debug("cron: late_buyer skipped due to ENABLE_LATE_BUYER_JOB==False")
        return
    try:
        with app.app_context():
            users = lovac.services.get_users()
            app.logger.info("iterating over the {} users ...".format(str(len(users))))
            for user in users:
                try:
                    if user.email == "admin" or user.email == "user0":
                        continue
                    if user.bitstampconfig: # user needs a bitstampconfig
                        if user.bitstampconfig.automatic_buy_flag: # and activated the automatic_buy
                            if user.bitstampconfig.strategy.code==u'dollar_cost_averaging':
                                lovac.services.purchase.automatic_buy(user)
                                update_autostatus(user,10)
                            elif user.bitstampconfig.strategy.code==u'buy_the_dip' and is_last_day_of_month():
                                lovac.services.purchase.automatic_buy(user) # You have to buy at the end of the month
                                update_autostatus(user,11)
                        else:
                            update_autostatus(user,17)
                    else: 
                        update_autostatus(user,16)
                except lovac.models.LovacError as error:
                    app.logger.error("Lovac Error for {} because {}".format(user.email,str(error) ))
                except Exception as exception:
                    app.logger.error("Unknown Error for {}".format(user.email))
                    handle_exception(exception)         
    except Exception as exception:
        handle_exception(exception)

def check_cold_storage():
    ''' Let's check all the addresses in cold-storage for amount-changes '''
    try:
        update_cold_storage()
    except Exception as exception:
        handle_exception(exception)   

def test_cron():
    ''' Maybe we want that to test at some time (see below) '''
    app.logger.debug("a debug log line")
    app.logger.info("a info log line")
    app.logger.error("a error log line")

def run_schedule():
    ''' The timer-thread will run with this function '''
    while 1:
        try:
            schedule.run_pending()
            time.sleep(3)
        except Exception as exception:
           handle_exception(exception)


class FlaskThread(Thread):
    ''' A special Thread suited to run Flask-apps-context-code in an own Thread 
        This comes from https://stackoverflow.com/questions/39476889/use-flask-current-app-logger-inside-threading
        I'm not 100% sure how the magic is working here, but it seems to work
    '''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app._get_current_object()

    def run(self):
        with self.app.app_context():
            super().run()

@app.before_first_request
def initialize():
    ''' A function which will run before the first request
        See lovac.start_runner() which will trigger this function indirectly via a request '''
    app.logger.debug("entered @app.before_first_request")
    if not app.config["TESTING"]:
        app.logger.info("initialize: Scheduling cronjobs and starting thread ...")
        #schedule.every(3).minutes.do(every_3minutes)
        schedule.every(3600).seconds.do(every_hour)
        schedule.every().day.at("23:45").do(late_buyer)
        schedule.every().day.at("05:50").do(notify_users)
        schedule.every().day.at("23:15").do(check_cold_storage)
        # Does not work yet in multi-user-scenario:
        #schedule.every(3).seconds.do(test_cron)
        thread = FlaskThread(target=run_schedule)
        thread.start()
        try:
            on_startup()
        except:
            pass
    

