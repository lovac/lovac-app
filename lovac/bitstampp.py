''' a wrapper around the bitstamp-module better suited in lovac-code'''
import time
import configparser

import bitstamp.client
from datetime import datetime
from lovac.models import Bitstampconfig, LovacError
from lovac.util import lovac_now, handle_exception
from flask import current_app as app

def get_ticker(quote):
    ''' something like: 
    {'high': '9404.77', 'timestamp': '1517693541', 'last': '9224.47', 'open': '9262.18', 'vwap': '9231.19', 'volume': '449.96758142', 'bid': '9224.47', 'low': '9165.19', 'ask': '9243.31'}
    '''
    return get_publiclient().ticker_hour(quote=quote)

def get_ticker_last(quote):
    ''' the last-price of that ticker '''
    return get_ticker(quote)['last']

def get_publiclient():
    ''' get the publicclient mostly for tickerdata '''
    public_client = bitstamp.client.Public()
    return public_client

def _summary_purchases(transactions, from_date, to_date= lovac_now().date()):
    ''' sums and averages for the purchases in this timewindow 
        the transactions are VANILLA bitstamp transactions!
        THIS SHOULD BE USED FOR TESTING PURPOSES ONLY!!!!
    '''
    summary = {}
    summary["usd_sum"] = 0
    summary["btc_sum"] = 0
    for transaction in transactions:
        tx_date = transaction["datetime"].date()
        if (tx_date >= from_date) and (tx_date <= to_date):
            summary["usd_sum"] += float(transaction["usd"])
            summary["btc_sum"] += float(transaction["btc"])
    summary["btc_usd_avg"] = summary["btc_sum"] / summary["usd_sum"] 
    return summary

class TradingClient(object):
    ''' A Flask-aware TradingClient '''
    def __init__(self, bs_user,key,secret):
        self.trading_client = bitstamp.client.Trading(
            username=bs_user,
            key=key,
            secret=secret
        )
    
    @classmethod
    def from_user(cls, user):
        return cls(
            user.bitstampconfig.username,
            user.bitstampconfig.key,
            user.bitstampconfig.secret
        )

    
    def get_balance(self,quote):
        """
        independent of quote, returns a json like this:
        for usd:
        {'btc_balance': 0.00514260, 'fiat_available': 2.13, 
        'fee': 0.24, 'btc_reserved': 0.00000000, 
        'fiat_balance': 2.13, 'btc_available': 0.00514260, 
        'fiat_reserved': 0.00}
        """
        # why isn't that working :-(
        self.trading_client.get_nonce()
        time.sleep(2)
        balance=self.trading_client.account_balance(quote=quote)
        # Let's make it easy to calculate with that stuff str->floats
        balance["btc_balance"]=float(balance["btc_balance"])
        balance["btc_reserved"]=float(balance["btc_reserved"])
        balance["btc_available"]=float(balance["btc_available"])
        balance["fee"]=float(balance["fee"])
        if quote=="usd":
            balance["fiat_available"]=float(balance["usd_available"])
            balance["fiat_balance"]=float(balance["usd_balance"])
            balance["fiat_reserved"]=float(balance["usd_reserved"])
        elif quote=="eur":
            balance["fiat_available"]=float(balance["eur_available"])
            balance["fiat_balance"]=float(balance["eur_balance"])
            balance["fiat_reserved"]=float(balance["eur_reserved"])
        else:
            raise LovacError("Unknown quote: {}".format(quote))
        return balance
    
    def get_orderbook(self):
        ''' returns the orderbook TBD example '''
        self.trading_client.get_nonce()
        return self.trading_client.open_orders()

    def buy_btc(self,amount,quote):
        ''' buy amount (in bitcoin) of bitcoin '''
        app.logger.debug("buying {} of bitcoin in btc{} market".format(amount,quote))
        # only 8 decimal places:
        amount_string = '{:2.8f}'.format(amount)
        try:
            self.trading_client.get_nonce()
            result = self.trading_client.buy_market_order(amount_string,quote=quote)
        except BaseException as error:
            handle_exception(error)
            raise LovacError(str(error),error)
        result["datetime"] = datetime.strptime(result['datetime'],"%Y-%m-%d %H:%M:%S.%f")
        return result

    def get_transactions(self, quote, limit=100,descending=True): # get by default a bit more then three month
        """
        returns a list like this:
        [{'order_id': 789628842, 'btc': '0.00051233', 'btc_usd': 11711.1, 
        'eur': 0.0, 'usd': '-6.00', 'id': 47855738, 'fee': '0.02', 
        'type': '2', 'datetime': '2018-01-18 18:37:35'}, 
        {'order_id': 783497480, 'btc': '0.00055880', 'btc_usd': 10749.99,
        ..... }]
        """
        # limit maximum is 1000
        if limit > 1000:
            raise lovac.models.LovacError("Maximun of 1000 for limit")
        # This is time/date sorted by default
        self.trading_client.get_nonce()
        transactions=self.trading_client.user_transactions(base="btc",quote=quote,limit=limit,descending=descending)
        app.logger.debug("raw transactions: " + str(transactions))

        for transaction in transactions:
            try:
                transaction["datetime"] = datetime.strptime(transaction['datetime'],"%Y-%m-%d %H:%M:%S.%f")
            except ValueError: 
                transaction["datetime"] = datetime.strptime(transaction['datetime'],"%Y-%m-%d %H:%M:%S")
        
        return transactions
