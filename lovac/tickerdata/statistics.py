''' Statistical helper-module to calculate mean, rolling_mean, bollinger-bands and the like '''

import pandas as pd

# lower level functions

def calc_rolling_mean(values, window):
    """Return rolling mean of given values, using specified window size."""
    return values.rolling(window=window).mean()


def calc_rolling_std(values, window):
    """Return rolling standard deviation of given values, using specified window size."""
    return values.rolling(window=window).std()

def get_bollinger_bands(rolling_mean, std_dev, k=2):
    """Return upper and lower Bollinger Bands."""
    # TODO: Compute upper_band and lower_band
    upper_band = rolling_mean + k*std_dev
    lower_band = rolling_mean - k*std_dev
    return upper_band, lower_band

def insert_bollinger(df, window=120):
    ''' inserts a rolling_mean and a upper and lower bolinger-band in the dataframe'''
    ma20 = calc_rolling_mean(df['weightedprice'],window=window)
    rstd = calc_rolling_std(df['weightedprice'],window=window)
    upper_band, lower_band = get_bollinger_bands(ma20, rstd)
    df['ma20']=ma20
    df['lower_band'] = lower_band
    df['upper_band'] = upper_band
    return df