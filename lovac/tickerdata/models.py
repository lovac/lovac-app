''' the lovac.tickerdata models '''
import datetime
import lovac
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from sqlalchemy.orm import relationship

from lovac.models import db

class TickerHourly(db.Model):
    ''' Lovac's Ticker-Model '''
    id      = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    symbol  = db.Column(db.String(10), unique=False, nullable=False)
    exchange = db.Column(db.String(20), nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)
    open    = db.Column(db.Float, nullable=False)
    close   = db.Column(db.Float, nullable=False)
    high    = db.Column(db.Float, nullable=False)
    low     = db.Column(db.Float, nullable=False)
    last    = db.Column(db.Float, nullable=False)
    weightedprice = db.Column(db.Float, nullable=False)
    
    def __init__(self,symbol, exchange, timestamp, open, close, high, low, last, weightedprice):
        self.symbol = symbol
        self.exchange = exchange
        self.timestamp = timestamp
        self.open = open
        self.close = close
        self.high = high
        self.low = low
        self.last = last
        self.weightedprice = weightedprice

    def __repr__(self):
        return '<tickerHourly {}>'.format(self.symbol)