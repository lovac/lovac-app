''' a module for tickerdata management. Statistic-stuff, downloading and db-import '''
import datetime
import os
import requests
import json
import pandas as pd
import lovac

from flask import current_app as app
from lovac.tickerdata import statistics
from lovac.tickerdata.models import TickerHourly
from lovac.models import db
from pytz import timezone

DEFAULT_MA=2

def get_json_from_bcc(number_of_days=1,symbol="btcusd"):
    ''' something like
    [[1517616000, 8838.29, 8864.3, 8679.58, 8771.91, 460.33477674, 4042688.312322012, 8782.061483495843],...]
    which is:
    [ timestamp, open, high, low, close, Volume(btc), Volume(currency), weighted-price] '''
    
#https://bitcoincharts.com/charts/chart.json?m=bitstampUSD&SubmitButton=Draw&r=30&i=Hourly&c=0&s=&e=&Prev=&Next=&t=S&b=&a1=&m1=10&a2=&m2=25&x=0&i1=&i2=&i3=&i4=&v=1&cv=0&ps=0&l=0&p=0&
#https://bitcoincharts.com/charts/chart.json?m=bitstampUSD&SubmitButton=Draw&r=10&i=Hourly&c=0&s=&e=&Prev=&Next=&t=S&b=&a1=&m1=10&a2=&m2=25&x=0&i1=&i2=&i3=&i4=&v=1&cv=0&ps=0&l=0&p=0&
    bitstamp_symbol = symbol[-3:].upper() # should result in "USD" or "EUR"
    if number_of_days > 200:
        raise "not more then 200 days is possible"
    url="https://bitcoincharts.com/charts/chart.json?m=bitstamp{}&SubmitButton=Draw&r={}&i=Hourly&c=0&s=&e=&Prev=&Next=&t=S&b=&a1=&m1=10&a2=&m2=25&x=0&i1=&i2=&i3=&i4=&v=1&cv=0&ps=0&l=0&p=0&".format(bitstamp_symbol,number_of_days)
    response = requests.get(url)
    return response.text

def bcc_to_dataframe(number_of_days=1,symbol="btcusd"):
    ''' bitcoincharts (bcc) to pandas-dataframe '''
    data=json.loads(get_json_from_bcc(number_of_days,symbol))
    dataframe=pd.DataFrame(data)
    dataframe.columns = ['timestamp', 'open', 'high', 'low', 'close', 'volumebtc', 'volumefiat','weightedprice']
    #df['timestamp'] = pd.to_datetime(df['timestamp'], unit='s')
    dataframe['symbol']="btc"
    dataframe['exchange']="bitstamp"
    dataframe['last']=dataframe['close']
    app.logger.debug(dataframe)
    return dataframe

import click
@app.cli.command('bcc2csv')
@click.option('--days', default=7, help='Number of days to get data from Bitcoincharts')
def bcc_to_csv(days=7):
    ''' bitcoincharts (bcc) to csv-file 
        see https://bitcoincharts.com/about/markets-api/
    '''
    dataframe=bcc_to_dataframe(days)
    dataframe.to_csv(os.path.join(app.root_path, '../instance/td-temp.csv'))

@app.cli.command('csv2db')
def csv_2_db_cli(filename="td-temp.csv",symbol="btcusd"):
    ''' importing a csv-file to the DB - CLI-command'''
    app.logger.info("Importing csv to database")
    csv_2_db(filename,symbol)
    app.logger.info("finished")

def csv_2_df(filename="td-temp.csv"):
    ''' importing a csv-file to a dataframe '''
    df=pd.read_csv(os.path.join(app.root_path, '../instance/' + filename))
    ma_series=statistics.calc_rolling_mean(df['weightedprice'], window=DEFAULT_MA)
    df["ma"]=ma_series
    return df

def csv_2_db(filename="td-temp.csv",symbol="btcusd"):
    ''' importing a csv-file to the DB '''
    df=csv_2_df(filename)
    df_to_db(df,symbol)

def df_to_db(df,symbol="btcusd"):
    ''' importing a dataframe to DB '''
    app.logger.info("About to save {} rows in db".format(df.shape[0]))
    for index, row in df.iterrows():
        
        date = datetime.datetime.fromtimestamp(row['timestamp'], tz=timezone('Europe/Berlin'))
        ticker_hourly = TickerHourly(symbol, 'bitstamp', date,
        row['open'],row['close'],row['high'],row['low'],row['last'],row['weightedprice'])

        db.session.add(ticker_hourly)
    db.session.commit()
