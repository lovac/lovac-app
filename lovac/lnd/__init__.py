''' A service-module for the lightning network '''

import os
import platform
import codecs
import grpc
from grpc import RpcError

from flask import current_app as app
from lovac.util import handle_exception
from lovac.lnd import rpc_pb2 as ln, rpc_pb2_grpc as lnrpc
from lovac.models import LovacError


class Lnd(object):
    ''' a class similiar to a Flask-extension but with less overhead
        Simply initialize via passing in the Flask-App '''
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)
            app.ln = self

    def init_app(self, app):
        os.environ.setdefault("GRPC_SSL_CIPHER_SUITES", "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256")
        if not app.config['LND_PATH']:
            if platform.system() == "Darwin":
                LND_PATH = os.path.join(os.getenv('HOME'), 'Library/Application Support/Lnd')
            elif platform.system() == "Linux":
                LND_PATH = os.path.join(os.getenv('HOME'), '.lnd')
            elif platform.system() == "Windows":
                LND_PATH = os.path.join(os.getenv('APPDATA'), 'Local', 'Lnd') 
        else:
            LND_PATH = app.config['LND_PATH']
        app.logger.info("Reading LND specific config files from " + LND_PATH)
        self.creds = grpc.ssl_channel_credentials(open(os.path.join(LND_PATH, 'tls.cert'), 'rb').read())
        if app.config['LND_RPCHOST'] and app.config['LND_RPCHOST'] != "":
            self.LND_RPCHOST = app.config['LND_RPCHOST']
        else:
            self.LND_RPCHOST = "localhost:10009"
        with open(os.path.join(LND_PATH, 'admin.macaroon'), 'rb') as f:
            macaroon_bytes = f.read()
            self.macaroon = codecs.encode(macaroon_bytes, 'hex')
        if app.config['LND_RPC_TIMEOUT']:
            self.LND_RPC_TIMEOUT=app.config['LND_RPC_TIMEOUT']
        else:
            self.LND_RPC_TIMEOUT=10
      
    def stub(self):
        app.logger.debug("Creating grpc stub with "+self.LND_RPCHOST)
        channel = grpc.secure_channel(self.LND_RPCHOST, self.creds)
        stub = lnrpc.LightningStub(channel)
        
        return stub

    def macaroon(self):
        return macaroon
    
    def generate_invoice(self,user, value):
        ''' low level function to create an invoice '''
        try:
            app.logger.info("Callin Invoice with timeout " + str(self.LND_RPC_TIMEOUT))
            add_invoice_resp = self.stub().AddInvoice(ln.Invoice(value=value, memo="User '{}' ".format(user.email)), timeout=self.LND_RPC_TIMEOUT, metadata=[('macaroon', self.macaroon)])
            return add_invoice_resp
        except RpcError as e:
            raise LovacError("PROBABLY (FIXME) Timeout calling GRPC",e)
        except Exception as exception:
            raise LovacError("PROBABLY (FIXME) Timeout calling GRPC",e)
    
    def check_payment(self,r_hash_bytes):
        invoice_resp = app.ln.stub().LookupInvoice(ln.PaymentHash(r_hash=r_hash_bytes),metadata=[('macaroon', self.macaroon)])
        return invoice_resp

