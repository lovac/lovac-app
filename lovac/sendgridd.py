# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
import sendgrid
import os
from sendgrid.helpers.mail import *

import email_validator

from flask import current_app as app
from flask import render_template



def sendmail(recipient, subject, mailtemplate="plain", **context):
    ''' Sends a mail via sendgrid '''
    email_validator.validate_email(recipient)
    api_key = app.config['SENDGRID_API_KEY']
    sg = sendgrid.SendGridAPIClient(apikey=api_key)
    data = _build_mail(recipient, subject, mailtemplate, **context)

    if not app.config["SEND_MAILS"]:
        app.logger.info("sendgrid: Not sending any mail because SEND_MAILS=False")
        return
    response = sg.client.mail.send.post(request_body=data)
    app.logger.info("AUDIT: sent mail to {} with result {}".format(recipient,response))

def _build_mail(recipient, subject, mailtemplate="plain", **context):
    """All settings set"""
    mail = Mail()

    mail.from_email = Email('admin@lovac.xyz')
    
    personalization = Personalization()
    personalization.add_to(Email(recipient))
    mail.add_personalization(personalization)

    mail.subject = "[Lovac] "+subject

    mail.add_content(Content("text/plain", render_template("mail/{}.txt".format(mailtemplate), **context)))
    mail.add_content(Content("text/html", render_template("mail/{}.html".format(mailtemplate), **context)))

    #mail.template_id = "13b8f94f-bcae-4ec6-b752-70d6cb59f932"

    return mail.get()