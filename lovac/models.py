''' the lovac models '''
import datetime
import pytz
import codecs
import grpc
from grpc import RpcError

from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from sqlalchemy.orm import relationship
from sqlalchemy_utils.types.choice import ChoiceType
from flask import current_app as app

import lovac
from lovac.util import handle_exception

db = SQLAlchemy()

class InvitationCode(db.Model):
    ''' At Register time, the user needs to provide an InvitationCode '''
    __tablename__ = 'invitationcode'
    code = db.Column(db.String(20), unique=True, primary_key=True, nullable=False)
    max_users = db.Column(db.Integer, nullable=False)
    comment = db.Column(db.String(80))
    users = relationship("User", uselist=True, back_populates="invitationcode")

    def __init__(self, code, max_users, comment):
        self.code  = code
        self.max_users = max_users
        self.comment = comment

    def __repr__(self):
        return '<invitationCode {} {} / {}>'.format(self.code, self.max_users, self.users.count)
    
    def exhausted(self):
        ''' returns true is the invitation-code is already fully used '''
        return self.max_users <= len(self.users)
    
    def used(self):
        ''' returns true is the invitation-code is used at least once '''
        return len(self.users) > 0


def get_users():
    ''' returns all users from the DB '''
    return db.session.query(User)

class AnonymousUser():
    ''' I don't want a db-entry for a the Anonymous User but it's just so usefull to
    have an object which behaves like a user to a certain extent. '''
    def is_authenticated(self):
        return False
    bitstampconfig = False


class User(db.Model):
    ''' Lovac's User-Model '''
    #__tablename__ = 'user'
    ''' Stores the current status in the autostatus field
        10 = Bought DCA the other day
        11 = Bought the dip the other day
        15 = Had an error last bought 
        16 = does not have a bitstamp-config
        17 = automatic_buy_flag was False
    '''
    autostatus = db.Column(db.Integer, nullable=True)
    email = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)
    password = db.Column(db.String(120), unique=False, nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    confirmed_on = db.Column(db.DateTime, nullable=True)
    satoshis = db.Column(db.Integer)
    last_login = db.Column(db.DateTime)
    last_purchase = db.Column(db.DateTime)
    # relations
    notifications = relationship("Notification", uselist=True, back_populates="user")
    bitstampconfig = relationship("Bitstampconfig", uselist=False, back_populates="user")
    invitationcode_code = db.Column(db.String, db.ForeignKey('invitationcode.code'))
    invitationcode = relationship("InvitationCode", back_populates="users")
    payments = relationship("Payment", uselist=True, back_populates="user")
    
    def __init__(self, email, password, invitationcode_code=None, admin=False, confirmed=False): 
        self.email = email
        self.set_password(password)
        self.invitationcode_code = invitationcode_code
        self.registered_on = datetime.datetime.now()
        self.last_login = datetime.datetime.now()
        self.confirmed = confirmed
        self.admin = admin
    
    def set_password(self, password):
        bcrypt = Bcrypt(app)
        self.password = str(bcrypt.generate_password_hash(password),"utf-8")

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.email

    def __repr__(self):
        return '<email {}>'.format(self.email)

class Notification(db.Model):
    ''' An item storing a Notification per mail (or other channles) sent to a user. 
        Mainly used in order to implement a reasonable Mailingstrategy 
        sentpurpose is one of:
        * please_confirm_email
        * please_configure_bitstampcredentials
        * please_configure_accumplan
        * please_send_fiat
        * please_withdraw_bitcoin
    '''
    __tablename__ = 'notifications'
    id          = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    sent_at     = db.Column(db.DateTime, nullable=False)
    user        = relationship("User", back_populates="notifications")
    user_id     = db.Column(db.String(100), db.ForeignKey('user.email'))
    SENTPURPOSE_CHOICES = (
        (u'please_confirm_email', u'Please confirm your Mail'),
        (u'please_configure_bitstampcredentials', u'Please configure your bitstampcredentials'),
        (u'please_configure_accumplan', u'Please configure your Accumulation-plan'),
        (u'please_send_fiat', u'Please send fiat to your exchange'),
        (u'please_withdraw_bitcoin', u'Please withdraw bitcoin from your exchange'),
        (u'generic', u'a generic mail'),
        (u'broadcast', u'a broadcast mail not only for this user'),
    )
    sentpurpose = db.Column(ChoiceType(SENTPURPOSE_CHOICES))
    # if the user did what pleas_* asked him, this should be set to True
    # Should be used to calculate mailfrequency for intentional mails
    resolved    = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, user, sentpurpose, sent_at=None, resolved=None):
        ''' initializes a Notification '''
        if resolved != None:
            self.resolved = resolved
        self.user = user
        self.sentpurpose = sentpurpose
        if sent_at == None:
            self.sent_at = datetime.datetime.now()
        else:
            self.sent_at = sent_at

    def resolve(self):
        pass

    def __repr__(self):
        return '<notification {} {} {}>'.format(self.user.email,self.sent_at,self.sentpurpose)

class Bitstampconfig(db.Model):
    ''' Lovac's Bitstampconfig-Model, each User should have one! '''
    STRATEGY_CHOICES = (
        (u'dollar_cost_averaging', u'Dollar Cost Averaging'),
        (u'buy_the_dip', u'Buy the dip')
    )
    __tablename__ = 'bitstampconfig'
    id      = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    user_id = db.Column(db.String(80), db.ForeignKey('user.email'), nullable=False)
    username = db.Column(db.String(20), unique=False, nullable=False)
    key = db.Column(db.String(50), unique=True, nullable=False)
    secret = db.Column(db.String(50), unique=False, nullable=False)
    # The market / currency to use. Currrently only "usd"
    currency = db.Column(db.String(10), unique=False, nullable=False)
    ' The amount to buy daily in Dollar - minimum $6'
    dailybuy = db.Column(db.Integer, nullable=False)
    strategy = db.Column(ChoiceType(STRATEGY_CHOICES), default='dollar_cost_averaging')
    automatic_buy_flag = db.Column(db.Boolean, nullable=False, default=True)
    ' number of days we are delaying the buy '
    delay_buy_counter = db.Column(db.Integer, nullable=False)
    user = relationship("User", back_populates="bitstampconfig")
    purchases = relationship("Purchase", uselist=True, back_populates="bitstampconfig")
    btcaddresses = relationship("BtcAddress", uselist=True, back_populates="bitstampconfig")
    
    def __init__(self, user_id, username, key, secret, currency, automatic_buy_flag=True):
        self.user_id = user_id
        self.username = username
        self.key = key
        self.secret = secret
        self.dailybuy = 6
        self.delay_buy_counter = 0
        self.currency = currency
        self.strategy = u'dollar_cost_averaging'
        self.automatic_buy_flag = automatic_buy_flag

    def to_json(self):
        settings={}
        settings["BITSTAMP_USERNAME"]=self.username
        settings["BITSTAMP_KEY"]=self.key
        settings["BITSTAMP_SECRET"]=self.secret
        settings["CURRENCY"]=self.currency
        return settings

    def __repr__(self):
        return '<bitstampconfig {} {} {}>'.format(self.username,self.key,self.currency)

class Purchase(db.Model):
    ''' a Purchase from a Bitstampconfig '''
    __tablename__ = 'purchase'
    id      = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime, nullable=False)
    currency = db.Column(db.String(10), unique=False, nullable=False)
    fiat = db.Column(db.Float, nullable=False)
    btc = db.Column(db.Float, nullable=False)
    id      = db.Column(db.Integer, primary_key=True)
    lfee = db.Column(db.Integer) # in the lovac-fee in satoshi, probably 0.25% of btc
    # The rate - costs of 1 btc in usd
    btc_fiat = db.Column(db.Float)
    bitstampconfig = relationship("Bitstampconfig", back_populates="purchases")
    bitstampconfig_id = db.Column(db.Integer, db.ForeignKey('bitstampconfig.id'))

    def __init__(self, bitstampconfig_id, datetime, fiat, btc, btc_fiat, lfee, currency="usd"):
        self.bitstampconfig_id = bitstampconfig_id
        self.datetime = datetime
        self.currency = currency
        self.fiat = fiat
        self.btc = btc
        self.btc_fiat = btc_fiat
        self.lfee = lfee
    
    def __repr__(self):
        return '<purchase {} {} {} {} {} {}>'.format(self.id, self.datetime.date(), self.currency, self.fiat, self.btc, self.bitstampconfig)

class BtcAddress(db.Model):
    ''' A BtcAddress which should count when calculating how much btc a accumPlan accumulated '''
    __tablename__ = 'btcaddress'
    id      = db.Column(db.Integer, primary_key=True)
    # last update
    syncdatetime = db.Column(db.DateTime, nullable=False)
    btcaddress = db.Column(db.String(50), unique=True, nullable=False)
    # number of satoshis on that address (cached)
    sat = db.Column(db.Integer)
    bitstampconfig = relationship("Bitstampconfig", back_populates="btcaddresses")
    bitstampconfig_id = db.Column(db.Integer, db.ForeignKey('bitstampconfig.id'))

    def __init__(self, bitstampconfig_id, syncdatetime, btcaddress, sat):
        self.bitstampconfig_id  = bitstampconfig_id
        self.syncdatetime       = syncdatetime
        self.btcaddress         = btcaddress
        self.sat                = sat


    def __repr__(self):
        return '<btcAddress {} {} {} {} {}>'.format(self.id, self.btcaddress)

class Payment(db.Model):
    ''' A payment coming in via Lightning. It goes through these stati:
        * Pending_invoice (No contact yet with Lightning)
        * pending_payment (invoice created via Lightning)
        * complete (paid via lightning)
        * error (no idea but probably not paid, also used to cancel)
    '''
    PAYMENT_STATUS_CHOICES = (
        (u'pending_invoice', u'Pending Invoice'), # Should be atomic
        (u'pending_payment', u'Pending Payment'),
        (u'complete', u'Complete'),
        (u'error', u'Error'),
    )

    PAYMENT_PURPOSE_CHOICES = (
        ('view', 'View'),
        ('upvote', 'Upvote')
    )
    id              = db.Column(db.Integer, primary_key=True)
    chainid = db.Column(db.String(10), unique=False, nullable=False)
    satoshi_amount  = db.Column(db.Integer)
    r_hash          = db.Column(db.String(64))
    payment_request = db.Column(db.String(1000),unique=True)

    status          = db.Column(ChoiceType(PAYMENT_STATUS_CHOICES), default='pending_invoice')
    created_at      = db.Column(db.DateTime(timezone=True), nullable=False)
    modified_at     = db.Column(db.DateTime(timezone=True), nullable=False)
    user = relationship("User", back_populates="payments")
    user_id = db.Column(db.String(100), db.ForeignKey('user.email'))
    
    def __init__(self, user, amount=1000):
        ''' Constructor '''
        self.chainid = app.config["LND_CHAINID"]
        self.user = user
        self.created_at = lovac.util.lovac_now()
        self.modified_at = self.created_at
        self.satoshi_amount = int(amount)
        self.status = 'pending_invoice'

    def generate_invoice(self):
        """
        Generates a new invoice
        """
        assert self.status == 'pending_invoice', "Already generated invoice"
        try:
            add_invoice_resp = app.ln.generate_invoice(self.user, self.satoshi_amount)
            r_hash_base64 = codecs.encode(add_invoice_resp.r_hash, 'base64')
        except Exception as e:
            raise LovacError("Error while generating invoice",e)
        self.r_hash = r_hash_base64.decode('utf-8')
        self.payment_request = add_invoice_resp.payment_request
        self.status = 'pending_payment'
        self.modified_at = lovac.util.lovac_now()
        db.session.add(self)
        db.session.commit()

    def has_expired(self):
        ''' returns true if older then an hour '''
        modified_at = self.modified_at.replace(tzinfo=pytz.utc)
        one_hour_ago = datetime.datetime.now(tz=pytz.utc) - datetime.timedelta(seconds=3600)
        print("modified_at " + str(modified_at))
        print("one_hour_ago "+ str(one_hour_ago))
        return modified_at < one_hour_ago

    def invalidate(self):
        if not self.check_payment():
            self.status = 'error'
            db.session.add(self)
            db.session.commit()
        else:
            # ToDo : raise a LovacError
            self.status = 'error'
            db.session.add(self)
            db.session.commit()

    def check_payment(self):
        """
        Checks if the Lightning payment has been received for this invoice
        """
        if self.status == 'pending_invoice':
            return False

        r_hash_base64 = self.r_hash.encode('utf-8')
        r_hash_bytes = codecs.decode(r_hash_base64, 'base64')
        try:
            invoice_resp = app.ln.check_payment(r_hash_bytes)
        except RpcError:
            self.status = 'error'
            db.session.add(self)
            db.session.commit()
            return False
        if invoice_resp.settled:
            # Payment complete
            self.status = 'complete'
            db.session.add(self)
            db.session.commit()
            return True
        else:
            # Payment not received
            return False


class LovacError(Exception):
    ''' The LovacError custom '''
    pass