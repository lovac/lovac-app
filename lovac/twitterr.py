import twitter
import os


def send_tweet(tweet):
    api_key = os.environ['TW_API_KEY']
    api_key_secret = os.environ['TW_API_KEY_SECRET']
    access_token = os.environ['TW_ACCESS_TOKEN']
    access_token_secret = os.environ['TW_ACCESS_TOKEN_SECRET']
    api = twitter.Api(consumer_key=api_key,
                    consumer_secret=api_key_secret,
                    access_token_key=access_token,
                    access_token_secret=access_token_secret)
    status = api.PostUpdate(tweet)


