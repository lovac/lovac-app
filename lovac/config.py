''' A config module contains static configuration '''
import os
import datetime
try:
    # Python 2.7
    import ConfigParser as configparser
except ImportError:
    # Python 3
    import configparser

# BASEDIR = os.path.abspath(os.path.dirname(__file__))


def _get_bool_env_var(varname, default=None):

    value = os.environ.get(varname, default)

    if value is None:
        return False
    elif isinstance(value, str) and value.lower() == 'false':
        return False
    elif bool(value) is False:
        return False
    else:
        return bool(value)

class BaseConfig(object):
    """Base configuration."""
    DATABASE='DbDefault.sqlite'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    SECRET_KEY='development key'
    USERNAME='admin'

    # Stuff here comes from http://flask-jwt-extended.readthedocs.io/en/latest/tokens_in_cookies.html
    # Only allow JWT cookies to be sent over https. In production, this
    # should likely be True
    JWT_COOKIE_SECURE=False
    # Configure application to store JWTs in cookies or headers. Whenever you make
    # a request to a protected endpoint, you will need to send in the
    # access or refresh JWT via a cookie.
    JWT_TOKEN_LOCATION=['cookies','headers']
    # Set the cookie paths, so that you are only sending your access token
    # cookie to the access endpoints, and only sending your refresh token
    # to the refresh endpoint. Technically this is optional, but it is in
    # your best interest to not send additional cookies in the request if
    # they aren't needed.
    #JWT_ACCESS_COOKIE_PATH='/api/' # that's not true, we're using it as well at /admin
    JWT_REFRESH_COOKIE_PATH='/api/v1alpha/refresh_token'
    # Enable csrf double submit protection. See this for a thorough
    # explanation: http://www.redotheweb.com/2015/11/09/api-security.html
    JWT_COOKIE_CSRF_PROTECT=False
    JWT_CSRF_IN_COOKIES=True
    # Set the secret key to sign the JWTs with
    JWT_SECRET_KEY='dslwx0d6wd5wsmc3nq4pdtw'
    # 3 days until the JWT Token expires
    JWT_ACCESS_TOKEN_EXPIRES=datetime.timedelta(hours=72)

    # We don't need to set it here, just to prevent a KeyError
    # default is platform-dependent and for Linux it's ~/.lnd
    LND_PATH=""
    # We don't need to set it here, just to prevent a KeyError
    # default is localhost:10009
    LND_RPCHOST=""
    LND_RPC_TIMEOUT=10
    # Being able to switch dynamically between "mainnet" and "testnet3"
    LND_CHAINID=os.environ.get('LND_CHAINID',  default='mainnet')

    # Mailjet-configuration needs to come from env-vars!
    MJ_APIKEY_PUBLIC  = os.environ.get('MJ_APIKEY_PUBLIC',  default='not-set')
    MJ_APIKEY_PRIVATE = os.environ.get('MJ_APIKEY_PRIVATE', default='not-set')

    # sendgrid-configuration needs to come from env-vars!
    SENDGRID_API_KEY = os.environ.get('SENDGRID_API_KEY', default='not-set')

    # needed to create the token for the email-validation
    SECURITY_PASSWORD_SALT = 'whatever_some_value_246'
    SECRET_KEY = 'yet_another_secret_value_26843'
    # Security turned on by default
    WTF_CSRF_ENABLED=True
    # suppress mailsending e.g. in handle_exception
    SEND_MAILS = _get_bool_env_var('SEND_MAILS',False) 
    # Suppressable buying via cronjob
    ENABLE_LATE_BUYER_JOB = _get_bool_env_var('ENABLE_LATE_BUYER_JOB',True)
    # Enable url_for in non-request-contexts (send mail via cron)
    # This enforces that the dev is setting localhost.localdomain to 127.0.0.1 in /etc/hosts
    SERVER_NAME = os.environ.get('SERVER_NAME', default='localhost.localdomain:5000')

    # See https://github.com/lightningnetwork/lnd/issues/795
    os.environ['GRPC_SSL_CIPHER_SUITES'] = '' + \
        '' + \
        'ECDHE-RSA-AES128-GCM-SHA256:' + \
        'ECDHE-RSA-AES128-SHA256:' + \
        'ECDHE-RSA-AES256-SHA384:' + \
        'ECDHE-RSA-AES256-GCM-SHA384:' + \
        'ECDHE-ECDSA-AES128-GCM-SHA256:' + \
        'ECDHE-ECDSA-AES128-SHA256:' + \
        'ECDHE-ECDSA-AES256-SHA384:' + \
        'ECDHE-ECDSA-AES256-GCM-SHA384'

class DevelopmentConfig(BaseConfig):
    """Development configuration."""
    DB_FLAVOR = 'sqlite'
    DEBUG = True
    DATABASE='dbDev.sqlite'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE
    # set these to false if you do LND_Development '''
    LND_DEACTIVATE = True
    LND_IGNORE_ISSUES = _get_bool_env_var('LND_IGNORE_ISSUES',True) 
    # Don't want to buy unintentianally on dev-system
    ENABLE_LATE_BUYER_JOB = _get_bool_env_var('SEND_MAILS',False) 

class TestConfig(BaseConfig):
    """Testing configuration."""
    # in Memory for the win
    DB_FLAVOR = 'sqlite'
    # doesn't work unfortunately
    #DATABASE=':memory:'
    #SQLALCHEMY_DATABASE_URI = 'sqlite://'
    DATABASE='dbTest.sqlite'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE
    
    LOGIN_DISABLED=False
    PRESERVE_CONTEXT_ON_EXCEPTION = False # https://github.com/jarus/flask-testing/issues/21
    TESTING = True
    DEBUG = True
    # when testing, we won't have an LND-server. That's mitigated by a timeout and test should
    # pass nevertheless. The file in below directory are fake-files anyway
    LND_RPCHOST = "localhost:12345"
    LND_PATH = "./tests/lnddata"
    LND_RPC_TIMEOUT=1
    # I guess we'll mainly have timeouts when we run tests, so Payments should have "fakenet" as "chainid"
    LND_CHAINID="fakenet"
    # This will only create a brief warning in the log in case of issues
    LND_IGNORE_ISSUES = True
    # This will make sure that no invoices are created which probably somehow have a bug causing:
    # sqlalchemy.exc.OperationalError: (sqlite3.OperationalError) database is locked
    LND_DEACTIVATE = True
    # that security-crap makes testing much more complicated
    WTF_CSRF_ENABLED=False

class CockroachBasedConfig(BaseConfig):
    """Development configuration with cockroachUsage."""
    DB_FLAVOR = 'cockroach'
    DEBUG = True
    USERNAME = os.environ.get('DB_USER', default='lovac')
    #PASSWORD = os.environ.get('DB_PASSWORD') no passwords in insecure setups
    DB_HOST = os.environ.get('DB_HOST', default='127.0.0.1') # will be overridden in docker-compose, but good for dev
    DB_PORT = os.environ.get('DB_PORT', default='26257')
    DATABASE = os.environ.get('DATABASE', default='lovac')
    SQL_ALCHEMY_TRACK_MODIFICATIONS = False
    #SQLALCHEMY_DATABASE_URI = 'cockroachdb://lovac@localhost:26257/lovac'
    SQLALCHEMY_DATABASE_URI = 'cockroachdb://{}@{}:{}/{}'.format(USERNAME, DB_HOST, DB_PORT, DATABASE)

# Just a reminder by Kim which might be important for Prod (but might even currently break the App)
class ProductionConfig(CockroachBasedConfig):
    ''' If you want to run production, you probably want these settings '''
    # Only allow JWT cookies to be sent over https. In production, this
    # should likely be True
    JWT_COOKIE_SECURE=True
    # Enable csrf double submit protection. See this for a thorough
    # explanation: http://www.redotheweb.com/2015/11/09/api-security.html
    ## desctivated, see https://gitlab.com/lovac/lovac-app/issues/15
    JWT_COOKIE_CSRF_PROTECT=False
    DEBUG = False
    LND_PATH = "./data/lnd"
    # The docker-hots's IP might be this one, so let's default to it as we're using on docker
    LND_RPCHOST = os.environ.get('LND_RPCHOST', default='172.17.0.1:10009')
    SEND_MAILS = _get_bool_env_var('SEND_MAILS',True) # True by default in prod
    SERVER_NAME = os.environ.get('SERVER_NAME') # no default in production! This is #mandatory
    

