''' services for tickerdata '''

from datetime import datetime, date, timedelta

from flask import current_app as app
from sqlalchemy import and_
import pandas as pd

import lovac # .util import lovac_now to make it testable
from lovac.models import db, LovacError
from lovac.tickerdata import TickerHourly

def get_current_tickerdata(date=None):
    ''' returns current lower_bollinger_band and weighted_price '''
    if date is None:
        date = lovac.util.lovac_now()
    app.logger.debug("TS: get_current_tickerdata  date iis {}".format(date))
    td = get_tickerdata(from_date=date - timedelta(hours=3), to_date=date)
    app.logger.debug("get_current_tickerdata  date is {} and price {}".format(td.iloc[-1]["timestamp"],td.iloc[-1]["weightedprice"]))
    return td.iloc[-1]["weightedprice"] , td.iloc[-1]["lower_band"]

def get_tickerdata(from_date=datetime(2009, 1, 3,0,0,0),to_date=None, symbol="btcusd"):
    ''' user for the controller to get tickerdata
        returns a pandas dataframe with the (at least) the rows:
        * timestamp
        * weightedprice
        * ma20
        * lower_band
        * upper_band
        
    '''
    if to_date is None:
        to_date=lovac.util.lovac_now()+ timedelta(hours=24)

    #          order by timestamp asc"""
    
    # in order to properly caclulate the ma, we need 120 hours more then we show later
    from_date = from_date - timedelta(hours=120)
    query = db.session.query(TickerHourly).filter(
        and_(TickerHourly.timestamp >= from_date, TickerHourly.timestamp <= to_date, TickerHourly.symbol == symbol )).order_by(TickerHourly.timestamp).statement
    app.logger.debug("query in get_tickerdata {}".format(query))
    app.logger.debug("from_date={} to_date={}".format(from_date,to_date))
    df = pd.read_sql(query, db.session.connection() )
    if df.empty:
        return df # avoiding all sorts of headache
    app.logger.debug("get_tickerdata with these timestamps: {} until {}".format(df.iloc[0]["timestamp"],df.iloc[-1]["timestamp"]))
    df['timestamp'] = pd.to_datetime(df['timestamp'],unit='s',utc=True)
    df['last']=df['weightedprice'] # let's consider to plot last in js
    # which rolling-mean do we want to use?

    from lovac.tickerdata.statistics import insert_bollinger
    df=insert_bollinger(df)
    # just to be sure, fillNAs
    df.fillna(method="ffill",inplace=True)
    df.fillna(method="bfill",inplace=True)
    # remove the incomplete slice, this assumes that the data is always there 
    # (deutsch: "stetig") which might not be the case
    app.logger.debug("get_current_tickerdata  before slicing {} and price {}".format(df.iloc[-1]["timestamp"],df.iloc[-1]["weightedprice"]))
    df=df[120:]
    app.logger.debug("get_current_tickerdata  after slcing {} and price {}".format(df.iloc[-1]["timestamp"],df.iloc[-1]["weightedprice"]))
    app.logger.debug("get_tickerdata before return with these timestamps: {} until {}".format(df.iloc[0]["timestamp"],df.iloc[-1]["timestamp"]))
    return df

def is_price_in_freefall(td=None, date=None):
    ''' Is the current price below the lower bollinger_band ? '''
    if td is None:
        td = get_tickerdata()
    if date is None:
        date = lovac.util.lovac_now()
    i = _find_index_for_date(td,date)
    app.logger.debug("freefall? Checking for freefall with i={} which is {}".format(i,td.iloc[i]["timestamp"]))
    app.logger.debug("freefall? is {} < {} ? ".format( td.iloc[i]["weightedprice"] , td.iloc[i]["lower_band"]))
    if td.iloc[i]["weightedprice"] <  td.iloc[i]["lower_band"] :
        app.logger.debug("price is in FREEFALL at {}".format(date))
        return True;
    return False

def is_price_recovering(td=None, date=None):
    ''' Is the current price above the lower bollinger_band again? '''
    if td is None:
        td = get_tickerdata()
    if date is None:
        date = lovac.util.lovac_now()
    if not is_price_in_freefall(td, date - timedelta(minutes=59)) :
        return False;
    app.logger.debug("recovery? an hour before, the price was in freefall!")
    
    i = _find_index_for_date(td,date)
    app.logger.debug("recovery base for calculation is {} which should be ".format(i,td.iloc[i]["timestamp"]))
    # That means that the hour before the weightedprice was in fact below the lower_band
    # so now:
    if td.iloc[i]["weightedprice"] > td.iloc[i]["lower_band"]: # it's above again?
        app.logger.debug("RECOVERY! The price {} recovered above the lower_band {}".format(td.iloc[i]["weightedprice"],td.iloc[i]["lower_band"]) )
        return True
    else:
        return False
    

def _find_index_for_date(df=None, date=None):
    ''' returns an index for the df which corresponds to the date '''
    if df is None:
        df = get_tickerdata()
    if date is None:
        date = lovac.util.lovac_now()
    # ToDo: we should iterate from the other side
    for i in range(1,len(df)):
        if (i > 389 and i < 393) :
            app.logger.debug("comparing {} (that's {}) with the date {}".format(df.iloc[i]["timestamp"],i,date)) 
        # 59 minutes and 59 seconds after that timestamp, we're still returning that index
        if df.iloc[i]["timestamp"] >=  (date - timedelta(minutes=59,seconds=59)):
            app.logger.debug("founf i={} with date = {} for the date {}".format(i,df.iloc[i]["timestamp"],date ))
            return i
    raise LovacError("Couldn't find index in dataframe corresponding to date {} last timestamp is {}".format(date, df.iloc[-1]["timestamp"]))
