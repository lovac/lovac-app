''' encapsulate startegy better then services '''

from datetime import timedelta

import lovac
from lovac.util import first_day_of_month
from flask import current_app as app

class AccumulationStrategy(object):
    ''' Helps to decide when and how muchg to buy '''
    def __init__(self, purchases, dailybuy, strategy, automatic_buy_flag, delay_buy_counter):
        self.purchases = purchases
        self.dailybuy = dailybuy
        self.strategy = strategy
        self.automatic_buy_flag = automatic_buy_flag
        self.delay_buy_counter = delay_buy_counter

    @classmethod
    def from_user(cls, user):
        return cls(
            user.bitstampconfig.purchases,
            user.bitstampconfig.dailybuy,
            user.bitstampconfig.strategy,
            user.bitstampconfig.automatic_buy_flag,
            user.bitstampconfig.delay_buy_counter
        )

    def calculate_purchase_amount(self, on_date=None):
        ''' 
        calculates the amount a user can purchase based on what he already purchased and his
        accumulation plan
        In the first month there are some special rules because we don't want to catch up
        the non-purchases from the beginning of the month
        '''
        if on_date == None:
            on_date=lovac.util.lovac_now().date()
        summary = self._summary_purchases(first_day_of_month(on_date),on_date)
        app.logger.debug("PA: user has purchased a fiat_sum of {} (based on {})".format(str(summary["fiat_sum"]),str(on_date)))
        if not self.purchases :
            # if you haven't purchased AT ALL so far, you should start with your daily amount
            return max(self.dailybuy,25) # even if you have configured less, the first time you need to purchase the minimum
        first_purchase = self.purchases[0].datetime.date()
        if self._is_new_user_this_month(on_date):
            # if it's the first month of purchasing, we'll calculate from the first purchase
            app.logger.debug("PA: user is a first-month customer bought first {}".format(str(first_purchase.day)))
            should_purchase_this_month = self.dailybuy * (on_date.day - first_purchase.day +1)
        else:
            should_purchase_this_month = self.dailybuy * on_date.day
        app.logger.debug("PA: user should_purchase_this_month= {}".format(str(should_purchase_this_month)))
        should_purchase_today = should_purchase_this_month - summary["fiat_sum"]
        app.logger.debug("PA: user should_purchase_today {}".format(str(should_purchase_today)))
        if should_purchase_today <= 0:
            should_purchase_today = 0
        return should_purchase_today

    def calculate_next_potential_purchase(self, on_date=None):
        ''' calculates the next potential purchase amount and date '''
        if on_date == None:
            on_date=lovac.util.lovac_now().date()
        amount = self.calculate_purchase_amount(on_date)
        while (amount  < 6):
            app.logger.debug("amount is too less, adding a day")
            on_date += timedelta(days=1)
            amount = self.calculate_purchase_amount(on_date)
        on_date += timedelta(days=self.delay_buy_counter)
        app.logger.debug("next potential_purchase date: "+str(on_date))
        return (on_date, amount)

    def _summary_purchases(self, from_date, to_date= lovac.util.lovac_now().date()):
        ''' sums and averages for the purchases in this timewindow '''
        summary = {}
        summary["fiat_sum"] = 0
        summary["btc_sum"] = 0
        for purchase in self.purchases:
            mydate = purchase.datetime.date()
            if (mydate >= from_date) and (mydate <= to_date):
                summary["fiat_sum"] += round(purchase.fiat)
                summary["btc_sum"] += purchase.btc
        if summary["fiat_sum"] != 0:
            summary["btc_fiat_avg"] = summary["btc_sum"] / summary["fiat_sum"] 
        return summary

    def _is_new_user_this_month(self, on_date=None):
        ''' true if the first purchase has been done this month '''
        if on_date == None:
            on_date=lovac.util.lovac_now().date()
        first_purchase = self.purchases[0].datetime.date()
        return first_purchase.month == on_date.month and first_purchase.year == on_date.year