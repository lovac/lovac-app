''' A module optimized for more or less complex queries in the DB '''
from datetime import datetime, timedelta

from flask import current_app as app
from sqlalchemy import and_, extract, func

import lovac  # to patch util.lovac_today
from lovac.models import Purchase, User, db


def purchase_report():
    ''' How much has been purchased the last day/7days/month in USD? 
        Returns something like {'yesterday': 6, 'last_7_days': 42, 'last_30_days': 180}
    '''
    purchase_report = {}
    purchases_yesterday = db.session.query(func.sum(Purchase.fiat)).filter(\
        filter_purchases_yesterday() \
    ).all()
    purchases_last_7_days = db.session.query(func.sum(Purchase.fiat)).filter(\
        filter_purchases_last_7_days() \
    ).all()
    purchases_last_30_days = db.session.query(func.sum(Purchase.fiat)).filter(\
        filter_purchases_last_30_days() \
    ).all()
    purchase_report['yesterday']= (0 if purchases_yesterday[0][0] == None else purchases_yesterday[0][0])
    purchase_report['last_7_days']= (0 if purchases_last_7_days[0][0] == None else purchases_last_7_days[0][0])
    purchase_report['last_30_days'] = (0 if purchases_last_30_days[0][0] == None else purchases_last_30_days[0][0])

    return purchase_report

def register_report():
    ''' How much people registered the last months? '''
    register_report = db.session.query(extract('year', User.registered_on), extract('month', User.registered_on),func.count(extract('month', User.registered_on))) \
        .group_by(extract('year', User.registered_on),extract('month', User.registered_on)) \
        .order_by(extract('year', User.registered_on),extract('month', User.registered_on)).all()
    return register_report
    

def filter_purchases_yesterday():
    ''' to be used in a query like: Purchase.query.filter(purchases_yesterday()) '''
    return and_(Purchase.datetime >= yesterday(), Purchase.datetime < lovac.util.lovac_today())

def filter_purchases_last_7_days():
    ''' to be used in a query like: Purchase.query.filter(filter_purchases_last_7_days()) '''
    week_ago = lovac.util.lovac_today() - timedelta(days=7)
    return and_(Purchase.datetime >= week_ago, Purchase.datetime < tomorrow())

def filter_purchases_last_30_days():
    ''' to be used in a query like: Purchase.query.filter(filter_purchases_last_30_days()) '''
    month_ago = lovac.util.lovac_today() - timedelta(days=30)
    return and_(Purchase.datetime >= month_ago, Purchase.datetime < tomorrow())


def yesterday():
    '''  to be used in queries like "All stuf from yesterday" '''
    return lovac.util.lovac_today() - timedelta(days=1)

def tomorrow():
    ''' returns tomorrow's date '''
    return lovac.util.lovac_today() + timedelta(days=1) 
