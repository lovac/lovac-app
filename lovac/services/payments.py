''' A module for dealing with payments and lnd '''

import math

from grpc import RpcError

from flask import current_app as app

from lovac.models import Payment
from lovac.util import handle_exception

def get_payments(user=None, status="complete"):
    ''' returns a list of payments maybe filtered by User '''
    if user == None:
        payments = Payment.query.filter_by(status = status)
    else:
        payments = Payment.query.filter_by(user_id = user.email)
    payments = payments.filter_by(chainid = app.config["LND_CHAINID"])
    if status == None:
        app.logger.debug("len of payments"+str(len(payments.all())))
        return payments.all()
    else:
        return payments.filter_by(status = status).all()


def search_or_create_payment(user):
    ''' Searches for a (open) payment which is in the right area for the user 
        If it's not existing, it's moving all unpaid Payments to "error"
        and creates an approriate one.
    '''
    payments = get_payments(user = user, status = "pending_payment")
    recommended_paym = recommended_payment(user)
    for payment in payments:
        try:
            if not (payment.check_payment()):
                if payment.satoshi_amount >= math.fabs(recommended_paym) and not payment.has_expired():
                    return payment
                else:
                    payment.invalidate()
        except RpcError as rpc_error:
            handle_exception(rpc_error)
    payment = Payment(user, amount=math.fabs(recommended_paym))
    payment.generate_invoice()
    return payment

def calculate_lfees_sum(user):
    ''' calculates the lfees of a specific user '''
    if (user.bitstampconfig):
        return sum(tx.lfee for tx in user.bitstampconfig.purchases)
    else:
        return 0

def calculate_payments_sum(user):
    ''' returns the number of satoshis the user paid already '''
    payments_complete = get_payments(user = user)
    app.logger.debug("payments_complete : "+str(payments_complete))
    payments_sum = sum(payment.satoshi_amount for payment in payments_complete)
    return payments_sum

def calculate_lfees_payments_balance(user):
    ''' calculates the lfees minus the payments of a specific user '''
    return calculate_payments_sum(user) - calculate_lfees_sum(user)

def recommended_payment(user):
    ''' If you want to pay, how much would that be? 
        For now a really "simple formula" which results in 
        "pay as many times you want without paying to much" '''
    lfee_balance = calculate_lfees_payments_balance(user)
    if lfee_balance <= 0:
        return math.fabs(lfee_balance) + 1000
    else:
        what_about_paying_this = min(1000, math.fabs((calculate_lfees_payments_balance(user)-500)/2))
        return what_about_paying_this