''' 
taken from https://realpython.com/handling-email-confirmation-in-flask/ 
This will be used when:
  * The user confirms his mailaddress
  * The user forgets his Password
'''

from itsdangerous import URLSafeTimedSerializer

from flask import current_app as app
from flask import url_for

from lovac.sendgridd import sendmail

import os

FORGOT_PASSWORD_CONST="forgotpassword"


def generate_confirmation_token(email):
    ''' a token to validate your username '''
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])


def generate_forgotpassword_token(email):
    ''' returns a token which appends _forgotpassword to the email '''
    return generate_confirmation_token(email+"_"+FORGOT_PASSWORD_CONST)

def confirm_token(token, expiration=3600):
    ''' if the token is older than expirations (seconds), it will return False
    Otherwise the encoded Mailaddress '''
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except:
        return False
    return email


def confirm_forgotpassword_token(token, expiration=3600):
    ''' 
    returns the mail if the forgotpasswordtoken is valid. The default expiration-time of 1h (3600s) 
    is built for higher security requirements but can be overidden for e.g. confirm_mail.
    '''
    tokenvalue = confirm_token(token,expiration)
    if tokenvalue == False:
        return False
    email = tokenvalue.rsplit('_', 1)[0]
    staticstring = tokenvalue.rsplit('_', 1)[1]
    if staticstring == FORGOT_PASSWORD_CONST:
        return email
    else:
        return False


