''' Functions to send mails for various reasons '''

from lovac.services.tokenregistering import generate_confirmation_token
from lovac.services.notifications import update_notification
import lovac # mock sendgridd.sendmail

def send_confirmationmail(email):
    ''' send a confirmation-mail so that the user gets confirmed '''
    token = generate_confirmation_token(email)
    lovac.sendgridd.sendmail(email,"Confirm your Email-Address",mailtemplate="emailconfirmation",token=token)

def send_forgotpasswordmail(email):
    ''' sends a forgotpassword-mail so that the user can reset it '''
    token = generate_confirmation_token(email+"_forgotpassword")
    lovac.sendgridd.sendmail(email,"Forgot your password?",mailtemplate="forgotpassword",token=token)

def send_fiat_shortage(user):
    ''' send a mail informing the user that there is shortage of fiat '''
    lovac.sendgridd.sendmail(user.email,"Fiat shortage in your Exchange account",mailtemplate="fiat_shortage_warning",currency=user.bitstampconfig.currency)

def send_no_bitstamp_config(user):
    ''' Sends a mail informing the user to use the wizard '''
    lovac.sendgridd.sendmail(user.email,"Next step: Connect your Bitstamp Account",mailtemplate="no_bitstamp_config")

def send_generic(user, subject, mailtext):
    ''' wrapper around sendmail to not use it directly and update_notifications '''
    lovac.sendgridd.sendmail(user.email, subject, mailtext=mailtext)
    update_notification(user,'generic')

def send_broadcast(user, subject, mailtext):
    ''' wrapper around sendmail to not use it directly and update_notifications '''
    lovac.sendgridd.sendmail(user.email, subject, mailtext=mailtext)
    update_notification(user,'broadcast')
