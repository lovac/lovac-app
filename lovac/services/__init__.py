''' a middle-layer module shielding the controller from the DB and third-party-API-calls '''
import math
import pandas as pd
from requests import HTTPError
from datetime import datetime, date, timedelta
from flask import current_app as app

from sqlalchemy import and_

import lovac
import lovac.util
from lovac.util import lovac_now
from lovac.bitstampp import TradingClient
from lovac.models import User, AnonymousUser, InvitationCode
from lovac.models import Purchase
from lovac.models import db, LovacError
from flask_jwt_extended import get_jwt_identity
from lovac.tickerdata.models import TickerHourly



def current_user():
    ''' wraps get_jwt_identity with the old name but as a function '''
    # Todo: Der Servicelayer: verwendet er diese Funktion oder nicht?
    # Wie verhält er sich beim anonymous?
    # Das checken auf den User und dann setzen mit current_user ist fragwürdig!
    if get_jwt_identity() == None:
        return AnonymousUser()
    user = lovac.models.User.query.filter_by(email=get_jwt_identity()).first()
    if user != None:
        return user
    else:
        return AnonymousUser()

def load_user(user_id):
    ''' a convenience-function '''
    user = User.query.filter_by(email=user_id).first()
    if user:
        return user
    else:
        return None

def create_new_user(username,password):
    ''' creates a new user in the DB, might raise a LovacError  '''
    if not User.query.filter_by(email=username).first():
        user = User(email=username, password=password, admin=False)
        db.session.add(user)
        db.session.commit()
        app.logger.info("AUDIT: Created new User: {}".format(user.email))
    else:
        lovac.models.LovacError('Username already existing')

def delete_user(email):
    ''' Deleteing a user '''
    user_to_be_deleted=User.query.filter_by(email=email).first()
    if (user_to_be_deleted):
        if (user_to_be_deleted.bitstampconfig):
            db.session.delete(user_to_be_deleted.bitstampconfig)
        db.session.delete(user_to_be_deleted)
        app.logger.info("Deleted User: {}".format(email))
        db.session.commit()
    else:
        raise LovacError("User {} does not exist!".format(email))

def ensure_bitstamp_config(user):
    ''' raises a LovacErrror if the user does not have a valid bitstamp_config'''
    bitstamp_config = user.bitstampconfig
    if not bitstamp_config:
        raise lovac.models.LovacError('No Configuration for Bitstamp connection existing! Please configure!' + user.email) 

def test_bs_config(user,key,secret):
    ''' true if config works '''
    from lovac.models import LovacError
    try:
        trading_client=TradingClient(user,key,secret)
        balance = trading_client.get_balance("usd") # doesn't matter ar this point qhich quote to use
    except HTTPError as error:
        return False
    return True

def get_balance(user):
    ''' wraps the Bitstampp-call and catches exception (and wraps them in LovacErrors)
        something like:
        {'btc_available': 0.09591537, 'btc_balance': 0.09591537, 'btc_reserved': 0.0, 'fee': 0.25, 'usd_available': 0.08, 'usd_balance': 0.08, 'usd_reserved': 0.0}

    '''
    ensure_bitstamp_config(user)
    from lovac.models import LovacError
    try:
        currency = user.bitstampconfig.currency
        trading_client=TradingClient.from_user(user)
        balance = trading_client.get_balance(quote=currency)
    except RuntimeError as error:
        raise LovacError('There is an Error with Bitstamp:' + str(error))
    return balance

def calculate_bitstamp_balance_summary(user):
    ''' grabs the balance from bitstamp and adds some calculated values on top '''
    balance_summary = {}
    if user.bitstampconfig != None:
        balance = get_balance(user)
        balance_summary["currency"] = user.bitstampconfig.currency
        balance_summary['btc_balance'] = balance['btc_balance']
        balance_summary['fiat_available'] = balance['fiat_available']
        balance_summary['cold_storage'] = lovac.services.settings.get_sum_from_adresses(user) / 100000000
        balance_summary['btc_sum'] = balance_summary['btc_balance'] + balance_summary['cold_storage'] 
    else:
        balance_summary["currency"] = 'None'
        balance_summary['btc_balance'] = 0
        balance_summary['fiat_available'] = 0
        balance_summary['cold_storage'] = 0
        balance_summary['btc_sum'] = 0
    return balance_summary

def user_is_short_of_fiat(user):
    ''' returns whether the user is short of fiat '''
    ensure_bitstamp_config(user)
    balance_summary = calculate_bitstamp_balance_summary(user)
    return balance_summary['fiat_available'] < 6

def get_transactions_bitstamp(user=None, limit=100, descending=False):
    ''' wraps the Bitstampp-call and catches exception (and wraps them in LovacErrors)
    '''
    ensure_bitstamp_config(user)
    from lovac.models import LovacError
    try:
        trading_client=TradingClient.from_user(user)

        transactions = trading_client.get_transactions(user.bitstampconfig.currency, limit=limit,descending=descending)
    except RuntimeError as error:
        raise LovacError('There is an Error with Bitstamp:' + str(error))
    return transactions

def bought_today_bitstamp(user=None):
    ''' whether bitstamp thinks you have already bought today'''
    ensure_bitstamp_config(user)
    trading_client=TradingClient.from_user(user)
    transactions = trading_client.get_transactions(user.bitstampconfig.currency, limit=1)
    if len(transactions) == 0:
        return False
    last_transaction = transactions[0]
    last_bought=last_transaction['datetime']
    app.logger.debug("last_bought=" + str(last_bought.date()) + " now="+str(lovac.util.lovac_now().date()) )
    if lovac.util.lovac_now().date() == last_bought.date():
        return True
    else:
        return False

def save_tickers_hourly():
    ''' grab the ticker-price from bitstamp and save it in the DB '''
    from lovac.bitstampp import get_ticker
    for quote in ["usd","eur"]:
        ticker=get_ticker(quote=quote)
        date = datetime.fromtimestamp(int(ticker['timestamp']))
        ticker_hourly = TickerHourly("btc{}".format(quote), 'bitstamp',date,
            ticker['open'],ticker['last'],ticker['high'],ticker['low'],ticker['last'],ticker['last'])
        db.session.add(ticker_hourly)
        db.session.commit()


def get_purchase_data_for_ticker(user=User,from_date=date(2009, 1, 3),to_date=lovac.util.lovac_now().date()):
    ''' get the purchase-data in a way easy to merge with the tickerdata '''
    # tickerdata is doing that as well:
    from_date = from_date - timedelta(hours=120)
    db.session.add(user) # Make lazy loading possible in the next line
    if user.bitstampconfig:
        bitstampconfig_id = user.bitstampconfig.id
    else:
        bitstampconfig_id = -1 # This will guarantee that we'll get an empty result
    query = db.session.query(Purchase).filter(
        and_(Purchase.datetime >= from_date, Purchase.datetime <= to_date, Purchase.bitstampconfig_id == bitstampconfig_id )).order_by(Purchase.datetime).statement
    df = pd.read_sql(query, db.session.connection() ) #
    df['timestamp'] = pd.to_datetime(df['datetime'],unit='s')
    
    df.fillna(method="ffill",inplace=True)
    df.fillna(method="bfill",inplace=True)
    return df 

def get_users():
    ''' returns a list of all the users '''
    users = db.session.query(User).all()
    return users

def get_user_by_email(email):
    ''' returns a user with that mail if any, None if not '''
    user = lovac.models.User.query.filter_by(email=email).first()
    # A bit stupid this if/else but should demonstrate that we might want to do that different.
    # Throw an exception?
    if user != None:
        return user
    else:
        return None

def get_invitationcodes():
    ''' returns a list of all the Invitationcodes '''
    invitationcodes = db.session.query(InvitationCode).all()
    app.logger.debug("invitationCodesList: " + str(invitationcodes))
    return invitationcodes