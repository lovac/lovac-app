''' services related to integrate with the btc-network (roughly) '''
import requests


def get_balance(address):
    ''' use blockchain.info to figure out the balance on a specific address '''
    url = 'https://blockchain.info/address/{}?format=json'.format(address)
    resp = requests.get(url=url)
    data = resp.json() 
    return data["final_balance"]