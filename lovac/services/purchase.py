''' All sorts of services around Purchasing '''

import math

from flask import current_app as app
from requests import HTTPError  # for HTTPError

import lovac # lovac.util.lovac_now() is otherwise not mockable
from lovac.bitstampp import TradingClient, get_ticker_last
from lovac.models import LovacError, Purchase, db
from lovac.services import ensure_bitstamp_config, get_transactions_bitstamp
from lovac.services.strategy import AccumulationStrategy
from lovac.services.tickerdata import is_price_recovering
from lovac.util import handle_exception

def calculate_purchase_summary(user):
    ''' calculates a purchase summary, yield and so on '''
    summary = {}
    if user.bitstampconfig != None:
        transactions=lovac.services.purchase.get_transactions(user=user,descending=True)

        summary['currency'] = user.bitstampconfig.currency
        summary['btc_sum'] = sum(float(tx.btc) for tx in transactions) 
        summary['fiat_sum'] = float("{:10.2f}".format(abs(sum(float(tx.fiat) for tx in transactions))))
        if summary['btc_sum'] > 0:
            summary['average_price'] = "{:10.2f}".format(summary['fiat_sum'] / summary['btc_sum'])
        else:
            summary['average_price'] = 'non calculatable'
        currency = user.bitstampconfig.currency
        summary['btc_ticker_last'] = float(get_ticker_last(currency))
        if summary['btc_sum'] > 0:
            summary['yield'] = "{:10.2f}".format(((summary['btc_sum'] * summary['btc_ticker_last']) / summary['fiat_sum'] -1) *100) 
        else:
            summary['yield'] = 'non calculatable'
        summary['accum_setup'] = user.bitstampconfig.dailybuy
    else: # happens if admin calls it but user does not have bitstampconfig
        summary['currency'] = 'None'
        summary['btc_sum'] = 0
        summary['fiat_sum'] = 0
        summary['average_price'] = 'non calculatable'
        summary['btc_ticker_last'] = 0
        summary['yield'] = 'non calculatable'
        summary['accum_setup'] = 'None'
    return summary


def bought_today(user=None):
    ''' checks in the DB whether the user has bought today '''
    app.logger.debug("unfortunately, this function is not yet UserAware: " + str(user))
    # ToDo: make purchases multi-user-capable
    #user.bitstampconfig.purchases[0]
    # ToDo: check properly
    # Not that bad here because we'll check it on bitstamp again
    return False

def get_transactions(user, descending=False):
    ''' loads the transactions as stored in the DB
    '''
    ensure_bitstamp_config(user)
    from lovac.models import LovacError
    try:
        # reverse sort them so that the last one bought are first
        def getKey(purchase):
            ''' just to pass it for sort '''
            return purchase.datetime
        return sorted(user.bitstampconfig.purchases,key=getKey,reverse=descending)
    except RuntimeError as error:
        raise LovacError('There is an Error:' + str(error))

def sync_bitstamp_transactions(user, purge=False):
    ''' purges the Purchase-entries (FOR THIS USER) and repopulates it from the entries from bitstamp '''
    if purge != True:
        raise LovacError("Sync of Bitstamp-transactions without purging the table first is not supported yet")
    # this might be needed for the tests to work
    db.session.add(user.bitstampconfig)
    meta = db.metadata
    purchase = meta.tables["purchase"]
    db.session.execute(
        purchase.delete().where(
            purchase.c.bitstampconfig_id == user.bitstampconfig.id
        )
    )
    db.session.commit()
    # Now, the real sync
    currency = user.bitstampconfig.currency
    for transaction in get_transactions_bitstamp(user,limit=1000,descending=False):
        if "btc_"+currency in transaction:
            _save_bitstamp_purchase(user, transaction)

def _save_bitstamp_purchase(user, transaction):
    ''' 
    Saves a transaction OR a purchase in the purchase-table 
    whereas a transaction is something like:
     {"order_id": 789628842, "btc": "0.00051233", "btc_usd": 11711.1, 
        "eur": 0.0, "usd": "-6.00", "id": 47855738, "fee": "0.02", 
        "type": "2", "datetime": "2018-01-18 18:37:35"}
    and a Purchase looks like this:
     {'datetime': '2018-01-17 21:19:59.660310', 'id': '783497480', 'type': '0', 'price': '10726.48', 'amount': '0.00055880'} 
    '''
    currency = user.bitstampconfig.currency
    if not "usd" in transaction: # it's a Purchase
        transaction["fiat"] = 0 - float(transaction["price"]) * float(transaction["amount"])
        transaction["btc"] = float(transaction["amount"])
        transaction["btc_fiat"] = float(transaction["price"])
    else:
        if currency == 'eur':
            transaction["fiat"] = transaction["eur"]
            transaction["btc_fiat"] = transaction["btc_eur"]
        elif currency == 'usd':
            transaction["fiat"] = transaction["usd"]
            transaction["btc_fiat"] = transaction["btc_usd"]
        else:
            raise LovacError("currency {} does not exist!".format(currency))
    if float(transaction["fiat"]) > 0:
        raise LovacError("A bitstamp transaction is not allowed to sell Bitcoin: datetime: {} usd: ".format(transaction["datetime"], transaction["usd"] ))
    lfee = int(float(transaction["btc"]) * 100000000 * 0.0025)
    purchase = Purchase(
        user.bitstampconfig.id, 
        transaction["datetime"], 
        math.fabs(float(transaction["fiat"])), 
        transaction['btc'], 
        transaction['btc_fiat'],
        lfee,
        currency
    )
    db.session.add(purchase)
    db.session.commit()
    return purchase

def buy_the_dip_job():
    ''' A job called every hour which checks the price and might buy the dip
    '''
    fiat_sum, btc_sum = {}, 0
    fiat_sum["eur"], fiat_sum["usd"] = 0,0
    if is_price_recovering():
        app.logger.debug("buy_the_dip iterating through these users {}".format(lovac.services.get_users()))
        for user in lovac.services.get_users():
            app.logger.debug("buy_the_dip iterating through user {}".format(user))
            purchase = None
            try:
                if user.bitstampconfig and (user.bitstampconfig.strategy == u'buy_the_dip'):
                    app.logger.info("AUDIT: Let's buy the dip for user {}".format(user))
                    purchase = automatic_buy(user)
            except lovac.models.LovacError as error:
                app.logger.error("Lovac Error for {} because {}".format(user.email,str(error) ))
            except Exception as exception:
                app.logger.error("Unkinown Error for {}".format(user.email))
                handle_exception(exception)
            if purchase != None:
                fiat_sum[purchase.currency] = fiat_sum[purchase.currency] + purchase.fiat
                btc_sum = btc_sum + purchase.btc
        app.logger.info("AUDIT: bought the dip (BTC {}) with EUR {}, USD{}".format(btc_sum,fiat_sum["eur"], fiat_sum["usd"]))
    return btc_sum, fiat_sum
    

def automatic_buy(user):
    ''' checks whether the user already bought and do it
        There is no strategy-check, this needs to be done earlier!
    '''
    app.logger.debug("entering automatic_buy for user {}".format(user))
    ensure_bitstamp_config(user)
    strategy = AccumulationStrategy.from_user(user)
    if not user.bitstampconfig.automatic_buy_flag:
        raise LovacError("User {} set automatic_buy_flag to False".format(user))
    if not bought_today(user): 
        app.logger.info("Considering to buy for user {}".format(str(user)))
        if (user.bitstampconfig.delay_buy_counter == 0) and (strategy.calculate_purchase_amount() >= 6):
            app.logger.info("Buying for user {}".format(str(user)))
            return buy(user=user)
        elif (strategy.calculate_purchase_amount() >= 6):
            app.logger.info("User {} decided to defer the purchase".format(user.email))
            decrease_defer_counter(user)
        else:
            app.logger.info("User {} can't buy today because of bitstamp limit".format(user.email))
    else:
        app.logger.info("Nope, my user bought already manually, all fine!" + user.email)


def buy(user):
    ''' buys an amount (USD) of bitcoin and stores that purchase in the DB
    Checks before whether they have not been any purchase on that day.
    might raise RuntimeErrors
    returns a purchase object
    '''   
    ensure_bitstamp_config(user)
    from lovac.models import LovacError
    import requests 
    # Let's check whether we already bought today
    # Might raise a RuntimeError
    from lovac.services import bought_today_bitstamp
    bought_today=bought_today_bitstamp(user)
    if bought_today:
        raise LovacError('Sorry, you already bought today!')
    from lovac.services.strategy import AccumulationStrategy
    strategy = AccumulationStrategy.from_user(user)
    # 5 EUR in BTC (USD)
    fiat_amount = strategy.calculate_purchase_amount()
    from lovac.bitstampp import get_ticker
    currency=user.bitstampconfig.currency
    amount=fiat_amount/(float(get_ticker(quote=currency)["last"]))
    #result={}
    #result["amount"]=12
    # stupid Bitstamp API doesn't want us to call too often
    import time
    time.sleep(1)
    try:
        trading_client=TradingClient.from_user(user)
        app.logger.info("AUDIT: Buying Amount {} {} ( BTC {}) for user {} ".format(currency, fiat_amount,amount,str(user)))
        result = trading_client.buy_btc(amount,quote=currency)
        update_autostatus(user, 10)
    except RuntimeError as error:
        update_autostatus(user, 15)
        raise LovacError('There is an Error with Bitstamp:' + str(error))
    except HTTPError as error:
        raise LovacError('There is an Error with Bitstamp:' + str(error))
    # something like: generic: {'datetime': '2018-01-17 21:19:59.660310', 'id': '783497480', 'type': '0', 'price': '10726.48', 'amount': '0.00055880'} 
    purchase = _save_bitstamp_purchase(user, result)
    #purchase = Purchase(user.bitstampconfig.id, lovac.util.lovac_now(), dollar_amount, "usd", result['amount'])
    #db.session.add(purchase)
    #db.session.commit()
    # db.commit() //TODO: Use transacions!!!
    # What? Why? There is no other db-manipulation to put that statement within a transaction?!
    return purchase
    
def update_autostatus(user,status):
    ''' Stores the current status in the autostatus field
        10 = Bought DCA the other day
        11 = Bought the dip the other day
        15 = Had an error last bought 
        16 = does not have a bitstamp-config
        17 = automatic_buy_flag was False
    '''
    if user.autostatus != status:
        user.autostatus = status
        db.session.add(user)
        db.session.commit()
        app.logger.info("User {} changed autostatus, now {}".format(user,status))


def increase_defer_counter(user):
    ''' increases the defer-counter by 1 '''
    ensure_bitstamp_config(user)
    # Let's ensure that we'll buy at the last day of the month
    now = lovac.util.lovac_now()
    from lovac.util import days_in_current_month
    days_in_current_month = days_in_current_month()
    max_increase = days_in_current_month - now.day 
    if user.bitstampconfig.delay_buy_counter < max_increase:
        _set_defer_counter(user, user.bitstampconfig.delay_buy_counter +1)
    return user.bitstampconfig.delay_buy_counter

def decrease_defer_counter(user):
    ''' increases the defer-counter by 1 '''
    ensure_bitstamp_config(user)
    if user.bitstampconfig.delay_buy_counter <= 1:
        _set_defer_counter(user, 0)
    else:
        _set_defer_counter(user, user.bitstampconfig.delay_buy_counter -1)
    return user.bitstampconfig.delay_buy_counter

def reset_defer_counter(user):
    ''' sets the defer-counter back to 0 '''
    _set_defer_counter(user, 0)

def _set_defer_counter(user, value):
    ''' sets the defer_counter to a specific value, only internal '''
    user.bitstampconfig.delay_buy_counter = value
    db.session.add(user.bitstampconfig)
    db.session.commit()
    app.logger.info("AUDIT: User {} set defer-counter to {}".format(user.email, value))
