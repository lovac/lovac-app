''' Services which are encapsulating the complexity of sending mails (not too often) '''
from datetime import datetime, timedelta
from flask import current_app as app
import lovac # patch today
from lovac.models import Notification, db, LovacError
from sqlalchemy import and_
import lovac # being able to mock all the mail.send_* methods

class Purposes:
    please_confirm_email =  "please_confirm_email"
    please_send_fiat =      "please_send_fiat"
    please_configure_bitstampcredentials = "please_configure_bitstampcredentials"

def get_notifications(user, resolved=None):
    ''' returns a list of notifications filtered by User '''
    if user==None:
        notifications =Notification.query
    else:
        notifications = Notification.query.filter_by(user = user)
    if resolved!=None:
        notifications = notifications.filter_by(resolved = resolved)
    return notifications.all()

def _update_notification(user,purpose,sent_at=None,resolved=False):
    if sent_at == None:
        sent_at = lovac.util.lovac_today()
    notification = Notification(user,purpose,sent_at,resolved=resolved)
    db.session.add(notification)
    db.session.commit()

def update_notification(user,purpose,sent_at=None):
    ''' exposed function to create a notification for broadcast or generic mails '''
    if purpose == 'generic' or purpose == 'broadcast':
        _update_notification(user,purpose,sent_at, resolved=True)
    else:
        raise LovacError('you cannot update a notification which is not generic or broadcast')

def reset_notification(user,purpose):
    ''' set all the notification for that purpose for that user to true '''
    notifications = (
        db.session.query(Notification).filter(
            and_(Notification.user == user, Notification.sentpurpose == purpose )) \
        .all()
    )
    for notification in notifications:
        notification.resolved = True
    db.session.flush()
    db.session.commit()

def consider_notifying(user, purpose):
    ''' might send a mail '''
    from lovac.models import db
    today = lovac.util.lovac_today().date()
    db.session.add(user)
    notifications = db.session.query(Notification) \
        .filter(\
            and_(Notification.user_id==user.email, Notification.sentpurpose==purpose,Notification.resolved==False) \
        ) \
        .order_by(Notification.sent_at).all()
    send_flag=False
    if len(notifications) == 0:
        app.logger.debug("user{} has no past notifications, immediately sending ...")
        send_flag = True
    elif len(notifications) == 1:
        # make sure that we have not sent today
        app.logger.debug("user{} one past notifications, check for today ...")
        if notifications[0].sent_at.date() != today:
            app.logger.debug("hasn't been notified today ({}) but on {}, let's notify".format(datetime.today().date(),notifications[0].sent_at.date()))
            send_flag = True
    elif len(notifications) == 2:
        assert notifications[0].sent_at < notifications[1].sent_at
        if notifications[-1].sent_at.date() != today:
            app.logger.debug("2 notif already but hasn't been notified today ({}) but on {}, let's notify".format(datetime.today().date(),notifications[0].sent_at.date()))
            send_flag = True
    else: # means len(notifications) >= 3
        app.logger.debug(str(notifications[-1]))
        app.logger.debug(str(notifications[-2]))
        app.logger.debug(str(notifications[-3]))
        first_delta = notifications[-1].sent_at.date() - notifications[-2].sent_at.date()
        print(first_delta)
        second_delta = notifications[-2].sent_at.date() - notifications[-3].sent_at.date()
        app.logger.debug("delta: {} {}".format(first_delta,second_delta))
        app.logger.debug("last notidifcation: {}".format(notifications[2].sent_at))
        if (notifications[-1].sent_at + first_delta + second_delta).date() <= today:
            app.logger.debug("finbonacci thingy fulfilled ..")
            send_flag = True
    if not send_flag:
        return
    # ok, let's send that mail!
    app.logger.info("Notifying user {} for {}".format(user.email,purpose))
    if purpose == Purposes.please_confirm_email:
        lovac.services.mail.send_confirmationmail(user.email)
    elif purpose == Purposes.please_send_fiat:
        lovac.services.mail.send_fiat_shortage(user)
    elif purpose == Purposes.please_configure_bitstampcredentials:
        lovac.services.mail.send_no_bitstamp_config(user)
    else:
        raise LovacError("Cannot notify user {} for unknown purpose {}".format(user.email,purpose))
    _update_notification(user,purpose)

    
    
