''' services related to settings '''
from flask import current_app as app
from lovac.services import ensure_bitstamp_config, test_bs_config
from lovac.services.btc import get_balance
from lovac.models import BtcAddress, Bitstampconfig, LovacError
from lovac.models import db
from lovac.util import lovac_now, handle_exception

def get_sum_from_adresses(user):
    ''' returns the sum of bitcoins in cold storage '''
    ensure_bitstamp_config(user)
    sum = 0
    for btcaddress in user.bitstampconfig.btcaddresses:
        sum += btcaddress.sat
    return sum

def update_cold_storage():
    ''' checks all the btcaddresses and updates values '''
    app.logger.info("update_cold_storage ...")
    btcaddresses = db.session.query(BtcAddress).all()
    for btcaddress in btcaddresses:
        try:
            new_balance = get_balance(btcaddress.btcaddress)
            if new_balance < btcaddress.sat:
                app.logger.info("Spending on cold-storage {} detected".format(btcaddress.btcaddress))
            btcaddress.sat = new_balance
            btcaddress.syncdatetime = lovac_now()
            db.session.add(btcaddress.bitstampconfig)
            db.session.commit()
        except Exception as exception:
            handle_exception(exception)
    app.logger.info("Finished updating cold storage")


def save_or_update_bitstampconfig(user, bs_user, key, secret, currency="usd"):
    ''' Saves a bitstamp-config, does NOT do a tzransaction-sync!! '''
    email = user.get_id()
    bitstamp_config = user.bitstampconfig
    app.logger.debug("about to save_or_update_bitstampconfig: {} {} {}".format(str(user),bs_user,key))
    if bitstamp_config:
        raise LovacError('You cannot update a Bitstamp-configuration!')
    else:
        bitstamp_config = Bitstampconfig(
            user_id = email,
            username = bs_user,
            key = key,
            secret = secret,
            currency= currency
        )
        db.session.add(bitstamp_config)
        db.session.add(user)
        db.session.commit()
        app.logger.info("AUDIT: User {} set Bitstamp-Credentials for Bitstamp-Username {}".format(user,bs_user))
    try:
        test_bs_config(bitstamp_config.username,bitstamp_config.key,bitstamp_config.secret)
    except:
        raise LovacError('Bitstamp-configuration not valid!' + str(bitstamp_config))

def create_or_update_dailybuy(user, dailybuy, strategy, automatic_buy_flag):
    ''' updates the dailybuy '''
    if not dailybuy is None:
        user.bitstampconfig.dailybuy = dailybuy
    if not strategy is None:
        user.bitstampconfig.strategy = strategy
    if automatic_buy_flag == "on":
        user.bitstampconfig.automatic_buy_flag = True
    else:
        user.bitstampconfig.automatic_buy_flag = False
    db.session.add(user.bitstampconfig)
    db.session.commit()
    app.logger.info("AUDIT: User {} set dailybuy to {} and {} and {}".format(user,dailybuy,strategy, user.bitstampconfig.automatic_buy_flag))