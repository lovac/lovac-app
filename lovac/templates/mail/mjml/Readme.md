# Overview

The mails we're sending out should be pretty and so we're using [mjml](https://mjml.io/download) which can be installed e.g. by:
    
    npm init -y && npm install mjml

The mjml-templates are living in this directory. They can be developed via the [online-editor](https://mjml.io/try-it-live) or the mjml-plugin for VSCode.

They can get rendered via:

    $ ./node_modules/.bin/mjml lovac/templates/mail/mjml/mail_layout.mjml > lovac/templates/mail/html_mail_layout.html



    