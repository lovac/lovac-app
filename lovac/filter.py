"""implementing jinja2 filters in here"""
from datetime import datetime, timedelta

from flask import current_app as app

# @app.template_filter('bootstrap_notif_mapping')
# For some reason this doesn't work and we're injecting it explicitely in __init__.py
def bootstrap_notif_mapping(text):
    """maps reasonalble error-levels to notification-layouts in bootstrap"""
    if text=="message":
        return "info"
    if text=="error":
        return "danger"
    return text