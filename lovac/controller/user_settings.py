'''
Controller stuff for the user-settings
'''
import time #time.sleep(1) TBD fix it
import math 

from flask import request, flash, redirect, url_for, render_template
from flask import current_app as app
from flask_jwt_extended import create_refresh_token, jwt_required
from wtforms_components import read_only

import lovac.models
from lovac.models import db, Payment
from lovac.controller import CONTROLLER
from lovac.services import current_user, load_user
from lovac.services.purchase import sync_bitstamp_transactions
from lovac.services.settings import save_or_update_bitstampconfig
from .user_forms import BitstampconfigForm

@CONTROLLER.route('/wizard/step/<int:step>', methods=['GET'])
@CONTROLLER.route('/wizard', methods=['GET','POST'])
@jwt_required
def wizard(step=0):
    ''' a Wizard to help setup Bitstamp credentials and an accumulation-Plan '''
    if request.method == 'POST':
        form = BitstampconfigForm(request.form)
        if form.validate_on_submit():
            save_or_update_bitstampconfig(current_user(),        
                request.form.get('username'),
                request.form.get('key'),
                request.form.get('secret'),
                request.form.get('currency')
            )
            time.sleep(2)
            sync_bitstamp_transactions(current_user(), True)
            flash("Successfully stored!")
            return render_template('/wizard/wizard_4.html',mode="finished",form=form)
        else:
            flash("Double-Check your configuration and make sure your API-key is activated!","error")
            settings={"BITSTAMP_USERNAME":request.form.get('bitstamp_username'),"BITSTAMP_KEY":request.form.get('bitstamp_key'),"BITSTAMP_SECRET":request.form.get('bitstamp_secret')}
            return render_template('/wizard/wizard_4.html',mode="notfinished",settings=settings, form=form)
    else:
        if step == 4:
            return render_template('/wizard/wizard_{}.html'.format(str(step)),mode="notfinished",form = BitstampconfigForm(request.form))
        return render_template('/wizard/wizard_{}.html'.format(str(step)))
        
@CONTROLLER.route('/settings/bs-cred',methods=['GET'])
@jwt_required
def bitstampcred_get():
    ''' get-request for settings '''
    email = current_user().get_id()
    bitstamp_config = lovac.models.Bitstampconfig.query.filter_by(user_id=email).first()
    app.logger.debug("Bitstamp_config: "+ str(bitstamp_config)) 
    if current_user().bitstampconfig:
        form = BitstampconfigForm(formdata=None, obj=current_user().bitstampconfig, secret="***********")
        read_only(form.username)
        read_only(form.key)
        read_only(form.secret)
        read_only(form.currency)
        mode="DELETE"
    else:
        form = BitstampconfigForm(request.form)
        #read_only(form.currency)
        mode="POST"
    return render_template('settings_bitstamp_credentials.html', mode=mode, form=form)

@CONTROLLER.route('/settings/bs-cred',methods=['DELETE'])
@jwt_required
def bitstampcred_delete():
    ''' delete-request for settings, unfortunately not used (see POST-request) '''
    email = current_user().get_id()
    bitstamp_config = lovac.models.Bitstampconfig.query.filter_by(user_id=email).first()
    db.session.delete(bitstamp_config)
    db.session.commit()
    app.logger.info("AUDIT: User {} deleted Bitstamp-Credentials".format(email))
    flash("Bitstamp credentials deleted!")
    return redirect(url_for('controller.bitstampcred_get'))

@CONTROLLER.route('/settings/bs-cred',methods=['POST'])
@jwt_required
def bitstampcred_post():
    ''' post-request for settings '''
    email = current_user().get_id()
    bitstamp_config = current_user().bitstampconfig
    if (request.form.get('_method')=="DELETE"):
        db.session.delete(bitstamp_config)
        db.session.commit()
        app.logger.info("AUDIT: User {} deleted Bitstamp-Credentials".format(email))
        flash("Bitstamp credentials deleted!")
        return redirect(url_for('controller.bitstampcred_get'))
    else:
        if bitstamp_config:
            flash("Please delete first!")
            return render_template('settings_bitstamp_credentials.html', form=form, mode="POST")
        form = BitstampconfigForm(request.form)
        if form.validate_on_submit():
            save_or_update_bitstampconfig(current_user(),        
                request.form.get('username'),
                request.form.get('key'),
                request.form.get('secret'),
                request.form.get('currency')
            )
            time.sleep(2)
            sync_bitstamp_transactions(current_user(), True)
            flash("Successfully stored!")
            return redirect(url_for('controller.dashboard'))
        else:
            return render_template('settings_bitstamp_credentials.html', form=form, mode="POST")

@CONTROLLER.route('/settings/sync-transactions',methods=['POST'])
@jwt_required
def bitstamp_sync_post():
    ''' syncing the Bitstamp-Transactions down to purchases '''
    sync_bitstamp_transactions(current_user(), True)
    flash("Successfully synced Transactions")
    return redirect(url_for('controller.dashboard'))

@CONTROLLER.route('/lfee/payment-requ/<string:payment_req>',methods=['GET'])
def lfee_payment_requ(payment_req):
    ''' 
    returns true is the payment-rquest is already paid, otherwise false 
    This is somehow a tradeoff. This function can be used unauthorized.
    That's the downside.
    However, the javascript-code in charge-me.html is not able to load the JWT-token
    as this is stored in a httpOnly token. Much more secure as any potential XSS-attacks
    won't reveal the JWT-token to the malicious code. 
    '''
    payment = Payment.query.filter_by(payment_request = payment_req).first()
    if payment is None:
        return "False"
    return str(payment.check_payment())

@CONTROLLER.route('/adhocinvoice/<int:satoshis>',methods=['GET'])
def adhocinvoice(satoshis):
    ''' 
    returns an arbitrary invoice for the user0
    '''
    payment = Payment(load_user("user0"), amount=math.fabs(satoshis))
    payment.generate_invoice()
    return render_template("adhocinvoice.html",satoshis=satoshis,lnd_invoice=payment.payment_request)
