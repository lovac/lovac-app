'''
Controller stuff for the user for selfadministration.
'''
from datetime import datetime

from flask import request, flash, redirect, url_for, render_template, make_response
from flask import current_app as app
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, 
    set_access_cookies, set_refresh_cookies, unset_jwt_cookies)
from flask_bcrypt import Bcrypt

from lovac.models import User, db
from lovac.controller import CONTROLLER
from lovac.services import current_user, delete_user, load_user
from lovac.services.settings import save_or_update_bitstampconfig
from lovac.services.tokenregistering import confirm_token
from lovac.services.mail import send_confirmationmail, send_forgotpasswordmail
from lovac.services.notifications import reset_notification, Purposes
from .user_forms import RegisterForm, ChangepasswordForm, ResetpasswordForm, ForgotpasswordForm

@CONTROLLER.route('/login', methods=['GET', 'POST'])
def login():
    ''' login '''
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    # ToDo: use WTForms
    #form = LoginForm()
    # if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
    if request.method == 'POST': 
        user = User.query.filter_by(email=request.form['username']).first()
        bcrypt = Bcrypt(app)
        if not user:
            flash('Invalid username or password', "error")
            app.logger.info("Someone tried to login with email {}".format(request.form['username']))
            return render_template('selfadmin/login.html',data={'controller':'controller.login'}), 401
        elif user and request.form['username'] != user.email:
            flash('Invalid username or password', "error")
            return render_template('selfadmin/login.html',data={'controller':'controller.login'}), 401
        elif bcrypt.check_password_hash(bytes(user.password,"utf-8"), str(request.form['password'])):
            access_token = create_access_token(identity = user.email)
            refresh_token = create_refresh_token(identity = user.email)
            app.logger.info("AUDIT: Successfull Login by email {}".format(user.email))
            flash('Logged in successfully.',"info")
            if request.form.get('next') and request.form.get('next').startswith("http"):
                response = redirect(request.form['next'])
            else:
                response = redirect(url_for('controller.dashboard'))
            set_access_cookies(response, access_token)
            set_refresh_cookies(response, refresh_token)
            user.last_login = datetime.now()
            db.session.add(user)
            db.session.commit()
            return response
        else:
             app.logger.info("AUDIT: Invalid password login attempt by email {}".format(user.email))
             flash('Invalid username or password',"error")
             return render_template('selfadmin/login.html',data={'controller':'controller.login', next:''}), 401
    else:
        return render_template('selfadmin/login.html',data={'controller':'controller.login','next':request.args.get('next')})

@CONTROLLER.route('/logout', methods=['GET'])
@jwt_required
def logout():
    ''' logout '''
    flash('You were logged out')
    response = make_response(render_template('selfadmin/login.html',data={'controller':'controller.login'}))
    unset_jwt_cookies(response)
    app.logger.debug("Logging out some unknown user ")
    return response, 200

@CONTROLLER.route('/sendconfirmationmail', methods=['POST'])
@jwt_required
def send_confirmation_mail():
    ''' send the confirmation mail '''
    send_confirmationmail(current_user().email)
    flash("Mail was sent. Please check your Mail and click the confirmation-Link to confirm your Mailaddress!")
    return redirect(url_for('controller.dashboard'))

@CONTROLLER.route('/confirm/<token>')
@jwt_required
def confirm_email(token):
    ''' An endpoint to confirm your mailaddress '''
    try:
        email = confirm_token(token,expiration=604800) # One week time to confirm your password
    except:
        flash('The confirmation link is invalid or has expired.', 'danger')
        app.logger.info("User {} used outdated or wrong confirmation-link".format(user.email))
    if email == False:
        flash('The confirmation link is invalid or has expired. After login, you can create another confirmationmail!', 'danger')
        return redirect(url_for('controller.changepassword'))
    user = User.query.filter_by(email=email).first()
    if user != current_user():
        flash('The confirmation link is invalid or has expired.', 'danger')
        return redirect(url_for('controller.dashboard'))
    if user.confirmed:
        flash('Account already confirmed. Please login.', 'success')
    else:
        user.confirmed = True
        user.confirmed_on = datetime.now()
        db.session.add(user)
        db.session.commit()
        reset_notification(user,Purposes.please_confirm_email)
        flash('Your Account is now confirmed! Thanks!', 'success')
        app.logger.info("AUDIT: User {} confirmed".format(user.email))
    return redirect(url_for('controller.dashboard'))

@CONTROLLER.route('/register', methods=['GET', 'POST'])
def register():
    ''' the register endpoint '''
    form = RegisterForm(request.form)
    if form.validate_on_submit():

        user = User(
            email=form.email.data,
            password=form.password.data,
            invitationcode_code = form.invitation_code.data
        )
        db.session.add(user)
        db.session.commit()
        app.logger.info("AUDIT: new User registered: {} via {}".format(user.email,user.invitationcode_code))
        send_confirmationmail(user.email)
        return render_template('selfadmin/register_success.html')

    return render_template('selfadmin/register.html', form=form)

@CONTROLLER.route('/settings/change-password',methods=['POST','GET'])
@jwt_required
def changepassword():
    ''' changes the password '''
    form = ChangepasswordForm(request.form)
    if form.validate_on_submit():
        new_password = form.new_password1.data
        bcrypt = Bcrypt(app)
        current_user().set_password(new_password)
        db.session.add(current_user())
        db.session.commit()
        flash("Success!!")
        return redirect(url_for('controller.dashboard'))
    return render_template('selfadmin/settings_changepassword.html', form=form)

@CONTROLLER.route('/forgotpassword', methods=['GET', 'POST'])
def forgot_password():
    ''' the forgot password endpoint '''
    if request.method == 'POST':
        form = ForgotpasswordForm(request.form)
        if form.validate_on_submit():
            user = load_user(form.email.data)
            if user != None:
                app.logger.info("AUDIT: User {} requested a new Password".format(user.email))
                send_forgotpasswordmail(user.email)
            else:
                app.logger.info("AUDIT: Some requested a new Password for a not existing user {}".format(form.email.data))
        return render_template('selfadmin/forgotpassword_success.html')
    else:
        return render_template('selfadmin/forgotpassword.html',form=ForgotpasswordForm())

@CONTROLLER.route('/reset-password/<token>',methods=['POST','GET'])
def reset_password(token):
    ''' will reset the password '''
    if request.method == 'POST': 
        form = ResetpasswordForm(request.form)
        if form.validate_on_submit():
            new_password = form.new_password1.data
            bcrypt = Bcrypt(app)
            user = load_user(confirm_token(token).rsplit('_', 1)[0])
            user.set_password(new_password)
            db.session.add(user)
            db.session.commit()
            app.logger.info("AUDIT: User {} changed his Password".format(user.email))
            flash("Success!!")
            return render_template('selfadmin/login.html',data={'controller':'controller.login'})
        else:
            return render_template('selfadmin/resetpassword.html', form=form, token=token)
    else: # GET
        tokenvalue = confirm_token(token)
        if tokenvalue.endswith("_forgotpassword"):
            usermail = tokenvalue.rsplit('_', 1)[0]
            if load_user(usermail) == None:
                flash("what?","error")
                app.logger.debug("user {} not existing".format(usermail))
                return render_template('selfadmin/login.html',data={'controller':'controller.login'})
            else:
                app.logger.debug("user {} found".format(usermail))
        else:
            flash("what?","error")
            app.logger.debug("token error for tokenvalue {}".format(tokenvalue))
            return render_template('selfadmin/login.html',data={'controller':'controller.login'})
        return render_template('selfadmin/resetpassword.html', form=ResetpasswordForm(formdata=None, token=token), token=token)

@CONTROLLER.route('/close-account', methods=['GET', 'POST'])
@jwt_required
def close_account():
    ''' Can be called to close the account '''
    delete_user(current_user().email)
    flash('You closed your account! Good Bye! Happy to have you again!', 'success')
    return redirect(url_for('controller.login'))