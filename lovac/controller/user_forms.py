''' Forms for users'''
from flask_wtf import FlaskForm
from flask import current_app as app
from flask_bcrypt import Bcrypt
from wtforms import StringField, PasswordField, BooleanField, SelectField, HiddenField
from wtforms.validators import DataRequired, Email, Length, EqualTo, AnyOf

from lovac.services import current_user, test_bs_config, load_user
from lovac.services.tokenregistering import confirm_token
from lovac.models import User, InvitationCode

class ForgotpasswordForm(FlaskForm):
    ''' The FlaskForm for forgetting your password '''
    email = StringField(
        'email',
        validators=[DataRequired(), Email(message="Invalid email address."), Length(min=6, max=40)]
    )

class RegisterForm(ForgotpasswordForm):
    ''' The FlaskForm for registering '''


    password = PasswordField(
        'password',
        validators=[DataRequired(), Length(min=6, max=25)]
    )

    confirm = PasswordField(
        'Repeat password',
        validators=[
            DataRequired(),
            EqualTo('password', message='Passwords must match.')
        ]
    )

    invitation_code = StringField(
        'invitation code',
        validators=[
            DataRequired(),Length(min=6, max=40)
        ]
    )

    tos = BooleanField(
        "Accept Terms of Service",
        validators=[
            DataRequired()
        ]
    )

    def validate(self):
        ''' a validate Function validates the FLaskForm '''
        initial_validation = super(RegisterForm, self).validate()
        if not initial_validation:
            app.logger.debug("initial validation failed")
            app.logger.debug(self.errors)
            return False
        user = User.query.filter_by(email=self.email.data).first()
        if user:
            self.email.errors.append("Email already registered")
            return False
        invcode = InvitationCode.query.filter_by(code=self.invitation_code.data).first()
        if not invcode:
            self.invitation_code.errors.append("Invitation code not existing!")
            return False
        if invcode.exhausted():
            self.invitation_code.errors.append("Sorry, this invitation-code is already exhausted!")
            return False
        return True

class AbstractpasswordForm(FlaskForm):
    ''' An abstract FlaskForm for registering and forgot Password '''
    new_password1 = PasswordField(
        'New Password',
        validators=[DataRequired(), Length(min=6, max=25, message='New password must be between 6 and 25 characters long.')]
    )
    new_password2 = PasswordField(
        'Repeat new password',
        validators=[
            DataRequired(),
            EqualTo('new_password1', message='New passwords must match.')
        ]
    )

class ChangepasswordForm(AbstractpasswordForm):
    ''' The FlaskForm for registering '''
    current_password = PasswordField(
        'Old Password',
        validators=[DataRequired()]
    )

    def validate(self):
        ''' a validate Function validates the FlaskForm '''
        initial_validation = super(ChangepasswordForm, self).validate()
        bcrypt = Bcrypt(app)
        if ( not bcrypt.check_password_hash(current_user().password, self.current_password.data)):
            self.current_password.errors.append("Current password did not match!")
            return False
        if not initial_validation:
            app.logger.debug("Change_Password: initial validation failed")
            app.logger.debug(self.errors)
            return False
        return True

class ResetpasswordForm(AbstractpasswordForm):
    ''' The FlaskForm for registering '''
    token = HiddenField(
        'the token',
        validators=[DataRequired()]
    )

    def validate(self):
        ''' a validate Function validates the FlaskForm '''
        initial_validation = super(ResetpasswordForm, self).validate()
        tokenvalue = confirm_token(self.token.data)
        if not tokenvalue:
            self.token.errors.append("Token Expired!")
            return False
        if not tokenvalue.endswith("_forgotpassword"):
            self.token.errors.append("Invalid token!")
            return False
        else:
            usermail = tokenvalue.rsplit('_', 1)[0]
            if load_user(usermail) == None:
                self.token.errors.append("User does not exist!")
                return False
        if not initial_validation:
            app.logger.debug("forgot password: initial validation failed")
            app.logger.debug(self.errors)
            return False
        return True

class BitstampconfigForm(FlaskForm):
    ''' The FlaskForm creating a Bitstampconfig '''
    username = StringField(
        'username',
        validators=[DataRequired(), Length(min=5, max=10)]
    )

    key = StringField(
        'key',
        validators=[DataRequired(), Length(min=6, max=40)]
    )

    secret = PasswordField(
        'secret',
        validators=[DataRequired(), Length(min=20, max=40, message='bitstamp_secret is usually about 32 characters long.')]
    )

    currency = SelectField(
        'currency',
        choices=[('usd', 'USD'), ('eur', 'EUR')],
        validators=[DataRequired(),
            AnyOf(["usd","eur"], message=u'Invalid value, must be USD or EUR', values_formatter=None)]
    )



    def validate(self):
        ''' a validate Function validates the FlaskForm '''
        initial_validation = super(BitstampconfigForm, self).validate()
        if not initial_validation:
            app.logger.debug("BitstampconfigForm: initial validation failed")
            app.logger.debug(self.errors)
            return False
        if not test_bs_config(self.username.data, self.key.data, self.secret.data):
            self.username.errors.append("Bitstamp-Credentials seem to not be valid!")
            return False
        return True