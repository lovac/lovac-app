'''
All Admin-specific Controllers. This is currently, other then the user-stuff
jwt authenticated.
'''
import os

from flask import current_app as app
from flask import (flash, make_response, redirect, render_template, request,
                   url_for)
from flask_bcrypt import Bcrypt
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_required, set_access_cookies,
                                set_refresh_cookies, unset_jwt_cookies)
from sqlalchemy import extract, func

from lovac.api.security import require_admin
from lovac.controller import CONTROLLER
from lovac.models import (InvitationCode, LovacError, Payment, Purchase, User,
                          db)
from lovac.services import (calculate_bitstamp_balance_summary, current_user,
                            delete_user, get_invitationcodes,
                            get_user_by_email, get_users)
from lovac.services.mail import send_broadcast, send_generic
from lovac.services.notifications import get_notifications
from lovac.services.payments import (calculate_lfees_payments_balance,
                                     calculate_lfees_sum,
                                     calculate_payments_sum, get_payments)
from lovac.services.purchase import (calculate_purchase_summary,
                                     get_transactions)
from lovac.services.reporting import purchase_report, register_report

from .admin_forms import NewInvitationcodeForm, SendmailForm


@CONTROLLER.context_processor
def inject_current_identity():
    ''' "current_user" available in templates'''
    return {'current_user': current_user() }

@CONTROLLER.route('/admin/login', methods=['GET', 'POST'])
def admin_login():
    ''' login '''
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    # ToDo: use WTForms
    #form = LoginForm()
    # if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
    if request.method == 'POST': 
        app.logger.debug("Username {} tries to login on adminendpoint!".format(request.form.get("username")))
        current_user = User.query.filter_by(email=request.form['username']).first()
        bcrypt = Bcrypt(app)
        if not current_user:
            flash('Invalid username or password', "error")
            app.logger.debug("ERROR: User does not exist!is not admin")
            return render_template('selfadmin/login.html',data={'controller':'controller.admin_login'}), 401
        elif current_user and request.form['username'] != current_user.email:
            flash('Invalid username or password', "error")
            app.logger.debug("ERROR: Something wrong with the form!")
            return render_template('selfadmin/login.html',data={'controller':'controller.admin_login'}), 401
        elif not current_user.admin:
            flash('Invalid username or password',"error")
            app.logger.debug("ERROR: User is not admin")
            return render_template('selfadmin/login.html',data={'controller':'controller.admin_login'}), 401
        elif bcrypt.check_password_hash(current_user.password, request.form['password']):
            access_token = create_access_token(identity = current_user.email)
            refresh_token = create_refresh_token(identity = current_user.email)
            app.logger.info("AUDIT: User " + str(current_user) + " logged in successfully as Admin")
            flash('Logged in successfully.',"info")
            response = redirect(url_for('controller.admin_dashboard'))
            set_access_cookies(response, access_token)
            set_refresh_cookies(response, refresh_token)
            return response
        else:
             flash('Invalid username or password',"error")
             return render_template('selfadmin/login.html',data={'controller':'controller.admin_login'}), 401
    else:
        return render_template('selfadmin/login.html',data={'controller':'controller.admin_login'})

@CONTROLLER.route('/admin/logout', methods=['GET'])
@jwt_required
def admin_logout():
    ''' logout '''
    flash('You were logged out')
    response = make_response(render_template('selfadmin/login.html',data={'controller':'controller.admin_login'}))
    unset_jwt_cookies(response)
    return response, 200

@CONTROLLER.route('/admin/dashboard', methods=['GET'])
@jwt_required
@require_admin
def admin_dashboard():
    ''' Some stuff interesting to know '''
    autostatus_report = db.session.query(User.autostatus, func.count(User.autostatus)).group_by(User.autostatus).all()
    return render_template(
        'admin/dashboard.html', 
        autostatus_report=autostatus_report,
        register_report = register_report(),
        purchase_report = purchase_report()
    )

@CONTROLLER.route('/admin/usermanagement', methods=['GET'])
@jwt_required
@require_admin
def admin_usermanagement():
    ''' admin-usermanagement '''
    response = make_response(render_template('admin/usermanagement.html', users=get_users()))
    return response

def str2bool(v):
    ''' util-method: Getting a bool out of a String '''
    try:
        return v.lower() in ("yes", "true", "t", "1", "on")
    except:
        return False

@CONTROLLER.route('/admin/newuser', methods=['POST'])
@jwt_required
@require_admin
def admin_newuser():
    ''' creating a new user '''
    try:
        new_user = User(
            email = request.form.get('email'),
            password = request.form.get('password'),
            admin = str2bool(request.form.get('admin'))
        )
        db.session.add(new_user)
        db.session.commit()
        app.logger.info("AUDIT: New user {} (admin= {}) created ".format(request.form.get('email'),str2bool(request.form.get('admin'))))
        flash("Successfully created!")
    except LovacError as error:
        flash('Something went wrong'+ error.args,"error") 
    return redirect(url_for('controller.admin_dashboard'))

@CONTROLLER.route('/admin/deluser', methods=['POST'])
@jwt_required
@require_admin
def admin_deluser():
    ''' deleting a user '''
    try:
        email = request.form.get('email')
        delete_user(email)
    except LovacError as error:
        flash('Something went wrong'+ error.args, "error") 
    return redirect(url_for('controller.admin_dashboard'))

@CONTROLLER.route('/admin/invitationcodes', methods=['GET'])
@jwt_required
@require_admin
def admin_invitationcodes():
    ''' Not much here, just a dummy-reource for now'''
    invitationform = NewInvitationcodeForm()
    response = make_response(render_template('admin/invitationcodes.html', invitationcodes=get_invitationcodes(), invitationform=invitationform))
    return response

@CONTROLLER.route('/admin/newinvitationcode', methods=['POST'])
@jwt_required
@require_admin
def admin_newinvitationcode():
    ''' creating a new invitationcode '''
    invitationform = NewInvitationcodeForm(request.form)
    if invitationform.validate_on_submit():
        new_invitationcode = InvitationCode(
            code = invitationform.code.data,
            max_users = invitationform.max_users.data,
            comment = invitationform.comment.data
        )
        db.session.add(new_invitationcode)
        db.session.commit()
        flash("Successfully created!")
    return render_template('admin/invitationcodes.html', users=get_users(), invitationcodes=get_invitationcodes(), invitationform=invitationform)

@CONTROLLER.route('/admin/invitationcode-delete',methods=['POST'])
@jwt_required
@require_admin
def invitationcode_delete():
    ''' Removing a invitationcode '''
    invitationcode = InvitationCode.query.filter_by(code=request.form.get('code')).first()
    if invitationcode.used():
        flash("Sorry, this code is already used! Cannot get deleted!","warning")
    else:
        db.session.delete(invitationcode)
        db.session.commit()
        flash("Invitationcode sucessfully deleted!")
    return redirect(url_for('controller.admin_invitationcodes'))

@CONTROLLER.route('/admin/import-bcc-data', methods=['GET'])
@jwt_required
@require_admin
def admin_import_bcc_data():
    ''' Imports 200 days of tickerdata directly from Bitcoincharts'''
    from lovac.tickerdata import bcc_to_dataframe, csv_2_db
    #import btcusd
    dataframe=bcc_to_dataframe(200)
    dataframe.to_csv(os.path.join(app.root_path, '../instance/td-temp.csv'))
    csv_2_db()
    #import btceur
    dataframe=bcc_to_dataframe(200,symbol="btceur")
    dataframe.to_csv(os.path.join(app.root_path, '../instance/td-temp.csv'))
    csv_2_db(symbol="btceur")
    flash("Fine! Done!")
    return redirect(url_for('controller.admin_dashboard'))

@CONTROLLER.route('/admin/purge-bcc-data', methods=['GET'])
@jwt_required
@require_admin
def admin_purge_tickerdata():
    ''' Removes the content of the tickerdataHourly-table'''
    meta = db.metadata
    table = meta.tables["ticker_hourly"]
    app.logger.info('AUDIT: Admin clearing the ticker_hourly table: ' +  str(table))
    db.session.execute(table.delete())
    db.session.commit()
    flash("Fine! Done!")
    return redirect(url_for('controller.admin_dashboard'))

@CONTROLLER.route('/admin/import-tickerdata-file', methods=['POST'])
@jwt_required
@require_admin
def admin_import_tickerdata_file():
    ''' imports files from the instance-irectory, e.g. td-temp.csv 
        We can only directly import around 200 days. After that we need "historical
        data" in terms of csv-files
        Better import-mechanism sooner or later needed.
    '''
    from lovac.tickerdata import csv_2_db
    app.logger.info("AUDIT: Importing file: "+ request.form.get('filename'))
    csv_2_db(request.form.get('filename'))
    flash("What?","error")
    return redirect(url_for('controller.admin_dashboard'))

@CONTROLLER.route('/admin/user-details/<string:email>', methods=['GET'])
@jwt_required
@require_admin
def admin_userdetails(email):
    ''' The userdetails including payments of that user, being able to delete payments
        and also create artificial payments as benefit '''
    user = get_user_by_email(email)
    payments = get_payments(user, status=None)
    notifications = get_notifications(user)
    user_summary={}
    
    bitstamp_balance_summary = calculate_bitstamp_balance_summary(user)
    purchase_summary = calculate_purchase_summary(user)
    if user.bitstampconfig != None:
        purchases = get_transactions(user, descending=True)
        purchases_count = len(get_transactions(user))
        purchases = purchases[0:5]
    else:
        purchases_count = 0
        purchases = []
        #app.logger.debug("ERROR: "+str(error))
    user_summary["lfees_sum"] = calculate_lfees_sum(user)
    user_summary["payments_sum"] = calculate_payments_sum(user)
    user_summary["lfees_payments_balance"] = calculate_lfees_payments_balance(user)
    user_summary["payments_completed_count"] = len(get_payments(user, status="completed"))
    return render_template('admin/user_details.html', 
        user=user, payments=payments, user_summary=user_summary, 
        balance_summary = bitstamp_balance_summary, purchase_summary = purchase_summary, 
        purchases = purchases, purchases_count=purchases_count, sendmailform=SendmailForm(),
        notifications = notifications )

@CONTROLLER.route('/admin/user-details/<string:email>/sendmail', methods=['POST'])
@jwt_required
@require_admin
def admin_sendmail(email):
    ''' send an email to that user '''
    user = get_user_by_email(email) # be sure that the user exists
    email = user.email
    sendmailform = SendmailForm(request.form)
    if sendmailform.validate_on_submit():
        subject = sendmailform.mailsubject.data
        mailtext = sendmailform.mailtext.data
        send_generic(user, subject, mailtext)
        flash("Mail Successfully sent!")
    else:
        flash("There is an issue with that mail (probably subject too short)!","error")
    return redirect(url_for('controller.admin_userdetails',email=user.email,sendmailform=sendmailform))

@CONTROLLER.route('/admin/sendmail', methods=['GET'])
@jwt_required
@require_admin
def admin_sendmail_all_get():
    ''' send an email to all users '''
    sendmailform = SendmailForm(request.form)
    notifications = get_notifications(user=None)
    return render_template("admin/sendmail_all.html",sendmailform=sendmailform, notifications=notifications)

@CONTROLLER.route('/admin/sendmail', methods=['POST'])
@jwt_required
@require_admin
def admin_sendmail_all_post():
    ''' send an email to all users '''
    sendmailform = SendmailForm(request.form)
    if sendmailform.validate_on_submit():
        subject = sendmailform.mailsubject.data
        mailtext = sendmailform.mailtext.data
        users = get_users()
        app.logger.info("iterating over the {} users ...".format(str(len(users))))
        errorlist=""
        successcounter=0
        for user in users:
            try:
                send_broadcast(user, subject, mailtext)
                successcounter = successcounter + 1
            except Exception as e:
                errorlist += " " + user.email
                app.logger.error("sending mail to {} because of {}".format(user.email,e))
        if errorlist != "":
            flash("Issues sending mail to these folks: {} ".format(errorlist),"error")
            if successcounter != 0:
                flash("Mail Successfully sent to {} recipients!".format(successcounter))
            app.logger.error("Issues sending mail to these folks: {} ".format(errorlist))
        else:
            flash("Mail Successfully sent to {} recipients!".format(successcounter))
    else:
        flash("There is an issue with that mail (probably subject too short)!","error")
        return redirect(url_for("controller.admin_sendmail_get",sendmailform=sendmailform))
    return redirect(url_for('controller.admin_dashboard'))

@CONTROLLER.route('/admin/payment-delete',methods=['POST'])
@jwt_required
@require_admin
def payment_delete():
    ''' Removing a Payment from a user '''
    payment = Payment.query.filter_by(id=request.form.get('id')).first()
    user = payment.user
    db.session.delete(payment)
    db.session.commit()
    flash("payment sucessfully deleted!")
    return redirect(url_for('controller.admin_userdetails',email=user.email))

@CONTROLLER.route('/admin/config', methods=['GET'])
@jwt_required
@require_admin
def admin_listconfigs():
    ''' Listing all configuration-values of the running application '''
    list_of_secrets=["SECRET_KEY","JWT_SECRET_KEY","MJ_APIKEY_PRIVATE","SENDGRID_API_KEY","SECURITY_PASSWORD_SALT"]

    return render_template("admin/list_configs.html",configs=app.config,list_of_secrets=list_of_secrets)
