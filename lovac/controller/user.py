'''
Controller stuff for the user. This needs to be migrated to JQT
'''
from datetime import date, timedelta, datetime

from flask import request, flash, redirect, url_for, render_template
from flask import current_app as app
from flask_jwt_extended import (jwt_required, jwt_optional, get_jwt_identity)
from sqlalchemy.exc import InvalidRequestError, IntegrityError
import requests # For HttpError

from lovac.util import handle_exception
import lovac.models
from lovac.util import lovac_now
from lovac.models import LovacError, db, BtcAddress, AnonymousUser
from lovac.controller import CONTROLLER, check_bitstampconfig
from lovac.services import current_user, calculate_bitstamp_balance_summary, get_purchase_data_for_ticker
from lovac.services.purchase import increase_defer_counter, reset_defer_counter, buy, calculate_purchase_summary
from lovac.services.tickerdata import get_tickerdata
from lovac.services.btc import get_balance
from lovac.services.payments import calculate_lfees_payments_balance, search_or_create_payment
from lovac.services.settings import create_or_update_dailybuy
from lovac.bitstampp import get_ticker_last # hmmm shouldn't be used TBD: wrap by service


@CONTROLLER.context_processor
def inject_current_identity():
    ''' "current_user" available in templates'''
    return {'current_user': current_user() }

@CONTROLLER.context_processor
def inject_lfee():
    ''' "lfee" available in ALL templates'''
    if app.config.get("LND_DEACTIVATE",False):
        return {'lfee_sum': -1 , 'lnd_invoice': "error"}
    if current_user().bitstampconfig:
        try:
            lfee_sum = calculate_lfees_payments_balance(current_user())
            try:
                invoice = search_or_create_payment(current_user())
                return {'lfee_sum': lfee_sum , 'lnd_invoice': invoice.payment_request}
            except Exception as exception:
                if app.config.get("LND_IGNORE_ISSUES",False):
                    app.logger.warning("LND_IGNORE_ISSUES ignored a LND issue")
                else:
                    handle_exception(exception)
                return {'lfee_sum': -1 , 'lnd_invoice': "error"}
        except Exception as exception:
            if app.config.get("LND_IGNORE_ISSUES",False):
                app.logger.warn("LND_IGNORE_ISSUES ignored a LND issue")
            else:
                handle_exception(exception)
            return {'lfee_sum': -1 , 'lnd_invoice': "error"}
    else:
        return {'lfee_sum': 0 }

@CONTROLLER.context_processor
def inject_now():
    ''' "now" available in templates (tickerdata)'''
    return {'now': datetime.utcnow()}

@CONTROLLER.context_processor
def inject_lastmonth():
    ''' lastmonth available in templates  (tickerdata)'''
    return {'lastmonth': datetime.utcnow() - timedelta(weeks=4)}
    
@CONTROLLER.context_processor
def inject_lastlastmonth():
    ''' lastlastmonth available in templates  (tickerdata)'''
    return {'lastlastmonth': datetime.utcnow() - timedelta(weeks=8)}

@CONTROLLER.after_request
def add_header(response):
    ''' I'm so sure that we'll need that one day that's why i keep it here '''
    if get_jwt_identity() == None:
        app.logger.debug("get_jwt_identity is empty")
        return response   
    #app.logger.info("get_jwt_identity = " + str(get_jwt_identity()))
    # app.logger.info("add_header " + str(current_user()) )
    #set_access_cookies(response, access_token)
    #set_refresh_cookies(response, refresh_token)
    return response

@CONTROLLER.route('/')
@jwt_optional
def home():
    ''' homescreen, for now just a redirect '''
    if isinstance(current_user(),AnonymousUser) :
        app.logger.debug("Redirecting to login")
        return redirect(url_for('controller.login'))
    else:
        app.logger.debug("Redirecting to dashboard")
        return redirect(url_for('controller.dashboard'))
    

@CONTROLLER.route('/dashboard')
@jwt_required
@check_bitstampconfig
def dashboard():
    ''' the dashboard '''
#     balance=    {'btc_balance': 0.00514260, 'usd_available': 2.13, \
#  'fee': 0.24, 'btc_reserved': 0.00000000, \
#  'usd_balance': 2.13, 'btc_available': 0.00514260, \
#  'usd_reserved': 0.00}  
    try:
        balance_summary = calculate_bitstamp_balance_summary(current_user())
    except requests.HTTPError as error:
        flash('There was an API-Error with Bitstamp, please check your Auth! HttpStatusCode:' + str(error.response.status_code), "error")
        return redirect(url_for('controller.bitstampcred_get'))
    except lovac.models.LovacError as error:
        flash( str(error))
        return redirect(url_for('controller.bitstampcred_get'))
    transactions=lovac.services.purchase.get_transactions(user=current_user(),descending=True)
    purchase_summary = calculate_purchase_summary(current_user())
    return render_template('dashboard.html', balance_summary=balance_summary, purchase_summary=purchase_summary, purchases=transactions)



@CONTROLLER.route('/settings/accum-plan',methods=['GET'])
@jwt_required
@check_bitstampconfig
def accum_plan_get():
    ''' currently the daily-investment of the bitstamp_config '''
    accum_plan = {}
    daily_investment = current_user().bitstampconfig.dailybuy
    accum_plan["daily_investment"] = daily_investment
    accum_plan["monthly_investment"] = daily_investment * 30
    accum_plan["strategy_target"] = current_user().bitstampconfig.strategy
    accum_plan["strategy_dict"] = {'dollar_cost_averaging' : 'Dollar Cost Averaging', 'buy_the_dip' : 'Buy the dip'}
    accum_plan["automatic_buy_flag"] = current_user().bitstampconfig.automatic_buy_flag
    return render_template('settings_accumulation_plan.html',accum_plan=accum_plan, btcaddresses=current_user().bitstampconfig.btcaddresses)

@CONTROLLER.route('/settings/accum-plan',methods=['POST'])
@jwt_required
@check_bitstampconfig
def accum_plan_post():
    ''' currently the daily-investment of the bitstamp_config '''
    app.logger.debug("Entering accum_plan_post with User " + str(current_user()))
    create_or_update_dailybuy(current_user(),request.form.get('daily_investment'), request.form.get('strategy'), request.form.get('automatic_buy_flag'))
    flash("Success!!")
    return redirect(url_for('controller.accum_plan_get'))

@CONTROLLER.route('/settings/btcaddress',methods=['POST'])
@jwt_required
@check_bitstampconfig
def btcaddress_post():
    ''' Adding a btcaddress to the BistampConfig '''
    from bitcoin.wallet import CBitcoinAddress
    try:
        addr = CBitcoinAddress(request.form.get('btcaddress'))
        satoshis = get_balance(request.form.get('btcaddress'))
    except:
        flash("{} does not seem to be a valid bitcoin adress!".format(request.form.get('btcaddress')))
        return redirect(url_for('controller.accum_plan_get'))    
    btcaddress = BtcAddress(current_user().bitstampconfig.id, lovac_now(),request.form.get('btcaddress'), satoshis)
    db.session.add(btcaddress)
    try:
        db.session.commit()
    except (InvalidRequestError, IntegrityError) as e:
        db.session.rollback()
        flash("That Address is already in use")
        return redirect(url_for('controller.accum_plan_get'))
    flash("Success!!")
    return redirect(url_for('controller.accum_plan_get'))

@CONTROLLER.route('/settings/btcaddress-delete',methods=['POST'])
@jwt_required
@check_bitstampconfig
def btcaddress_delete():
    ''' Removing a btcaddress from the BistampConfig '''
    btcaddress = lovac.models.BtcAddress.query.filter_by(id=request.form.get('id')).first()

    if (btcaddress.bitstampconfig.user != current_user()) :
        flash("Hmmm, that did not work!")
        return redirect(url_for('controller.accum_plan_get'))
    db.session.delete(btcaddress)
    db.session.commit()
    flash("btcaddress sucessfully deleted!")
    return redirect(url_for('controller.accum_plan_get'))


@CONTROLLER.route('/buy',methods=['GET'])
@jwt_required
@check_bitstampconfig
def buy_get():
    ''' buy - shows the buy page '''
    from lovac.services import bought_today_bitstamp
    try:
        from lovac.services.strategy import AccumulationStrategy
        strategy = AccumulationStrategy.from_user(current_user())
        defer_days = current_user().bitstampconfig.delay_buy_counter
        bought_today = bought_today_bitstamp(current_user())
        #time.sleep(1)
        balance_summary = lovac.services.calculate_bitstamp_balance_summary(current_user())
        if bought_today:
            next_autobuy_date , purchase_amount = strategy.calculate_next_potential_purchase(datetime.utcnow().date() + timedelta(days=1))
        else:
            next_autobuy_date , purchase_amount = strategy.calculate_next_potential_purchase()
        buy_now_amount = strategy.calculate_purchase_amount()
        app.logger.debug("user.py - buy_get: buy_now_amount = {} by user {}".format(buy_now_amount,str(get_jwt_identity())))
        return render_template("buy_or_defer.html", 
            data={'manual_buy_enabled': not bought_today and (buy_now_amount >= 6) and (balance_summary["fiat_available"] > 6), 
                'defer_days':defer_days, 
                'next_autobuy_date':next_autobuy_date ,
                'purchase_amount': purchase_amount }, 
            balance_summary=balance_summary)
    except RuntimeError:
        flash('There is an API-Error with Bitstamp, please check your Auth! HttpStatusCode:' + str(e.response.status_code),"error")
        return redirect(url_for('controller.bitstampcred_get'))       
    except LovacError as error:
        flash('There was an Error' + str(error.args))
        return redirect(url_for('controller.dashboard'))

@CONTROLLER.route('/buy',methods=['POST'])
@jwt_required
@check_bitstampconfig
def buy_post():
    ''' buy - will buy 6 Dollar worth of BTC '''
    # Let's check whether we already bought today
    try:
        result=buy(current_user())
    except RuntimeError:
        flash('There is an API-Error with Bitstamp, please check your Auth! HttpStatusCode:' + str(e.response.status_code),"error")
        return redirect(url_for('controller.bitstampcred_get'))       
    except LovacError as error:
        flash('There was an Error' + str(error.args))
        return redirect(url_for('controller.dashboard'))
#    except:
#        flash('there was an unknown error')
#        return redirect(url_for('controller.dashboard'))
    return render_template('show_generic.html', generic=result)

@CONTROLLER.route('/defer',methods=['POST'])
@jwt_required
@check_bitstampconfig
def defer_post():
    ''' defer buying - will increase the defer-counter '''
    increase_defer_counter(current_user())
    return redirect(url_for('controller.buy_get'))

@CONTROLLER.route('/defer-reset',methods=['POST'])
@jwt_required
@check_bitstampconfig
def defer_reset_post():
    ''' resets the defer-counter back to 0 '''
    reset_defer_counter(current_user())
    return redirect(url_for('controller.buy_get'))


@CONTROLLER.route('/ticker')
@jwt_required
@check_bitstampconfig
def ticker():
    ''' ticker '''
    if current_user().bitstampconfig:
        tickersymbol = "btc{}".format(current_user().bitstampconfig.currency)
    else:
        tickersymbol = "btcusd"
    app.logger.debug("Ticker to be used: {}".format(tickersymbol))
    df=get_tickerdata(symbol=tickersymbol)
    df2=get_purchase_data_for_ticker(user=current_user())
    chart_data = df.to_json(orient='records',date_format="iso")
    chart_data2 = df2.to_json(orient='records',date_format="iso")
    data = {'chart_data': chart_data, 'chart_data2': chart_data2}
    return render_template('ticker.html', data=data)

@CONTROLLER.route('/ticker/monthly/<int:year>/<int:month>')
@jwt_required
@check_bitstampconfig
def ticker_monthly(year,month):
    ''' ticker but parametrized via month '''
    if current_user().bitstampconfig:
        tickersymbol = "btc{}".format(current_user().bitstampconfig.currency)
    else:
        tickersymbol = "btcusd"
    import calendar
    df=get_tickerdata(from_date=date(year, month, 1),to_date=date(year, month, calendar.monthrange(year,month)[1]),symbol=tickersymbol)
    df2=get_purchase_data_for_ticker(user=current_user(), from_date=date(year, month, 1),to_date=date(year, month, calendar.monthrange(year,month)[1]))
    chart_data = df.to_json(orient='records',date_format="iso")
    chart_data2 = df2.to_json(orient='records',date_format="iso")
    data = {'chart_data': chart_data, 'chart_data2': chart_data2}
    return render_template('ticker.html', data=data)