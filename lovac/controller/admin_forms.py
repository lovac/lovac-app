''' Forms for users'''
from flask_wtf import FlaskForm
from flask import current_app as app
from flask_bcrypt import Bcrypt
from wtforms import StringField, PasswordField, IntegerField, TextAreaField
from wtforms.validators import DataRequired, Email, Length, EqualTo, NumberRange
from wtforms.widgets import TextArea

from lovac.services import current_user
from lovac.models import User, InvitationCode


class NewInvitationcodeForm(FlaskForm):
    ''' The FlaskForm for creating a new Invitationcode '''
    code = StringField(
        'code',
        validators=[DataRequired(), Length(min=6, max=20)]
    )

    max_users = IntegerField(
        'password',
        validators=[DataRequired(), NumberRange(min=1, max=100)]
    )

    comment = StringField(
        'Comment',
        validators=[DataRequired()]
    )

    def validate(self):
        ''' a validate Function validates the FlaskForm '''
        initial_validation = super(NewInvitationcodeForm, self).validate()
        if not initial_validation:
            app.logger.debug("initial validation failed")
            app.logger.debug(self.errors)
            return False
        invitationcode = InvitationCode.query.filter_by(code=self.code.data).first()
        if invitationcode:
            self.email.errors.append("InvitationCode already registered")
            return False
        return True

class SendmailForm(FlaskForm):
    ''' The FlaskForm for sending a mail to a customer '''
    mailsubject = StringField(
        'mailsubject',
        validators=[DataRequired(), Length(min=6, max=998)]
    )

    mailtext = TextAreaField(
        'mailtext',
        widget=TextArea(),
        validators=[DataRequired()]
    )

    def validate(self):
        ''' a validate Function validates the FlaskForm '''
        initial_validation = super(SendmailForm, self).validate()
        if not initial_validation:
            app.logger.debug("initial validation failed")
            app.logger.debug(self.errors)
            return False
        return True
      