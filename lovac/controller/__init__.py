''' The controller '''
import traceback
from flask import Blueprint, render_template, make_response, redirect, url_for
from flask_jwt_extended.exceptions import NoAuthorizationError
from flask import current_app as app
from jwt.exceptions import ExpiredSignatureError
# from flask import Flask, session, g, abort

# import pandas as pd
# import sqlite3

from functools import wraps

from flask import flash, redirect, url_for, request

from lovac.models import db
from lovac.util import handle_exception
from lovac.services import current_user


# from bitstamp.client import BitstampError 

# import json


CONTROLLER = Blueprint('controller', __name__, template_folder='templates')

def check_bitstampconfig(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if not current_user().bitstampconfig:
            flash("Please specify Bistamp-credentials first!")
            return redirect(url_for('controller.bitstampcred_get'))
        return func(*args, **kwargs)
    return decorated_function

import lovac.controller.user
import lovac.controller.user_selfadmin
import lovac.controller.user_settings
import lovac.controller.admin

@CONTROLLER.errorhandler(Exception)
def handle_error(error):
    ''' this tries to fix #12 where an internal DB-error renders the application unusable'''
    app.logger.error("handled an error on bp-level for request{}: {}".format(request.url,error))
    if isinstance(error, ExpiredSignatureError):
        response = make_response(redirect(url_for('controller.login')))
        return response, 307 # temporary redirect - browser will follow
    if isinstance(error, NoAuthorizationError):
        response = make_response(redirect(url_for('controller.login')+"?next={}".format(request.url)))
        return response, 307 # temporary redirect - browser will follow
    handle_exception(error)
    from lovac.models import LovacError
    db.session.rollback()
    response = make_response(render_template('errorpages/unspecific_error.html'))
    return response, 500



