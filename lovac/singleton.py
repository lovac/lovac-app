''' a singleton-like module to bootstrap the Flask-app '''
import os
import sys
import lovac
from logging.config import dictConfig
import logging


def app():
    ''' a function to create the app'''
    app = lovac.create_app()
    app.app_context().push()
    lovac.init_db(app)
    lovac.init_app(app)
    return app

# Flask is recommending to configure logging before creating the Flask-App
if os.environ.get("FLASK_DEBUG") != "True":
    dictConfig({
        'version': 1,
        'formatters': {'default': {
            'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
        }},
         'handlers': {'wsgi': {
             'class': 'logging.StreamHandler',
             'stream': sys.stdout,
             'formatter': 'default'
         }},
        'root': {
            'level': 'INFO',
            'handlers': ['wsgi']
        }
    })
    # in prod, we don't want request/response to be logged (we already have that in nginx)
    logging.getLogger('werkzeug').setLevel(logging.ERROR)

app=app()


# You want something specific in DEBUG ?!
for logger in [
    #app.logger,
    #logging.getLogger('sqlalchemy'),
    logging.getLogger('lovac.services.tickerdata')
]:
    logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run()