''' the lovac init '''
import traceback
import os

from flask import Flask, g, render_template, redirect, url_for, make_response
from werkzeug.routing import HTTPException
from flask_sqlalchemy import SQLAlchemy
from flask_qrcode import QRcode
from lovac.lnd import Lnd
from lovac.util import handle_exception

def create_app(config="lovac.config.DevelopmentConfig"):
    ''' A minimal Flask-app creation-function basically only doing config '''
    if os.environ.get("CONFIG"):
        config = os.environ.get("CONFIG")

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config)
    app.config['CONFIG_ORIG'] = config
    return app

def init_db(app, purge_db=False):
    ''' Initialize the Database with SQLAlchemy '''
    # lnd is imported in model so we need to initialize it here
    try:
        Lnd(app)
    except Exception as e:
        # might fail e.g. because of FileNotFoundError: [Errno 2] No such file or directory: './data/lnd/tls.cert'
        handle_exception(e)
    from lovac.models import User, Bitstampconfig, Purchase, db
    import lovac.tickerdata.models
    app.logger.debug("calling db.init_app(app)")
    db.init_app(app)
    if purge_db:
        db.drop_all()
    # see #5 this create_all() call need to disappear mid-/longterm
    # However, it's still so convenient
    db.create_all()
    db.session.commit()
    if not User.query.filter_by(email='admin').first():
        admin = User(email="admin", password="derida99", admin=True)
        db.session.add(admin)
        db.session.commit()
    if not User.query.filter_by(email='user0').first():
        user0 = User(email="user0", password="0re5u", admin=False)
        db.session.add(user0)
        db.session.commit()

# Separating creation and initialisation is necessary
# see blogpost 17nd march 2018 
def init_app(app):
    ''' Enrich app '''

    ####################
    #### extensions ####
    ####################

    from flask_jwt_extended import JWTManager
    jwt = JWTManager(app)

    @jwt.expired_token_loader
    @jwt.invalid_token_loader
    def expired_token_callback():
        ''' A callback for an invalid or expired token '''
        return render_template('errorpages/no_valid_token.html'), 401

    @jwt.unauthorized_loader
    def cb_no_token_callback(reason):
        ''' a callback for no token at all '''
        return redirect(url_for('controller.login')), 401

    ####################
    #### blueprints ####
    ####################

    @app.errorhandler(Exception)
    def handle_error(error):
        ''' this tries to fix #12 where an internal DB-error renders the application unusable'''
        if isinstance(error, HTTPException): # filtering normal status-code-stuff
            return error.get_response({"wtf":"unused_param_what_the_heck"}), error.code
        app.logger.error("handled an error on app-level: " + str(error))
        handle_exception(error)
        response = make_response(render_template('errorpages/unspecific_error.html'))
        return response, 500

    # registering the controller (.py)
    from lovac.controller import CONTROLLER
    from lovac.api import api_bp

    app.register_blueprint(CONTROLLER)
    app.register_blueprint(api_bp)

    # Set jinja template global
    app.jinja_env.globals['type'] = type
    app.jinja_env.globals['str'] = str

    # specify additional filters
    import lovac.filter
    app.jinja_env.filters['bootstrap_notif_mapping'] = lovac.filter.bootstrap_notif_mapping

    ######################
    #### flask-QRcode ####
    ######################

    # Being able to create QR-Codes on jinja2
    QRcode(app)


    #####################
    #### flask-login ####
    #####################

    from lovac.models import User

    def load_user(user_id):
        ''' a convenience-function formerly needed by Flask-Login, no used by Tests '''
        user = User.query.filter_by(email=user_id).first()
        if user:
            return user
        else:
            return None
    app.load_user = load_user

    from lovac.cron import run_schedule
    import lovac.tickerdata
    import lovac.tickerdata

    # tiny ugly hack copied from:
    # https://networklore.com/start-task-with-flask/
    # this code will trigger functions which are marked with @app.before_first_request
    # see cron.py to find such functions
    def start_runner():
        ''' try to create a request to kickstart the app immediately without waiting on the first request'''
        def start_loop():
            ''' make the f*#king pylint happy '''
            login_url = "http://"+app.config["SERVER_NAME"]+"/login" # https will get redirected on prod (hopefully)
            started = False
            for i in range(0,3):
                app.logger.info('start loop trying to access {} the {} time'.format(login_url,i))
                try:
                    import requests
                    request = requests.get(login_url)
                    # 401 unauthorized is exactly what we expect and enough to trigger the startcode
                    if request.status_code == 401 or request.status_code == 200:
                        app.logger.info('Server started, quitting start_loop')
                        app.logger.info('Up and running at {}'.format(login_url))
                        return
                except:
                    app.logger.info('Server not yet started')
                time.sleep(i)
            app.logger.fatal("Application didn't came up at {} Did you properly \n\
                setup SERVER_NAME and have that SERVER_NAME setup in /etc/hosts ?! Check also config.py. \n\
                This is necessary in order to be able to send mails via cronjob and have a proper cookie-setup. \n\
                If you don't care that much you can alternatively simply set SERVER_NAME=localhost:5000\n\
                export SERVER_NAME=localhost:5000".format(login_url))
            raise Exception("App didn't came up at {} check the logs for hints!".format(login_url))

        import threading
        import time
        thread = threading.Thread(target=start_loop)
        thread.start()


    if __name__ == "lovac":
        if not app.config["TESTING"]:
            app.logger.info("starting the runner")
            start_runner()

    # end of tiny ugly hack


    ########################
    #### error handlers ####
    ########################

    @app.errorhandler(401)
    def forbidden_page(error):
        ''' the handler for a 401-forbidden error --> login-page '''
        app.logger.error(error)
        return redirect(url_for('controller.login')), 401
    
    if  app.config.get("CONFIG_ORIG").endswith("TestConfig"):
        app.logger.info("Skipping long list of CONFIGURATION-OVERVIEW------------ in TestConfig")
        return app
    app.logger.info("-------------------------CONFIGURATION-OVERVIEW------------")
    app.logger.info("Config from "+os.environ.get("CONFIG","empty"))
    app.logger.info("SQLALCHEMY_DATABASE_URI "+app.config.get("SQLALCHEMY_DATABASE_URI","empty"))
    for key, value in app.config.items():
        app.logger.info("{} = {}".format(key,value))
    app.logger.info("-----------------------------------------------------------")
    return app