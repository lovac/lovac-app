"""
REST API Resource Routing

http://flask-restful.readthedocs.io/en/latest/
"""

import time
from flask import request
from lovac.api.rest.base import BaseResource, SecureResource, rest_resource

import lovac.api.rest.resources_tickerdata
import lovac.api.rest.resources_auth

@rest_resource
class ResourceSmoketest(BaseResource):
    """ /api/smoketest """
    endpoints = ['/smoketest']

    def get(self):
        return "i am alive"