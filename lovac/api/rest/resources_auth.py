'''
simple hello world rest
'''

from flask import current_app as app
from flask_jwt_extended import (create_access_token, create_refresh_token, 
        jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)

from lovac.api.rest.base import BaseResource, SecureResource, AdminResource, rest_resource
from lovac.api.security import require_admin
import lovac
from flask_bcrypt import Bcrypt
import lovac.models
from  lovac.services import create_new_user
from lovac.models import LovacError

@rest_resource
class ResourceHelloUser(SecureResource):
    """ /api/v1alpha/hello """
    endpoints = ['/v1alpha/hello']
    @require_admin
    def get(self):
        ''' say hello to the User '''
        current_user = get_jwt_identity()
        return { "hello" : current_user }

from flask_restful import reqparse

parser = reqparse.RequestParser()
parser.add_argument('username', help = 'This field cannot be blank', required = True)
parser.add_argument('password', help = 'This field cannot be blank', required = True)


@rest_resource
class UserRegistration(BaseResource):
    ''' /api/v1alpha/register '''
    endpoints = ['/v1alpha/register']
    def post(self):
        data = parser.parse_args()
        try:     
            lovac.services.create_new_user(data['username'],data['password'],)
            return {
                'message': 'User {} was created'.format( data['username'])
            }
        except LovacError as error:
            return {'message': 'Something went wrong'+ error.args}, 500
        return 

@rest_resource
class UserLogin(BaseResource):
    """ /api/v1alpha/auth """
    endpoints = ['/v1alpha/auth']
    def post(self):
        data = parser.parse_args()
        current_user = lovac.models.User.query.filter_by(email=data['username']).first()
        if not current_user:
            return {'message': 'User {} doesn\'t exist'.format(data['username'])}
        bcrypt = Bcrypt(app)
        if bcrypt.check_password_hash(current_user.password, data['password']):
            access_token = create_access_token(identity = data['username'])
            refresh_token = create_refresh_token(identity = data['username'])
            return {'message': 'Logged in as {}'.format(current_user.email),
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        else:
            return {'message': 'Wrong credentials'}

@rest_resource
class TokenRefresh(BaseResource):
    """ /api/v1alpha/refresh_token """
    endpoints = ['/v1alpha/refresh']
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        return {'access_token': access_token}