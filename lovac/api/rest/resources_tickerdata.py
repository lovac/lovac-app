"""
REST API tickerdata

"""

import time
from datetime import date, timedelta
from calendar import monthrange
from flask import request
from lovac.api.rest.base import BaseResource, SecureResource, rest_resource
from  lovac.services.tickerdata import get_tickerdata
from flask_restful import fields, marshal_with

class PandaTimestampRenderer(fields.Raw):
    ''' A renderer for pandas Timestamp Datatype '''
    def format(self, value):
        return str(value.to_pydatetime())

''' These are the price-data fields '''
fields = {
    'timestamp': PandaTimestampRenderer(),
    'last' : fields.Float,
    'high' : fields.Float,
    'low' : fields.Float,
    'open' : fields.Float,
    'weightedprice' : fields.Float,
    'ma20' : fields.Float(default=float('nan')),
    'lower_band' : fields.Float(default=float('nan')),
    'upper_band' : fields.Float(default=float('nan'))
}


@rest_resource
class ResourcePricedata(BaseResource):
    """ /api/v1alpha/prices """
    endpoints = ['/v1alpha/prices/<string:symbol>']
    @marshal_with(fields)
    def get(self, symbol):
        ''' get pricedata of alltime '''
        df=get_tickerdata(symbol=symbol)
        return df.to_dict(orient='records')


@rest_resource
class ResourcePricedataYear(BaseResource):
    """ /api/v1alpha/prices/btcusd/2017 """
    endpoints = ['/v1alpha/prices/<string:symbol>/<int:year>']
    @marshal_with(fields)
    def get(self, symbol, year):
        ''' get pricedata of a specific year '''
        df=get_tickerdata(from_date=date(year, 1, 1),to_date=date(year, 12, 31), symbol=symbol)
        return df.to_dict(orient='records')

@rest_resource
class ResourcePricedataMonth(BaseResource):
    """ /api/v1alpha/prices/btcusd/2018/12 """
    endpoints = ['/v1alpha/prices/<string:symbol>/<int:year>/<int:month>']
    @marshal_with(fields)
    def get(self, symbol, year, month):
        ''' get pricedata of a specific month '''
        df=get_tickerdata(from_date=date(year, month, 1),to_date=date(year, month,  monthrange(year, month)[1]), symbol=symbol)
        return df.to_dict(orient='records')

@rest_resource
class ResourcePricedataDay(BaseResource):
    """ /api/v1alpha/prices/btcusd/2017/6/12 """
    endpoints = ['/v1alpha/prices/<string:symbol>/<int:year>/<int:month>/<int:day>']
    @marshal_with(fields)
    def get(self, symbol, year, month, day):
        ''' get pricedata of a specific day '''
        df=get_tickerdata(from_date=date(year, month, day),to_date=date(year, month, day)+timedelta(days=1), symbol=symbol)
        return df.to_dict(orient='records')

