""" Security Related things """
from functools import wraps
from flask_restful import abort
from lovac.models import User
from flask import current_app as app


from flask_jwt_extended import get_jwt_identity

def require_admin(func):
    """ User needs Admin-rights method decorator """
    @wraps(func)
    def wrapper(*args, **kwargs):
        ''' this needs to get implemented properly '''
        # Verify if User is Admin
        current_user = get_jwt_identity()
        if not current_user:
            return abort(401)
        app.logger.debug("User is :" + current_user)
        current_user = User.query.filter_by(email=current_user).first()
        if current_user == None:
            return abort(401)
        if current_user.admin:
            return func(*args, **kwargs)
        else:
            return abort(401)
    return wrapper