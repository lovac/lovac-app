''' Utility methods for lovac '''

from datetime import datetime, timedelta
from calendar import monthrange
import traceback
import pytz

from flask import current_app as app

import lovac # to successfully patch lovac.mailjett.sendmail

def lovac_now():
    ''' let's wrap datetime now in order to avoid
     https://stackoverflow.com/questions/20503373/how-to-monkeypatch-pythons-datetime-datetime-now-with-py-test 
    '''
    return datetime.now(tz=pytz.utc)

def lovac_today():
    ''' let's wrap datetime now in order to avoid
     https://stackoverflow.com/questions/20503373/how-to-monkeypatch-pythons-datetime-datetime-now-with-py-test 
    '''
    return datetime.today()

def days_in_current_month(date=None):
    ''' returns the number of days in the current month or the month from the date-passed '''
    if date==None:
        date = lovac_now()
    days_in_month = monthrange(date.year, date.month)[1]
    return days_in_month

def first_day_of_month(date=None):
    ''' returns the first day of the current month or the month from the date-passed '''
    if date==None:
        date = lovac_now()
    return datetime(date.year, date.month, 1, 12, 30, 45).date()

def is_last_day_of_month(date=None):
    ''' returns true if it's the last day of the month and might take a date'''
    if date==None:
        date = lovac_now().date()
    days_in_that_month = monthrange(date.year,date.month)[1]
    return date.day == days_in_that_month

def is_middle_of_month(date=None):
    ''' returns true if it's the 16 of the month and might take a date'''
    if date==None:
        date = lovac_now().date()
    return date.day == 16

def handle_exception(exception, user=None):
    ''' prints the exception and most important the stacktrace '''
    from lovac.controller import current_user
    if user == None:
        try:
            usermail = current_user().email()
        except:
            usermail = "unknown"
    else:
        usermail = user.email
    app.logger.error("Unexpected error for user {}:".format(usermail))
    app.logger.error("----START-TRACEBACK-----------------------------------------------------------------")
    app.logger.exception(exception)    # the exception instance
    app.logger.error("----END---TRACEBACK-----------------------------------------------------------------")
    try:
        # This doesn't seem to work at all!
        # Let's reduce errors AND resolve this before reactivating.
        # mailtext = """Unexpected error for user {}:
        # ----START-TRACEBACK-----------------------------------------------------------------
        # {}
        # ----END---TRACEBACK-----------------------------------------------------------------""".format(current_user_mail, traceback.format_exc())
        # if app.config["SEND_MAILS"]:
        #     lovac.mailjett.sendmail("kneunert@gmail.com","Error in Lovac",mailtext=mailtext)
        from lovac.models import db
        db.session.rollback()
    except Exception as e:
        app.logger.error("Issue in handle_exception: {}".format(e))
