"""
This call sends an email to one recipient, using a validated sender address
Do not forget to update the sender address used in the sample
"""

import mailjet_rest
import os
import email_validator

from flask import current_app as app
from flask import render_template

def sendmail(recipient, subject, mailtemplate="plain", **context):
    ''' Sends a mail via mailjet '''   

    email_validator.validate_email(recipient)
    subject = "[Lovac] "+subject
    mailhtml = render_template("mail/{}.html".format(mailtemplate), **context)
    mailtext = render_template("mail/{}.txt".format(mailtemplate), **context)
    data = {
    'FromEmail': 'admin@lovac.xyz',
    'FromName': 'Lovac Administrator',
    'Subject': subject,
    'Text-part': mailtext,
    'Html-part': mailhtml,
    'Recipients': [
                    {
                            "Email": recipient
                    }
            ]
    }
    if not app.config["SEND_MAILS"]:
        app.logger.info("Mailjett: Not sending any mail because SEND_MAILS=False")
        return
    api_key = app.config['MJ_APIKEY_PUBLIC']
    api_secret = app.config['MJ_APIKEY_PRIVATE']
    if api_key == "not-set" or api_secret == "not_set":
        app.logger.error("Mailjet credentials not available. Not sending any mail!")
    mailjet = mailjet_rest.Client(auth=(api_key, api_secret), version='v3')
    result = mailjet.send.create(data=data)
    app.logger.info("AUDIT: sent mail to {}".format(recipient))



mail_template = '''

'''
