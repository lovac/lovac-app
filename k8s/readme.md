This folder consists of deployment-descriptors for Kubernetes for specific (sub-) components of Lovac.

To understand this better, one should check the spike-documentation which explains each descriptor in detail:
* https://gitlab.com/lovac/lovac-spikes/blob/spikes/issue-4/readme.md
* https://gitlab.com/lovac/lovac-spikes/blob/spikes/issue-4/manual/readme.md

Here is a small description of each folder.

# cert-manager
* Is managing ssl-certificates via Let's encrypt
* We're using v0.3.0
* https://github.com/jetstack/cert-manager
* https://cert-manager.readthedocs.io/en/latest/
* check utils/provision_k8s.sh

# Grafana
* To show Data from Prometheus
* Deployed via Helm
* This stuff here basically only consists of configuration of Prometheus as datasource and having reasonable dashboards
* check utils/deploy_k8s_mon.sh

# Lovac
* That's our payload :-)
* pretty straight forward
